<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-home">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">
	
	
	<section id="broadcasts" class="sec-1">
		<div class="container">
			
			<div class="msg-hl txt-c wow fadeIn" data-wow-delay="0.25s">
				<h2 class="h-text">ประกาศข่าวสาร (broadcasts) แบบมีประสิทธิภาพ<br>
ได้อย่างไม่จำกัด และรับประกันความปลอดภัย<br>
ของข้อมูลภายในองค์กรของคุณ</h2>
				<p class="mt10-xs mt20-lg"><big>มาเริ่มสร้างความปลอดภัย และจัดการการสนทนาในกลุ่มของคุณกัน!</big></p>
				<div class="mt10-xs mt20-lg">
					<button class="ui-btn-white btn-lg btn-focus" type="button" id="button" title="เรียนรู้เพิ่มเติม">เรียนรู้เพิ่มเติม</button>
					<a class="ui-btn-green btn-lg" href="package.php" title="เริ่มใช้เลย">เริ่มใช้เลย</a>
				</div>
			</div>
			
			<figure class="d-flex center-xs">
				<a class="_self-cl-xs-08-md-06 wow fadeInUp" data-wow-delay="0.35s" href="di/home-sec1.png" data-fancybox="feature1" data-caption="ประกาศข่าวสาร (broadcasts) แบบมีประสิทธิภาพ"><img src="di/home-sec1.png" alt="ประกาศข่าวสาร (broadcasts) แบบมีประสิทธิภาพ"></a>
			</figure>

		</div>
	</section>
	
	<section id="learn-more" class="sec-2">
		<div class="container">
			<div class="_chd-cl-xs-12-xsh-06 between-xs middle-xs row reverse">
				<figure class="d-flex center-xs">
					<a class="_self-cl-xs-08-sm-12 wow fadeInUp" data-wow-delay="0.15s" href="di/home-sec3.png" data-fancybox="feature2" data-caption="Line Broadcasts ได้ไม่จำกัด"><img src="di/home-sec2.png" alt="Line Broadcasts ได้ไม่จำกัด"></a>
				</figure>
				<div class="msg-hl txt-l wow fadeIn" data-wow-delay="0.25s">
					<h2 class="h-text txt-l">Line Broadcasts ได้ไม่จำกัด<br>สามารถเลือกกลุ่มและเวลาได้ </h2>
					<big>
					<ul class="list mt20-xs">
						<li>เชื่อมข้อมูลสมาชิกในองค์กรจากบัญชีไลน์</li>
						<li>ตั้งสถานะการเข้าร่วมกลุ่มต่างๆในองค์กร</li>
						<li>จัดการข้อมูลกลุ่มต่างๆในองค์กรของคุณ</li>
						<li>จัดการสิทธิในการเข้าถึงกลุ่มต่างๆได้อย่างมีประสิทธิภาพ</li>
						<li>สามารถอัพโหลดไฟล์ข้อมูลเพื่อการจัดการที่ง่ายขึ้น</li>
					</ul>
					</big>
				</div>
			</div>
		</div>
	</section>
	
	<section id="unlimit" class="sec-3">
		<div class="container">
			<div class="_chd-cl-xs-12-xsh-06 between-xs middle-xs row">
			<figure class="d-flex center-xs">
				<a class="_self-cl-xs-08-md-10 wow fadeInUp" data-wow-delay="0.15s" href="di/home-sec2.png" data-fancybox="feature3" data-caption="จัดการข้อมูลบุคคลและกลุ่มline"><img src="di/home-sec3.png" alt="จัดการข้อมูลบุคคลและกลุ่มline"></a>
			</figure>
			<div class="msg-hl txt-l wow fadeIn" data-wow-delay="0.25s">
				<h2 class="h-text txt-l">จัดการข้อมูลบุคคลและกลุ่มline<br>ภายในองค์กรของคุณเอง</h2>
				<big>
				<ul class="list mt20-xs">
					<li>ไม่มี Limit ของ Message</li>
					<li>เลือกผู้รับได้ตามต้องการ</li>
					<li>อัพโหลดไฟล์เพื่อส่งข้อความแบบ Customize ได้</li>
					<li>ตั้งเวลาในการ Broadcasts ได้</li>
					<li>ปฏิทินสำหรับวางแผน Broadcasts</li>
				</ul>
				</big>
			</div>
		</div>
		</div>
	</section>
	
	<section id="security" class="sec-4">
		<div class="container">
			<div class="_chd-cl-xs-12-xsh-06 between-xs middle-xs row reverse">
				<figure class="d-flex center-xs">
					<a class="_self-cl-xs-08-md-09 wow fadeInUp" data-wow-delay="0.15s" href="di/home-sec4.png" data-fancybox="feature4" data-caption="ระบบ Bot Security เพื่อรักษาความปลอดภัย"><img src="di/home-sec4.png" alt="ระบบ Bot Security เพื่อรักษาความปลอดภัย"></a>
				</figure>
				<div class="msg-hl txt-l wow fadeIn" data-wow-delay="0.25s">
					<h2 class="h-text txt-l">ระบบ Bot Security เพื่อรักษาความปลอดภัย<br>ของข้อมูลที่สำคัญภายในกลุ่มของคุณ</h2>
					<big>
					<ul class="list mt20-xs">
						<li>Add bot security ไปในกลุ่มขององค์กรเพื่อเชื่อมข้อมูลหลังบ้าน</li>
						<li>Bot security สามารถแจ้งสถานะกลุ่มว่ามีบุคคลแปลกปลอม</li>
						<li>ที่ไม่สามารถระบุตัวตนได้อยู่ภายในกลุ่ม </li>
						<li>Bot security สามารถแจ้งสถานะกลุ่มว่ามีบุคคลที่ยืนยันตัวตน</li>
						<li>แต่ไม่มีสิทธิในการเข้าถึงกลุ่ม</li>
						<li>Bot security แจ้งข่าวสารเฉพาะกลุ่มได้</li>		
					</ul>
					</big>
				</div>
			</div>
		</div>
	</section>
	
	<section id="features" class="sec-5">
		<div class="container txt-c">
			<div class="msg-hl txt-l wow fadeIn" data-wow-delay="0.25s">
				<h2 class="h-text txt-c">ระบบหลังบ้าน ที่ออกแบบมาให้ใช้งานง่ายและมีประสิทธิภาพ</h2>
			</div>
			
			<ul class="feature-icon _chd-cl-xs-06-sm-04-md-25 center-xs mt30-md mt20-xs">
				<li class="wow fadeInUp" data-wow-delay="0.05s">
					<i class="ic-feat1"></i>
					<h3>Broadcasts</h3>
					<p>สร้างหรือเลือก template<br class="hidden-xs">
ตามความต้องการ, สามารถจัดเก็บ<br class="hidden-xs">
templateได้, เลือกผู้รับ<br class="hidden-xs">
และตั้งเวลาส่งได้
					</p>
				</li>
				<li class="wow fadeInUp" data-wow-delay="0.15s">
					<i class="ic-feat2"></i>
					<h3>User management</h3>
					<p>การเพิ่ม/ลด แก้ไข Admin<br class="hidden-xs">
ในแต่ละส่วนขององค์กร
					</p>
				</li>
				<li class="wow fadeInUp" data-wow-delay="0.25s">
					<i class="ic-feat3"></i>
					<h3>Company</h3>
					<p>ข้อมูลบริษัท, จัดเก็บข้อมูลกลุ่ม<br class="hidden-xs">
และสมาชิกภายในองค์กร<br class="hidden-xs">
อย่างเป็นระบบ
					</p>
				</li>
				<li class="wow fadeInUp" data-wow-delay="0.35s">
					<i class="ic-feat4"></i>
					<h3>Dashboard</h3>
					<p>รวบรวมข้อมูลทางสถิติเพื่อใช้<br class="hidden-xs">
ในการวิเคราะห์ภายในองค์กร<br class="hidden-xs">
สามารถ export file ได้
					</p>
				</li>
				<li class="wow fadeInUp" data-wow-delay="0.45s">
					<i class="ic-feat5"></i>
					<h3>Report</h3>
					<p>รายงานการส่ง messsage<br class="hidden-xs">
ในรูปแบบต่างๆ<br class="hidden-xs">
สามารถ export file ได้
					</p>
				</li>
			</ul>
			
			<div class="ctrl-btn d-flex center-xs mt30-md mt20-xs wow fadeInUp" data-wow-delay="0.55s">
				<a class="_self-cl-xs-10-sm-06-lg-04 ui-btn-green btn-lg" href="package.php" title="เปิดการใช้งานได้เลยทันที">เปิดการใช้งานได้เลยทันที</a>
			</div>
		</div>
	</section>
		
		
</div>


<footer class="footer">© 2021 INNO HUB CO.,LTD. ALL RIGHTS RESERVED..</footer>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
