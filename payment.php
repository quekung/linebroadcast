<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-checkout">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">

	<section id="package" class="sec-pay">
		<div class="container txt-c">
			<div class="msg-hl txt-l wow fadeIn pb30-sm" data-wow-delay="0.25s">
				<h2 class="h-text txt-c">เลือกวืธีชำระค่าบริการ</h2>
			</div>
			<ul class="idTabs d-flex center-xs flex-nowarp _chd-ph10 pb30-xs">
				<li><a class="ui-btn-border selected" href="#pay1" title="LINE Pay"><img src="di/ic-linepay.png"><span class="hid">LINE Pay</span></a></li>
				<li><a class="ui-btn-border"  href="#pay2" title="โอนเงินเข้าบัญชีธนาคาร">โอนเงินเข้าบัญชีธนาคาร</a></li>
				<li><a class="ui-btn-border"  href="#pay3" title="สแกน QR Code"><img src="di/ic-gbpay.png"> สแกน QR Code</a></li>
			</ul>
			<div class="contentTabs mt30-xs">
				<div id="pay1" class="bx-tab d-flex center-xs">
					<div class="ctrl-btn _self-cl-xs-12-sm-08-lg-06">
						<div class="box">
							<h3 class="hid">LINE Pay</h3>
							<a href="#"><img src="di/linepay.png" alt="line pay"></a>
						</div>
						<a class="ui-btn-green btn-block mt30-xs" href="admin/broadcasts-setup.php" title="LINE Pay">ชำระผ่าน LINE Pay</a>
					</div>
				</div>
				<div id="pay2" class="bx-tab d-flex center-xs">
					<div class="ctrl-btn _self-cl-xs-12-sm-08-lg-06">
						<p class="head">ช่องทางการโอนเงิน/แจ้งชำระเงิน</p>
						<ul class="row _chd-cl-xs-12-xsh-06-mb10">
							<li>
								<div class="box">
									<h3 class="hid">โอนเงินเข้าบัญชีธนาคารกสิกรไทย</h3>
									<a href="#"><img src="di/kbank.png" alt="Kbank"></a>
									<p class="text"><small>บริษัท อินโนฮับ จำกัด</small><big class="d-block">024-3-XXXXX-0</big><small>ธนาคารกสิกรไทย บัญชีออมทรัพย</small></p>
								</div>
							</li>
							<li>
								<div class="box">
									<h3 class="hid">โอนเงินเข้าบัญชีธนาคารไทยพาณิชย์</h3>
									<a href="#"><img src="di/scb.png" alt="SCB"></a>
									<p class="text"><small>บริษัท อินโนฮับ จำกัด</small><big class="d-block">247-XXXXXX-2</big><small>ธนาคารไทยพาณิชย์ บัญชีออมทรัพย</small></p>
								</div>
							</li>
						</ul>

						<a class="ui-btn-darkblue btn-block mt30-xs" href="admin/broadcasts-setup.php" title="แจ้งชำระเงิน">แจ้งชำระเงิน</a>
					</div>
				</div>
				<div id="pay3" class="bx-tab d-flex center-xs">
					<div class="ctrl-btn _self-cl-xs-12-sm-08-lg-06">
						<div class="box">
							<h3 class="hid">สแกน QR Code</h3>
							<a href="di/qr-demo.png" data-fancybox="" data-caption="Generate QR"><img src="di/qr-demo.png" alt="QR Bank"></a>
							<p class="text"><small>สามารถสแกนเพื่อชำระจากแอพพลิเคชั่น Mobile Banking ของทุกธนาคารในประเทศไทย</small></p>
						</div>
						<a class="ui-btn-blue btn-block mt30-xs" href="di/qr-demo.png" data-fancybox="" data-caption="Generate QR" title="Generate QR">Generate QR</a>
					</div>
				</div>
				
			</div>
		</div>
	</section>
</div>


<footer class="footer">© 2021 INNO HUB CO.,LTD. ALL RIGHTS RESERVED..</footer>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<script src="js/jquery.idTabs.min.js"></script> 
<!-- /JS -->
</body>
</html>
