<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html"); ?>
<!-- /Top Head -->

<body class="page-package">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
<div id="toc" class="warpper">

	<section id="package" class="sec-pk">
		<div class="container txt-c">
			<div class="msg-hl txt-l wow fadeIn pb30-sm" data-wow-delay="0.25s">
				<h2 class="h-text txt-c">เลือก Package ที่เหมาะกับคุณ</h2>
			</div>
			<form method="post" action="payment.php">
			
			<ul class="pk-list _chd-cl-xs-12-sm-06-md-03-mb20 mt30-xs center-xs">
				<li class="pk1 wow fadeInUp" data-wow-delay="0.15s">
					<div class="box">
						<h3>S Size</h3>
						<p>เหมาะสำหรับองค์กรที่มีจำนวนสมาชิก<br>ไม่เกิน 100 คน</p>
						<div class="price _chd-cl-xs-06">
							<div class="mz-chk">
								<input type="radio" id="pk-sm" name="chk-pk">
								<label for="pk-sm"><h4>monthly</h4><div>฿ <big>777</big> /month</div></label>
							</div>
							<div class="mz-chk">
								<input type="radio" id="pk-sy" name="chk-pk">
								<label for="pk-sy"><h4>annually</h4><div>฿ <big>7,777</big> /year</div></label>
							</div>
						</div>
					</div>
					<button type="submit" title="ซื้อเลย" class="ui-btn-border btn-lg mt20-xs">ซื้อเลย</button>
				</li>
				
				<li class="pk2 wow fadeInUp" data-wow-delay="0.25s">
					<div class="box">
						<h3>M Size</h3>
						<p>เหมาะสำหรับองค์กรที่มีจำนวนสมาชิก<br>101 - 500 คน</p>
						<div class="price _chd-cl-xs-06">
							<div class="mz-chk">
								<input type="radio" id="pk-mm" name="chk-pk">
								<label for="pk-mm"><h4>monthly</h4><div>฿ <big>888</big> /month</div></label>
							</div>
							<div class="mz-chk">
								<input type="radio" id="pk-my" name="chk-pk">
								<label for="pk-my"><h4>annually</h4><div>฿ <big>8,888</big> /year</div></label>
							</div>
						</div>
					</div>
					<button type="submit" title="ซื้อเลย" class="ui-btn-border btn-lg mt20-xs">ซื้อเลย</button>
				</li>
				
				<li class="pk3 wow fadeInUp" data-wow-delay="0.35s">
					<div class="box">
						<h3>L Size</h3>
						<p>เหมาะสำหรับองค์กรที่มีจำนวนสมาชิก<br>501 - 1,000 คน</p>
						<div class="price _chd-cl-xs-06">
							<div class="mz-chk">
								<input type="radio" id="pk-lm" name="chk-pk">
								<label for="pk-lm"><h4>monthly</h4><div>฿ <big>999</big> /month</div></label>
							</div>
							<div class="mz-chk">
								<input type="radio" id="pk-ly" name="chk-pk">
								<label for="pk-ly"><h4>annually</h4><div>฿ <big>9,999</big> /year</div></label>
							</div>
						</div>
					</div>
					<button type="submit" title="ซื้อเลย" class="ui-btn-border btn-lg mt20-xs">ซื้อเลย</button>
				</li>
				
				<li class="pk4 wow fadeInUp" data-wow-delay="0.45s">
					<div class="box">
						<h3>XL Size</h3>
						<p>เหมาะสำหรับองค์กรที่มีจำนวนสมาชิก<br>มากกว่า 1,000 คน</p>
						<div class="price _chd-cl-xs-06">
							<div class="mz-chk">
								<input type="radio" id="pk-xlm" name="chk-pk">
								<label for="pk-xlm"><h4>monthly</h4><div>฿ <big>1,100</big> /month</div></label>
							</div>
							<div class="mz-chk">
								<input type="radio" id="pk-xly" name="chk-pk"><label for="pk-xly"><h4>annually</h4><div>฿ <big>11,000</big> /year</div></label>
							</div>
						</div>
					</div>
					<button type="submit" title="ซื้อเลย" class="ui-btn-border btn-lg mt20-xs">ซื้อเลย</button>
				</li>
				
				
			</ul>
			
			<!--<div class="ctrl-btn d-flex center-xs mt30-xs wow fadeInUp" data-wow-delay="0.55s">
				<a class="ui-btn-green btn-lg" href="package.php" title="เริ่มใช้เลย">ดำเนินการต่อ</a>
			</div>-->
			</form>
		</div>
	</section>
		
		
</div>


<footer class="footer">© 2021 INNO HUB CO.,LTD. ALL RIGHTS RESERVED..</footer>

<!-- Java Script -->
<?php include("incs/js.html"); ?>
<!-- /JS -->
</body>
</html>
