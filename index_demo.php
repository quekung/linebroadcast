<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<head>
<meta charset="utf-8">

<!-- Title -->
<title>::: One to One Contacts :::</title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<meta name="description" content="OTO contactcenter" />
<meta name="keywords" content="OTO contactcenter" />
<meta name="author" content="OTO contactcenter" />

<link href="https://fonts.googleapis.com/css?family=Prompt:200,400,600" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
<link href="cs/flexgrid.css" rel="stylesheet" type="text/css" />
<link href="cs/flexslider.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" rel="stylesheet">
<link href="cs/theme.css" rel="stylesheet" type="text/css" />

<link rel="contents" href="#toc" title="Table Of Contents" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery-1.11.3.min.js"></script> 

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<!--<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>-->


</head>
<!-- /Top Head -->

<body class="page-home">
<header id="header">
  <div class="container">
	  <div class="top-title">
		<h1 itemscope="" itemtype="http://schema.org/Organization" class="logo">
			<a href="index.php" itemprop="url">One to One Contacts</a>
			<img src="di/logo.png" itemprop="logo">
		</h1>
	  </div>
	  
	  <div class="btn-nv-m" role="navigation">
			<a class="b-ex active" href="javascript:;" title="Expand">
					<span>&nbsp;</span>
					<span>&nbsp;</span>
					<span>&nbsp;</span>
				</a>
		</div>
		<nav id="navigation" class="main-nav" role="navigation">
			<ul id="nav-drop" class=" _chd-cl-md _chd-cl-md-xs-12">
				<li class="home"><a href="index.php" title="HOME">HOME</a></li>
				<li><a class="ui-btn-trans" href="#" title="เรียนรู้เพิ่มเติม">เรียนรู้เพิ่มเติม</a></li>
				<li><a class="ui-btn-green" href="#" title="เริ่มใช้เลย">เริ่มใช้เลย</a></li>
			</ul>
		</nav>
	</div>
</header>

<div class="warpper">
	<!--<section class="sec-highlight">
		<div class="big-slider flexslider">
		<ul class="slides">
			<li>
            <figure>
                <a href="javascript:;" title="Ready Human a higher HR solution">
                <img src=di/highlight-01.jpg alt="Ready Human a higher HR solution">
                <figcaption>Customer success is our success <small>by Ready Human</small></figcaption>
                </a>
            </figure>
			</li>
            <li>
            <figure>
                <a href="javascript:;" title="Ready Human a higher HR solution">
                <img src=di/highlight-02.jpg alt="Ready Human a higher HR solution">
                <figcaption>Customer success is our success <small>by Ready Human</small></figcaption>
                </a>
            </figure>
			</li>
			<li>
            <figure>
                <a href="javascript:;" title="Ready Human a higher HR solution">
                <img src=di/highlight-03.jpg alt="Ready Human a higher HR solution">
                <figcaption>Ready Human a higher HR solution</figcaption>
                </a>
            </figure>
			</li>
                       
                                 
		</ul>
	</div>
	</section>-->
	
	<section id="about" class="sec-aboutus">
		<div class="container">
			<div class="block-quote wow fadeIn" data-wow-delay="0.25s">
				<h2 class="h-text">Ready!Human</h2>
				<blockquote class="txt-c">
					เราให้บริการการสรรหาบุคลากรและการพัฒนาทักษะงานทางด้านบริการ 
					ที่มีคุณภาพ โดยทีมงานมืออาชีพ มีการทำงานที่รวดเร็ว 
					ในการจัดส่งพนักงาน ด้วยเทคโนโลยีที่ทันสมัยและประกันในคุณภาพ 
					ของบุคลากรที่เราส่งมอบ
				</blockquote>
			</div>
			
			<div class="slogan row _chd-cl-xs-12-xsh-04">
				<article class="wow fadeInLeft" data-wow-delay="0.25s">
					<a href="javascript:;" title="xxx">
					<img src="di/ic-human-01.png" alt="xxx">
					<h3>“ FAST delivery”</h3>
					<p>รวดเร็วในการสรรหาบุคลากรเพื่อตอบสนองความต้องการของลูกค้า</p>
					</a>
				</article>
				<article class="wow fadeInUp" data-wow-delay="0.25s">
					<a href="javascript:;" title="xxx">
					<img src="di/ic-human-01.png" alt="xxx">
					<h3>“SMART process”</h3>
					<p>ได้มีการนำเทคโนโลยีเข้ามาให้ในระบบการสรรหาและคัดเลือกบุคลากร ทำให้ ได้บุคลากรที่มีคุณภาพตามที่ลูกค้าต้องการ</p>
					</a>
				</article>
				<article class="wow fadeInRight" data-wow-delay="0.25s">
					<a href="javascript:;" title="xxx">
					<img src="di/ic-human-01.png" alt="xxx">
					<h3>“ACCURACY guarantee”</h3>
					<p>รับประกันว่าพนักงานทุกคนที่ได้ผ่านการคัดเลือกจะมีคุณภาพ และคุณสมบัติตามข้อกำหนดของลูกค้า</p>
					</a>
				</article>
				
			</div>
		</div>
		<div class="history txt-c">
			<h2 class="h-text _self-mb20">Ready!Human</h2>
			<div class="text-titile wow fadeIn" data-wow-delay="0.45s">
			<p>เป็นส่วนหนึ่งของบริษัท วัน ทูวัน จำกัด มหาชน ที่มีความเชี่ยวชาญในการบริการงานทางด้าน <br>
Fully Outsource และ TurnKey ให้กับหน่วยงานรัฐและเอกชนมากกว่า 19 ปี
</p>
	<p>เราให้บริการในการสรรหาบุคลากรทุกสายอาชีพ ให้กับหน่วยงานรัฐและเอกชน <br>
รวมไปถึงการให้บริการงานทางด้านฝึกอบรมงานทางด้านงานบริการ (Service mine)  <br>
เทคนิคการโน้มน้าวลุกค้า (convincing) และหลักสูตรอื่นๆ
</p>
			</div>
		</div>
	</section>
	
	<section id="service" class="sec-service">
		<div class="container">
		<h2 class="h-text _self-mb20 t-white">OUR SERVICE</h2>
		<ul class="nav-cc _chd-cl-xs-03 between-xs">
            <li rel="tab1" class="active wow fadeInLeft" data-wow-delay="0.25s"><img src="di/ic-service-01.png" alt="Payroll Service"><h3 class="wr"> Payroll <small>Service</small></h3></li>
            <li rel="tab2" class="wow fadeInUp" data-wow-delay="0.45s"><img src="di/ic-service-02.png" alt="Headcount delivery Service"><h3 class="wr"> Headcount delivery <small>Service</small></h3></li>
            <li rel="tab3" class="wow fadeInUp" data-wow-delay="0.65s"><img src="di/ic-service-03.png" alt="Head hunt Service"><h3 class="wr"> Head hunt <small>Service</small></h3></li>
            <li rel="tab4" class="wow fadeInRight" data-wow-delay="0.85s"><img src="di/ic-service-04.png" alt="Payroll Service"><h3 class="wr"> All <small>Service</small></h3></li>
        </ul>
        <div class="tab_container wow fadeIn" data-wow-delay="0.35s">
            <h3 class=" tab_hd hid wr" rel="tab1">Payroll Service</h3>
          <!-- #tab1 -->
          <div id="tab1" class="tab_content">
            <h2 class="h-text">Payroll Service คือ</h2>
            <p><big>การบริการจัดส่งบุคลากรรายเดือนตามจำนวน คุณสมบัติและทักษะที่ต้องการ พร้อมดำเนินการด้านผลตอบแทนตามข้อตกลงของผู้ว่าจ้าง</big></p>
          </div>
          <!-- #tab2 -->
          <h3 class=" tab_hd hid wr" rel="tab2">Headcount delivery Service</h3>
          <div id="tab2" class="tab_content">
            <h2 class="h-text">Headcount delivery Service คือ</h2>
            <p><big>การบริการจัดส่งบุคลากรรายเดือนตามจำนวน คุณสมบัติและทักษะที่ต้องการ พร้อมดำเนินการด้านผลตอบแทนตามข้อตกลงของผู้ว่าจ้าง</big></p>
          </div>
          <!-- #tab3 -->
           <h3 class=" tab_hd hid wr" rel="tab3">Head hunt  Service</h3>
          <div id="tab3" class="tab_content">
            <h2 class="h-text">Head hunt Service คือ</h2>
            <p><big>การบริการจัดส่งบุคลากรรายเดือนตามจำนวน คุณสมบัติและทักษะที่ต้องการ พร้อมดำเนินการด้านผลตอบแทนตามข้อตกลงของผู้ว่าจ้าง</big></p>
          </div>
          <!-- #tab4 -->
           <h3 class=" tab_hd hid wr" rel="tab4">All Service</h3>
          <div id="tab4" class="tab_content">
            <h2 class="h-text">All Service คือ</h2>
            <p><big>การบริการจัดส่งบุคลากรรายเดือนตามจำนวน คุณสมบัติและทักษะที่ต้องการ พร้อมดำเนินการด้านผลตอบแทนตามข้อตกลงของผู้ว่าจ้าง</big></p>
          </div>
          <!-- #end tab --> 
        </div>
		
		</div>
	</section>
	
	<section id="world" class="sec-worldclass">
		<div class="container wow fadeInUp" data-wow-delay="0.25s">
			<h2 class="h-text">WORLD CLASS STANDARD</h2>
			<figure class="lg-service">
				<a href="di/logo-service.png" data-fancybox="image" data-caption="Contact US"><img src="di/logo-service.png" alt="World Class Standard"></a>
			</figure>
		</div>
	</section>
	
	<section id="customers" class="sec-customer">
		<div class="container">
			<h2 class="h-text t-white">OUR CUSTOMERS</h2>
			<div class="list-cs">
				<ol class="_chd-cl-xs-12">
					<li class="wow fadeInLeft" data-wow-delay="0.25s">
						<ul class="g1 _chd-cl-xs-25">
							<li><i class="ic-cs1-01"></i> <h3>Retails</h3></li>
							<li><i class="ic-cs1-02"></i> <h3>Airlines</h3></li>
							<li><i class="ic-cs1-03"></i> <h3>Electronic<br>Compliance</h3></li>
							<li><i class="ic-cs1-04"></i> <h3>Automotive</h3></li>
							<li><i class="ic-cs1-05"></i> <h3>Real Estate</h3></li>
						</ul>
					</li>
					
					<li class="wow fadeInLeft" data-wow-delay="0.35s">
						<ul class="g2 _chd-cl-xs-25">
							<li><i class="ic-cs2-01"></i> <h3>Finance <br>&amp; Banking</h3></li>
							<li><i class="ic-cs2-02"></i> <h3>Insurance</h3></li>
							<li><i class="ic-cs2-03"></i> <h3>GoverNment</h3></li>
							<li><i class="ic-cs2-04"></i> <h3>Hospital</h3></li>
						</ul>
					</li>
					
					<li class="wow fadeInLeft" data-wow-delay="0.45s">
						<ul class="g3 _chd-cl-xs-25">
							<li><i class="ic-cs3-01"></i> <h3>E-Commerce</h3></li>
							<li><i class="ic-cs3-02"></i> <h3>Energy</h3></li>
							<li><i class="ic-cs3-03"></i> <h3>Food<br>&amp; Beverage</h3></li>
							<li><i class="ic-cs3-04"></i> <h3>Tele<br>communication</h3></li>

						</ul>
					</li>
				</ol>
			</div>
		</div>
	</section>
		
	<section id="contact" class="sec-contact">
		<figure class="cover wow fadeIn" data-wow-delay="0.25s"><img src="di/head-contact.jpg" alt="contact us"></figure>
		<div class="bg">
			<div class="container _self-pa0">
				<h2 class="h-text t-white">CONTACT US</h2>
				<address class="wow fadeInUp" data-wow-delay="0.25s">99/19 หมู่ 4 อาคารซอฟต์แวร์ปาร์ค ชั้น 1 <br>ถนนแจ้งวัฒนะ ตำบลคลองเกลือ <br>อำเภอปากเกร็ด จังหวัดนนทบุรี 11120</address>
				
			</div>
		</div>
		<div class="container">
			<div class="wrap-qr _flex between-xs">
				<ul class="list-ct wow fadeInLeft" data-wow-delay="0.25s">
					<li>Telephone : <a href="tel:025026000">025026000</a></li>
					<li>Email:   <a href="mailto:Sale_info@oto.readyhuman.com">Sale_info@oto.readyhuman.com</a></li>
					<li>Line: <a href="http://line.me/ti/p/~@Readyhuman" target="_blank">@Readyhuman</a></li>
					<li>Facebook : <a href="https://www.facebook.com/ReadyHuman" target="_blank" title="FB : ReadyHuman">ReadyHuman</a></li>
					<li>Website : <a href="www.ReadyHumam.com">www.ReadyHumam.com</a></li>

				</ul>
				<figure class="_self-cl-xs-06-md-03 wow fadeInRight" data-wow-delay="0.35s">
					<a href="di/qr-code-contact.png" data-fancybox="image" data-caption="Contact US"><img src="di/qr-code-contact.png" alt="Contact US" class="img-responsive"></a>
				</figure>
			</div>
		</div>
	</section>
		
		
</div>


<footer class="footer">© 2019 ONE TO ONE CONTACTS PLC. ALL RIGHTS RESERVED.</footer>

<!-- Java Script -->
<script type="text/javascript">
$(document).ready(function(){
	//back to top
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('#back2top').css({bottom:"55px"});
		} else {
			$('#back2top').css({bottom:"-100px"});
		}
	});
	$('#back2top').click(function(){
		$('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});
	//expand nav
	$('.btn-nv-m .b-ex, .main-nav>ul>li a').click(function(){
		$(this).toggleClass('active');
		$('body').toggleClass('expand');
	});
     /*header*/
	if($('#header').length > 0) {
		var top17 = $('#header').offset().top + parseFloat($('#header').innerHeight());
		$(window).scroll(function (event) {
			var y17 = $(this).scrollTop();
			if (y17 >= top17 ) {$('#header').addClass('fixed');} else {$('#header').removeClass('fixed');}
		});
	}
	/*tab accordion*/
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.nav-cc li").click(function() {
        if (!$(this).hasClass( 'active' )) {	
          $(".tab_content").slideUp();
          var activeTab = $(this).attr("rel"); 
          $("#"+activeTab).slideDown();		

          $("ul.nav-cc li").removeClass("active");
          $(this).addClass("active");

          $(".tab_hd").removeClass("d_active");
          $(".tab_hd[rel^='"+activeTab+"']").addClass("d_active");
        } else {
            var activeTab = $(this).attr("rel"); 
            $("#"+activeTab).slideUp();		
            $(this).removeClass("active");
        }
    });
	/* if in drawer mode */
	$(".tab_hd").click(function() {
        if (!$(this).hasClass( 'd_active' )) {	   
          $(".tab_content").slideUp();
          var d_activeTab = $(this).attr("rel"); 
          $("#"+d_activeTab).slideDown();

          $(".tab_hd").removeClass("d_active");
          $(this).addClass("d_active");

          $("ul.nav-cc li").removeClass("active");
          $("ul.nav-cc li[rel^='"+d_activeTab+"']").addClass("active");
        } else {
            var d_activeTab = $(this).attr("rel"); 
            $("#"+d_activeTab).slideUp();		
            $(this).removeClass("d_active");
        }
    });

	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.nav-cc li').last().addClass("tab_last");

});


/*scroll to*/
$(window).load(function(){
    $(document).ready(function () {
    $(document).on("scroll", onScroll);
    
    //smoothscroll
    $('#nav-drop a[href^="#"]').on('click', function (e) {

        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
		
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 100
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('#nav-drop>li.pg>a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('#nav-drop>li.pg>a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}
});
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script>
$(document).ready(function() {
	$("[data-fancybox='modal']").fancybox({
		toolbar  : false,
		margin : [1, 0],
		infobar : false,
		protect : false,
		arrows : false,
        touch: false,
	});

	$('[data-fancybox="image"]').fancybox({
    	arrows : false,
    });
	
});
</script>

<script type="text/javascript" src="js/jquery.flexslider.js"></script>
<script>
$(window).load(function() {
  $('.big-slider').flexslider({
    animation: "slide",
    smoothHeight: true,
  });
});
</script>


<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    // WOW Animation
    new WOW().init();
</script>

<!-- /JS -->
</body>
</html>
