<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			
					

			
			<div class="bg-white contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					<div class="head-title m-0 txt-l">
						<h2>User Detail</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card">
									<div class="card-header _flex center-xs between-xsh">
										<ul class="idTabs _self-pa0 tab-receiver">
											<li><a href="broadcasts-acc.php" class="selected"><i class="qic-img"><img src="di/ic-groups.png" height="20"></i> Line Groups</a></li>
											<li><a href="broadcasts-acc-user.php"><i class="qic-img"><img src="di/ic-users.png" height="20"></i> Users</a></li>
										</ul>
										<div class="search-sm _self-mr20">
											<input class="txt-box" placeholder="ค้นหา...">
											<button type="submit" class="fas fa-search"></button>
										</div>
									</div>
									<!-- Group -->
									<div id="linegroup" class="card-body _self-pt30 middle-xs">
										<div class="list-user">
											<ol id="myTable">
												<li class="head">
													<div class="c1">AVATAR</div>
													<div class="c4">DISPLAY GROUP NAME</div>
													<div class="c3">GROUP NAME DETAIL</div>
													<div class="c2 txt-c">STATUS</div>
													<div class="c2 txt-c">SECURITY</div>
													<!--<div class="c10 txt-c">APPROVED</div>-->

												</li>

												<?php function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>
												<?php for($i=4;$i<=20;$i++){ ?>
												<li>
													<div class="c1 pd0 txt-c">
														<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar6.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar5.png" alt="Fat Rascal" width="40"><? } ?>
													</div>
													<div class="c4"><?php echo generateRandomString(); ?></div>
													<div class="c3"><input type="text" class="txt-box" placeholder="Setting Name"></div>

													<?php /*?><div class="c4">
													<select class="form-control keep-select-group" multiple="multiple">
														<option>Select Admin</option>
													    <option>Alabama</option>
														<option>Alaska</option>
														<option>California</option>
														<option>Delaware</option>
														<option>Tennessee</option>
														<option>Texas</option>
														<option>Washington</option>
													</select>
													</div><?php */?>
													
													<div class="c2 small txt-c">
													<select class="form-control select2">
													  <option selected="selected">Active</option>
													  <!--<option>Inactive</option>-->
													  <option>Disabled</option>
													</select>
													</div>
													
													<div class="c2 small txt-c">
													<select class="form-control select2">
													  <option selected="selected">1 hr</option>
													  <option>2 hr</option>
													  <option>3 hr</option>
													  <option>4 hr</option>
													  <option>5 hr</option>
													  <option>6 hr</option>
													  <option>7 hr</option>
													  <option>8 hr</option>
													  <option>9 hr</option>
													  <option>10 hr</option>
													  <option>11 hr</option>
													  <option>12 hr</option>
													  <option>24 hr</option>
													  <option>ปิด</option>
													</select>
													</div>

													<?php /*?><div class="c10 txt-c">
													  <div class="icheck-primary">
														<input type="checkbox" name="approve<? echo($i) ?>" id="approve-chk<? echo($i) ?>"> 
														<label for="approve-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
													  </div>
													</div><?php */?>
												</li>
												<?php } ?>
											</ol>
										</div>
									</div>

									
									<div class="sticky-bottom card-footer mf-bottom">
									<div class="__chd-ph10 center-xs">
											<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
											<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$(".select2").select2();
	$('.keep-select-group').select2({
		maximumSelectionLength: 3,
    	placeholder: "Select Admin",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});

	
	

});
  </script>
  


<!-- /js -->

</body>
</html>
