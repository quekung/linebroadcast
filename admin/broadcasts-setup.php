<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout bg-gray2">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			
					

			
			<div class="contentTabs">
				<div id="tbc-1" class="msg">
					<div class="form-checkout form-sending">
					<div class="head-title m-0 txt-l">
						<h2>Select Category</h2>	
						<p>เลือกหมวดหมู่การสร้างตาราง</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card">
									<div class="card-header _flex center-xs txt-c">
										<h2 class="text-md">เลือกหมวดหมู่ของคุณ</h2>
									</div>
	
									<div class="type-tab">
										<ul id="type-table" class="idTabs tab-btn between-xs row _chd-cl-xs-12-sm-04">
											<li><a href="#company" class="selected"><big>Company</big> <small class="d-block">บริษัท/องค์กร</small></a></li>	
											<li><a href="#education"><big>Education</big> <small class="d-block">สถานบันการศึกษา</small></a></li>
											<li><a href="#community"><big>Community</big> <small class="d-block">ชมรม/สมาคม</small> </a></li>
										</ul>
									</div>
									
									<div class="contentTabs">
										<!-- All User -->
										<div id="company" class="card-body _self-pt30 middle-xs">
											<h3 class="head t-black">Company</h3>
											<div class="bg-white _self-pa20 wrap-add-tb">
												<p class="lead t-black mb-0">Add / Edit Column</p>
												<ul class="customs-column">
													<li>
														<div class="form-group p-0 d-flex flex-wrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white _self-cl-xs" placeholder="รหัสพนักงาน" value="รหัสพนักงาน" readonly>
															<span class="_self-cl-xs-12-sm-06">
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-offid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-offid">กำหนดเอง</label></small>
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-genid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-genid">สร้างโดยอัตโนมัติ</label></small>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white" placeholder="Email" value="Email" readonly>
															<span class="_self-cl-xs-12-sm-06">
																&nbsp;
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ชื่อ-นามสกุล">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="บริษัท">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ฝ่าย">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="แผนก">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ตำแหน่ง">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="Level">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="สถานะ">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันเริ่มงาน">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันสิ้นสุด">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="Tag">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
												</ul>
												<div class="bar-add-table form-group p-0 d-flex flex-nowrap middle-xs">
													<label class="num pa10-xs _self-mb0">&nbsp;&nbsp;</label>
													<a href="javascript:;" class="ui-btn-green btn-md add-column"><i class="qic"><img src="di/ic-new-tpl.png" height="20"></i> Add Column</a>
												</div>
												
												<div class="card-footer mt30-md bg-gray2 mf-bottom">
													<div class="_chd-ph10 center-xs">
															<button type="reset" class="ui-btn-white btn-md">Cancel</button>
															<a data-fancybox data-type="iframe" data-src="broadcasts-table-preview.php" href="javascript:;" class="ui-btn-green btn-md"><i class="hid fas fa-circle-notch fa-spin"></i> Preview</a>
															<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save</button>
													</div>
												</div>
											</div>
										</div>
										
										<!-- education -->
										<div id="education" class="card-body _self-pt30 middle-xs">
											<h3 class="head t-black">Education</h3>
											<div class="bg-white _self-pa20 wrap-add-tb">
												<p class="lead t-black mb-0">Add / Edit Column</p>
												<ul class="customs-column">
													<li>
														<div class="form-group p-0 d-flex flex-wrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white _self-cl-xs" placeholder="รหัสนักศึกษา" value="รหัสนักศึกษา" readonly>
															<span class="_self-cl-xs-12-sm-06">
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-offid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-offid">กำหนดเอง</label></small>
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-genid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-genid">สร้างโดยอัตโนมัติ</label></small>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white" placeholder="Email" value="Email" readonly>
															<span class="_self-cl-xs-12-sm-06">
																&nbsp;
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ชื่อ-นามสกุล">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="สถานะ">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="University/School">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="คณะ/แผนการเรียน">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ชั้นปี/ระดับชั้น">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="สถานภาพ">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันที่เข้าศึกษา">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันสิ้นสุด">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="Tag">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
												</ul>
												<div class="bar-add-table form-group p-0 d-flex flex-nowrap middle-xs">
													<label class="num pa10-xs _self-mb0">&nbsp;&nbsp;</label>
													<a href="javascript:;" class="ui-btn-green btn-md add-column"><i class="qic"><img src="di/ic-new-tpl.png" height="20"></i> Add Column</a>
												</div>
												
												<div class="card-footer mt30-md bg-gray2 mf-bottom">
													<div class="_chd-ph10 center-xs">
															<button type="reset" class="ui-btn-white btn-md">Cancel</button>
															<a data-fancybox data-type="iframe" data-src="broadcasts-table-preview2.php" href="javascript:;" class="ui-btn-green btn-md"><i class="hid fas fa-circle-notch fa-spin"></i> Preview</a>
															<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save</button>
													</div>
												</div>
											</div>
									</div>
										
										<!-- Community -->
										<div id="community" class="card-body _self-pt30 middle-xs">
											<h3 class="head t-black">Community</h3>
											<div class="bg-white _self-pa20 wrap-add-tb">
												<p class="lead t-black mb-0">Add / Edit Column</p>
												<ul class="customs-column">
													<li>
														<div class="form-group p-0 d-flex flex-wrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white _self-cl-xs" placeholder="รหัสพนักงาน" value="รหัสประจำตัว" readonly>
															<span class="_self-cl-xs-12-sm-06">
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-offid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-offid">กำหนดเอง</label></small>
																<small class="icheck-primary mb-0 ml10-xs"><input type="radio" id="chk-genid" name="chk-genid"> <label class="text-sm _self-mb0" for="chk-genid">สร้างโดยอัตโนมัติ</label></small>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control bg-white" placeholder="Email" value="Email" readonly>
															<span class="_self-cl-xs-12-sm-06">
																&nbsp;
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ชื่อ-นามสกุล">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="ชื่อสมาคม">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="สถานะ">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="สถานภาพ">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="เบอร์โทรศัพท์">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันที่เข้าเป็นสมาชิก">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="วันสิ้นสุด">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													<li>
														<div class="form-group p-0 d-flex flex-nowrap middle-xs">
															<label class="num pa10-xs _self-mb0"></label>
															<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="Tag">
															<span class="_self-cl-xs-12-sm-06">
																<div class="tools">
																  <a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a>
																  <a href="javascript:;" onclick="$(this).parents('li').remove();"><i class="fas fa-trash text-danger"></i></a>
																</div>
															</span>
														 </div>
													</li>
													
												</ul>
												<div class="bar-add-table form-group p-0 d-flex flex-nowrap middle-xs">
													<label class="num pa10-xs _self-mb0">&nbsp;&nbsp;</label>
													<a href="javascript:;" class="ui-btn-green btn-md add-column"><i class="qic"><img src="di/ic-new-tpl.png" height="20"></i> Add Column</a>
												</div>
												
												<div class="card-footer mt30-md bg-gray2 mf-bottom">
													<div class="_chd-ph10 center-xs">
															<button type="reset" class="ui-btn-white btn-md">Cancel</button>
															<a data-fancybox data-type="iframe" data-src="broadcasts-table-preview3.php" href="javascript:;" class="ui-btn-green btn-md"><i class="hid fas fa-circle-notch fa-spin"></i> Preview</a>
															<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save</button>
													</div>
												</div>
											</div>
									</div>

									
									
									
										<?php /*?><div class="sticky-bottom card-footer mt30-md bg-gray2 mf-bottom">
										<div class="__chd-ph10 center-xs">
												<button type="reset" class="ui-btn-white btn-md">Cancel</button>
												<a data-fancybox data-type="iframe" data-src="broadcasts-table-preview.php" href="javascript:;" class="ui-btn-green btn-md"><i class="hid fas fa-circle-notch fa-spin"></i> Preview</a>
												<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save</button>
										</div>
										</div><?php */?>
								  
								  </div>
								  
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</div>

			</div>
			
			

		</section>
    </div>
</div>



<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 //add column
	 /*$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
			if (!$(this).hasClass('add-tab')) {
				$(this).tab('show');
			}
		})
		.on("click", "span", function () {
			var anchor = $(this).siblings('a');
			$(anchor.attr('href')).remove();
			$(this).parent().remove();
			$(".cs-flex-tab li").children('a').first().click();
		});*/
	 $('.add-column').click(function (e) {
		e.preventDefault();
		var id = $(this).parents(".wrap-add-tb").children(".customs-column").children().length+1; //think about it ;)
		var tabId = 'card_' + id;
		var element = "('li')";
		/*var startId = 'start: ' + id;
		var settings = { startId , change:false }; 
		alert ( startId );
		alert ( settings );*/
		//$(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Option ' + id + ' </a> <span> x </span></li>');
		//$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
		var clonecard= '<div class="form-group p-0 d-flex flex-nowrap middle-xs">';
		clonecard+='<label class="num pa10-xs _self-mb0"> </label>';
		clonecard+='<input type="text" class="form-control" placeholder="ตั้งชื่อ Column" value="">';
		clonecard+='<span class="_self-cl-xs-12-sm-06">';
		clonecard+='<div class="tools">';
		clonecard+='<a href="javascript:;"><i class="fas fa-edit t-gray2"></i></a> ';
		clonecard+='<a href="javascript:;" onclick="$(this).parents'+ element +'.remove();"><i class="fas fa-trash text-danger"></i></a>';
		clonecard+='</div>';
		clonecard+='</span>';
		clonecard+='</div>';			
		//clonecard+='<label>elect option title ' + id + '</label>';
		//write
		$(this).parents(".wrap-add-tb").children('.customs-column').append('<li class="tb-pane" id="' + tabId + '">' + clonecard + '</li>');
		/*$('#custom-carousel-tab').idTabs(); 
		$('#custom-carousel-tab li:nth-child(' + id + ') a').click();*/
	});

	//select2
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	});
	

	
	

  </script>

<!-- /js -->

</body>
</html>
