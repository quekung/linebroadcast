<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report" class="selected"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
								<div class="head-title m-0">
									<h2>Report</h2>	
									<p>ระบบส่งข้อความผ่านทาง line</p>
								</div>
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header">
										<h3 class="card-title center-xs"><b>transection detail</b></h3>
									</div>
									<div class="card-body _self-pa30 middle-xs">
										<div class="table-responsive table-responsive-sm table-detail">
											<table class="table table-hover table-report">
											  <thead>
											  <tr>
												<th class="text-center" align="center">AVATAR</th>
							<th>NAME SURNAME</th>
							<th class="text-center" align="center">STATUS <a href="javascript:;" class="text-muted"><i class="fas fa-sort"></i></a></th>
							<th>SENT AT <a href="javascript:;" class="text-muted"><i class="fas fa-sort"></i></a></th>

												<th class="text-center" align="center">READ <a href="javascript:;" class="text-muted"><i class="fas fa-sort"></i></a></th>

											  </tr>
											  </thead>
											  <tbody>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-success btn-sm">SUCCESS</span></td>
							<td><p><small>11/23/18,<br>
							10:30:00 am</small></p></td>
							<td align="center"><small class="text-orange">11/23/18,<br>
							 10:30:00 am</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-warning btn-sm">Waiting <small class="text-muted">(5hr.)</small></span></td>
							<td><p><small>11/23/18,<br>
							11:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr><tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-danger btn-sm">FAILED</span></td>
							<td><p><small>11/23/18,<br>
							12:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-success btn-sm">SUCCESS</span></td>
							<td><p><small>11/23/18,<br>
							10:30:00 am</small></p></td>
							<td align="center"><small class="text-orange">11/23/18,<br>
							 10:30:00 am</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-success btn-sm">SUCCESS</span></td>
							<td><p><small>11/23/18,<br>
							11:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr><tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-danger btn-sm">FAILED</span></td>
							<td><p><small>11/23/18,<br>
							12:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-success btn-sm">SUCCESS</span></td>
							<td><p><small>11/23/18,<br>
							10:30:00 am</small></p></td>
							<td align="center"><small class="text-orange">11/23/18,<br>
							 10:30:00 am</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-warning btn-sm">Waiting <small class="text-muted">(1hr.)</small></span></td>
							<td><p><small>11/23/18,<br>
							11:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr><tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>
	
												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-danger btn-sm">FAILED</span></td>
							<td><p><small>11/23/18,<br>
							12:00:00 am</small></p></td>
							<td align="center"><small class="text-orange">-</small></td>






											  </tr>
											  <tr>
												<td align="center">
												  <div class="avatar"><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"></div>

												</td>
							<td>Name Surname</td>
							<td align="center"><span class="btn  btn-outline-success btn-sm">SUCCESS</span></td>
							<td><p><small>11/23/18,<br>
							10:30:00 am</small></p></td>
							<td align="center"><small class="text-orange">11/23/18,<br>
							 10:30:00 am</small></td>






											  </tr>
											  </tbody>
											</table>
										  </div>
									</div>
									
									<div class="card-footer clearfix">
										<div id="smart-paginator" class="paging pagination center-xs">

											<ul class="col"><li><a href="#" class="active">1</a></li> <li><a href="#P12">2</a></li> <li><a href="#P24">3</a></li> <li><a href="#P12">&gt;</a></li> <li><a href="#P216">&gt;&gt;</a></li> </ul>
		
										</div>
								  </div>
									
									<!--<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>-->
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#-limit").removeClass("bg-white");
				$("#-limit").prop("disabled",true); 
				$("#-limit").prop("required",false); 
			} else {
				$("#-limit").addClass("bg-white");
				$("#-limit").prop("disabled",false);
				$("#-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
