<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(2)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-checkout _self-pt0 mb0">
			
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="company.php" title="Send Message"><i class="fas fa-archway"></i> <span>Company</span></a></li>
						  <li><a href="department.php" title="Create Message"><i class="fas fa-boxes"></i> <span>Department</span></a></li>
						  <li><a href="member-account.php" title="User management " class="selected"><i class="fas fa-user-shield"></i> <span>User management</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <!--<li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="lns-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>-->
				  </ul>
			</div>
			
			<div class="bg-white">
				<!--<div class="head-bg">
					<div class="container">
						<h2>SETTING</h2>	
					</div>
					</div>-->
				<div class="head-title m-0">
					<h2>SETTING</h2>	
					<p>ระบบส่งข้อความผ่านทาง line</p>
				</div>
				<div class="container msg">
				
					
        	<form class="bx-keep form-signin form-checkout _self-pt0" method="post" action="#">
				<fieldset class="fix-label _self-pt10">
						 
						 <div class="box-san">
							<!--<div class="head-title">
							<h2 class="hd">สำหรับองค์กร</h2>
							</div>-->
							<div class="cover pd0">
								<h3 class="head txt-l">Team Members</h3>
							</div>
							<div class="body">
								<div id="show-mem" class="row">
									<ul class="users-list w-100 clearfix">
									  
									  
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">Alexander Pierce</b>
										<span class="users-list-date">Super Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar1.png" alt="User Image">
										<b class="users-list-name">Alexander Pierce</b>
										<span class="users-list-date">Admin1</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar3.png" alt="User Image">
										<b class="users-list-name">Norman</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar5.png" alt="User Image">
										<b class="users-list-name">Jane</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar6.png" alt="User Image">
										<b class="users-list-name">John</b>
										<span class="users-list-date">Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar2.png" alt="User Image">
										<b class="users-list-name">Alexander Pierce</b>
										<span class="users-list-date">Super Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar6.png" alt="User Image">
										<b class="users-list-name">Norman</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Jane</b>
										<span class="users-list-date">Member</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li class="col-2">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar3.png" alt="User Image">
										<b class="users-list-name">John</b>
										<span class="users-list-date">Admin</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  <li id="test-show" class="col-2" style="display:none">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-edit-user">
										<img src="https://www.w3schools.com/w3images/avatar4.png" alt="User Image">
										<b class="users-list-name">Santa Keep</b>
										<span class="users-list-date">Viewer</span>
										</a>
										<div class="tools"><i class="fas fa-trash text-white" onClick="$(this).parents('li').remove();"></i></div>
									  </li>
									  
									  <li class="add-new">
										<a href="javascript:;"  data-fancybox="" data-modal ="modal" data-src="#modal-add-user">
											<i class="fas fa-user-plus fa-3x"></i>
											<b class="users-list-name">Add New</b>
											<span class="users-list-date">User</span>
										</a>
									  </li>
									</ul>
								</div>
							</div>
						</div>
						 


					
			<!--		<div class="agree">
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree" name="iam-gree" required="required"><label for="iam-gree"> I agree to the Smart Chat <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-terms"> Terms and Conditions.</a></label></div>
						<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree-policy" name="iam-gree-policy" required="required"><label for="iam-gree-policy"> I agree to the Smart Chat <a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-policy"> Privacy policy.</a></label></div>
					</div>
					-->
					
					<div class="ctrl-btn txt-c _self-mt20 sticky-bottom">
						<!--<input type="submit" id="Cancel" class="ui-btn-green btn-sm" value="เลือกแพ็คเกจอื่น"> -->
							<a href="javascript:window.history.back();" title="เลือกแพ็คเกจอื่น" class="ui-btn-gray btn-sm" ><i class="fas fa-angle-left"></i> Back</a> 
							<input type="submit" id="Submit" class="ui-btn-green btn-sm" value="Save">
					</div>
				</fieldset>
			</form>
				</div>
			</div>
			
			

			
		</section>
    </div>
</div>


<div class="popup thm-lite" id="modal-add-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Add Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="form-control" id="u-Email">
									  </div>
									   <div class="form-group">
										<label for="u-user1">Username</label>
										<input type="text" class="form-control" id="u-user">
									  </div>

									  <div class="form-group">
										<label for="u-pass">Password</label>

										<div class="input-group">
										  <input type="password" class="form-control" id="u-pass">
										  <span class="input-group-append">
											<button type="button" class="btn bg-gray pl10-xs pr10-xs border-0">Gen</button>
										  </span>
										</div>
									  </div>
								</div>

								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-name">Display Name</label>
										<input type="text" class="form-control" id="exampleInputEmail">
									  </div>

									  <div class="form-group">
										<label for="exampleInputEmail1">Role</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="options" id="option-add1" autocomplete="off" checked> Admin
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option-add2" autocomplete="off"> Agent
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option-add3" autocomplete="off"> Viewer
										  </label>
										</div>
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input1" id="apm_img1" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg1" for="apm_img1"><p>Choose file</p></label>
												</div>
												<!--<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>-->

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off"> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										</div>
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
<div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Edit Member</h2>
					</div>
					<div class="modal-body txt-l">
					  
					  <div class="form-group p-2 bg-light rounded" data-select2-id="72">

							<div class="row">
								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-Email1">Email address</label>
										<input type="email" class="form-control" id="u-Email1" value="Username@mail.com">
									  </div>
									   <div class="form-group">
										<label for="u-user1">Username</label>
										<input type="text" class="form-control" id="u-user1" value="innohub1">
									  </div>

									  <div class="form-group">
										<label for="u-pass">Password</label>

										<div class="input-group">
										  <input type="password" class="form-control" id="u-pass" value="123456">
										  <span class="input-group-append">
											<button type="button" class="btn bg-gray pl5-xs pr5-xs border-0">Reset</button>
										  </span>
										</div>
									  </div>
								</div>

								<div class="col-sm-6">
									  <div class="form-group">
										<label for="u-name">Display Name</label>
										<input type="text" class="form-control" id="exampleInputEmail1" value="Alexander Pierce">
									  </div>

									  <div class="form-group">
										<label for="exampleInputEmail1">Role</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="options" id="option1" autocomplete="off" checked> Admin
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option2" autocomplete="off"> Agent
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="options" id="option3" autocomplete="off"> Viewer
										  </label>
										</div>
									  </div>
									  
									  <div id="f-upload_avatar" class="f-upload form-group" style="">
										<label for="apm_img2">Upload Avatar</label>
										<div class="input-group">
												<div class="custom-file">
													<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
													<img id="temp_apmimage_src" hidden="">
													<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
												</div>
												<!--<div class="input-group-append">
													<button class="input-group-text" id="apm_uploadimg2">Upload</button>
												</div>-->

										</div>
									 </div>
									  
									  <div class="form-group">
										<label for="gender">Gender</label>
										<div class="btn-group btn-group-toggle w-100" data-toggle="buttons">
										  <label class="btn bg-teal active">
											<input type="radio" name="gender" id="male" autocomplete="off" checked> Male
										  </label>
										  <label class="btn bg-teal">
											<input type="radio" name="gender" id="female" autocomplete="off"> Female
										  </label>
										</div>
									  </div>
								</div>
							</div>
						</div>
					  
					</div>
					<div class="modal-footer justify-content-between">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
		

		<div class="popup thm-lite" id="modal-close">
				<div class="box-middle">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="modal-title t-black mb20-xs">Do you want to close Application?</h4>
					</div>
					<div class="modal-body txt-c">
					  <p>คุณต้องการออกจากระบบการส่งข้อความใช่ไหม?</p>
					</div>
					<div class="modal-footer p-0 center-xs">
					  <button type="button" data-fancybox-close="" class="ui-btn-gray btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</button>
					  <button type="button" data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Confirm</button>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
	

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script>
//Dropdown plugin data
$(document).ready(function() {
     $('.select2').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		minimumResultsForSearch: -1,
		dropdownAutoWidth : true,
		width: '100%'
	});

	/*toggle radio*/
	$(".btn-group-toggle input[type=radio]").on("click", function() {
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
			$(this).parents(".btn-group-toggle").find('.btn').removeClass('active');
			$(this).parents('.btn').addClass('active');
		}
		

	})
	
});
</script>
<!-- /js -->

</body>
</html>
