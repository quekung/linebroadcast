<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>

<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message" class="selected"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="template-list.php" title="My Templates"><i class="fas fa-layer-group"></i> <span>My Templates</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row _chd-cl-xs-12 _chd-cl-md-09">
							<div class="col-l _self-cl-xs-12-md-09">
								<div class="head-title">
									<h2>Send Message</h2>	
									<p>ระบบส่งข้อความผ่านทาง line</p>
								</div>
								<!-- card -->
								<div class="card">
									<div class="card-header">
										<h3 class="card-title m-0"><span class="step ui-btn-green _self-ph10-mr10">ขั้นตอนที่ 1</span> Select Template</h3>
									</div>
									<div class="card-body">
									
									<ul class="idTabs tab-receiver">
											<li><a href="#template" class="selected"><i class="qic"><img src="di/ic-tpl.png"></i> Template</a></li>
											<li><a href="#newpush" onClick="$('#live-screen .list-sr-chat').empty()"><i class="qic"><img src="di/ic-new-tpl.png"></i> New Push</a></li>
										</ul>
								
									<div id="template" class="accordion select-tpl" id="accordionExample">
										  <div class="card">
											<div class="hd-ac active" id="headingOne">
											  <h2>

												  Template #1 พนักงานใหม่

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												</div>
											</div>

											<div id="collapseOne" class="pane active">
											  <div class="card-body p-0 list-bubble">
													<ul class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo1" id="todoCheck1" checked="">
														  <label for="todoCheck1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>
														<!--<div class="tools">
														  <i class="fas fa-edit text-muted" onClick="$('#edit-template').show();"></i>
														  <i class="fas fa-trash"></i>
														</div>-->
													  </li>
													  <li id="msg2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2" id="todoCheck2" checked="">
														  <label for="todoCheck2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3" id="todoCheck3" checked="">
														  <label for="todoCheck3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo4" id="todoCheck4" checked="">
														  <label for="todoCheck4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo5" id="todoCheck5" checked="">
														  <label for="todoCheck5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
											  </div>
											</div>
										  </div>
										  
										  <div class="card">
											<div class="hd-ac" id="headingTwo">
											  <h2>

												  Template #2 อบรมหลักสูตร

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												</div>
											</div>
											<div id="collapseTwo" class="pane" style="display: none">
											  <div class="card-body p-0 list-bubble">
												<ul class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg2-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-1" id="todoCheck2-1" checked="">
														  <label for="todoCheck2-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-2" id="todoCheck2-2" checked="">
														  <label for="todoCheck2-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-3" id="todoCheck2-3" checked="">
														  <label for="todoCheck2-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-4" id="todoCheck2-4" checked="">
														  <label for="todoCheck2-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo2-5" id="todoCheck2-5" checked="">
														  <label for="todoCheck2-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
											  </div>
											</div>
										  </div>
										  
										  <div class="card">
											<div class="hd-ac" id="headingThree">
											  <h2>

												  Template #3 สิทธิประโยชน์พนักงาน 

											  </h2>
											  <div class="tools">
												  <a href="broadcasts-edit.php"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
												</div>
											</div>
											<div id="collapseThree" class="pane" style="display: none">
											   <div class="card-body p-0 list-bubble">
												<ul class="todo-list show ui-sortable" data-widget="todo-list">
													  <li id="msg3-1" class="done">
														<!-- drag handle -->
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<!-- checkbox -->
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-1" id="todoCheck3-1" checked="">
														  <label for="todoCheck3-1"></label>
														</div>
														<!-- todo text -->
														<span class="text">Design a nice theme</span>
														<!-- Emphasis label -->
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-2" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-2" id="todoCheck3-2" checked="">
														  <label for="todoCheck3-2"></label>
														</div>
														<span class="text">Make the theme responsive</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-3" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-3" id="todoCheck3-3" checked="">
														  <label for="todoCheck3-3"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>
													  <li id="msg2-4" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-4" id="todoCheck3-4" checked="">
														  <label for="todoCheck3-4"></label>
														</div>
														<span class="text">Let theme shine like a star</span>
														<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>

													  </li>
													  <li id="msg2-5" class="done">
														<span class="handle ui-sortable-handle">
														  <i class="fas fa-ellipsis-v"></i>
														  <i class="fas fa-ellipsis-v"></i>
														</span>
														<div class="icheck-primary d-inline ml-2">
														  <input type="checkbox" value="" name="todo3-5" id="todoCheck3-5" checked="">
														  <label for="todoCheck3-5"></label>
														</div>
														<span class="text">Check your messages and notifications</span>
														<small class="badge badge bg-teal"><i class="far fa-image"></i> Image</small>

													  </li>

													</ul>
											  </div>
											</div>
										  </div>
										</div>
									<div id="newpush" class="bg-white ps-rlt _self-pa10 cb-af" style="display: none">
										<!-- Tab -->
										<ul class="msg-tab idTabs">
											<li class="nav-item">
											<a class="selected" id="vert-tabs-text-tab" href="#vert-tabs-text">
												<span class="btn bg-info mb-0 mr-1"><i class="qic-big-text"></i></span>
												<div>Text</div></a>
											</li>

											<li class="nav-item">
											  <a class="nav-link" id="vert-tabs-image-tab" href="#vert-tabs-image">
												<span class="btn bg-teal mb-0 mr-1"><i class="qic-big-img"></i></span>
												<div>Image</div></a>
										  </li>

										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-map-tab" href="#vert-tabs-map">
												<span class="btn bg-purple mb-0 mr-1"><i class="qic-big-map"></i></span>
												<div>Image Map</div></a>
										  </li>

										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-flex-tab" href="#vert-tabs-flex">
												<span class="btn bg-maroon mb-0 mr-1"><i class="qic-big-carousel"></i></span>
												<div>Carousel</div></a>
										  </li>

										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-button-tab" href="#vert-tabs-button">
												<span class="btn bg-primary mb-0 mr-1"><i class="qic-big-button"></i></span>
												<div>Button</div></a>
										  </li>
										  
										  <li class="nav-item">
											  <a class="nav-link" id="vert-tabs-payload-tab" href="#vert-tabs-payload">
												<span class="btn bg-primary mb-0 mr-1"><i class="qic-big-payload"></i></span>
												<div>Payload</div></a>
										  </li>

										  <!--<li class="nav-item m-adv">
											<button type="button" class="bg-gray btn-sm dropdown-toggle">
												  <i class="fas fa-ellipsis-v"></i></button>
												<a class="nav-link" id="vert-tabs-payload-tab" href="#vert-tabs-payload" aria-controls="vert-tabs-payload" aria-selected="false">
														<span class="btn bg-olive mb-0 mr-1"><i class="fas fa-comment-dots"></i></span>
														Payload</a>
										  </li>-->
										</ul>


										<div class="tab-content" id="vert-tabs-tabContent">
										  <div class="tab-pane show" id="vert-tabs-text" >

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
																			  <!-- textarea -->
												  <div class="form-group">
													<label for="message_text">Your Message</label>
													<textarea class="form-control" id="message_text" rows="3" placeholder="Enter ..."></textarea>
													<textarea class="form-control" id="message_edittext" rows="3" placeholder="Enter ..." style="display: none;"></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaytxt" class="ui-btn  btn-info btn-lg float-right btn-add-msg">Display</button>
											</div>
										  </div>

										  <div class="tab-pane fade" id="vert-tabs-image">
											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">
											 	
												<div><label for="option_imgmap">Select Type</label></div>
												<!--<div class="btn-group btn-group-toggle mb-3" data-toggle="buttons">-->
												<div class="chk-type-imagemap">
												  <div class="ui-btn-border btn-sm icheck-primary _self-mr10">
													  <input type="radio" name="option_imgmap" id="option_image" class="mr-1" value="0" autocomplete="off" checked="" onClick="$('.chk-type-imagemap .btn-sm').removeClass('active'); $(this).parent().addClass('active')">
													  <label for="option_image">Image</label>
												  </div>
												  <div class="ui-btn-border btn-sm icheck-primary">
													<input type="radio" name="option_imgmap" id="option_vdo" class="mr-1" value="1" autocomplete="off" onClick="$('.chk-type-imagemap .btn-sm').removeClass('active'); $(this).parent().addClass('active')"> 
													<label for="option_vdo">Video</label>
												</div>
												</div>
												<!--</div>-->
												
												<div id="for-image" class="main-imagemap wrap-upload">
												<div class="form-group p-0">
													<label>Select Type Image</label>
													<select class="custom-select" id="select-image-type">
													  <option value="0">URL</option>
													  <option value="1">Local file</option>
													</select>
												 </div>

												<div id="f-url" class="f-url form-group">  
													<label for="exampleInputFile">From image URL</label>
													<div class="input-group mb-3">
													  <div class="input-group-prepend">
														<span class="input-group-text"><i class="fas fa-link"></i></span>
													  </div>
													  <input type="text" class="form-control" placeholder="URL" id="message_urlimg">
													</div>
												</div>

												<div id="f-upload" class="f-upload form-group" style="display: none">
													<label for="exampleInputFile">Upload image</label>
													<div class="group-upload d-flex middle-xs">
														<div class="show-thumb _self-cl-pr10 hid"><img id="temp_image_src" /><span class="delete"></span></div>
														<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input" id="message_img" accept=".jpg,.jpeg,.png">
																	<!--<img id="temp_image_src" hidden/>-->
																	<label class="custom-file-label" id="labelimg" for="message_img"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button type="button" class="input-group-text btn-up-file" id="uploadimg">Upload</button>
																</div>
														</div>
													</div>
													<script>
														//group-upload
														$( document ).ready( function () {
															$('.group-upload .btn-up-file').click(function() {
																$(this).parents('.group-upload').find('.show-thumb').removeClass('hid');
															});
															$('.show-thumb .delete').click(function() {
																$(this).parents('.show-thumb').addClass('hid').find('img').attr('src', '');
															});
														});
													</script>
												 </div>
												</div>
												
												
												<div id="for-vdo" class="f-imagevdo _self-pa10 bg-light rounded">

													<p class="head">Video Properties</p>

													<div class="tab-custom-content wrap-upload pt-3">
														<div class="form-group p-0">
															<label>Select Type Video</label>
															<select class="custom-select" id="select-vdo-type">
															  <option value="0">URL</option>
															  <option value="1">Local file</option>
															</select>
														 </div>


														<div id="f-url2" class="f-url form-group">  
															<label for="message_urlimg2">From Video URL</label>
															<div class="input-group mb-3">
															  <div class="input-group-prepend">
																<span class="input-group-text"><i class="fas fa-link"></i></span>
															  </div>
															  <input type="text" class="form-control" placeholder="URL Video" id="message_urlimg2">
															</div>
														</div>

														<div id="f-upload2" class="f-upload form-group" style="display: none">
															<label for="message_img2">Upload Video</label>
															<div class="group-upload d-flex middle-xs">
																<div class="show-thumb _self-cl-pr10 hid"><img id="temp_image_src" /><span class="delete"></span></div>
																<div class="input-group">
																		<div class="custom-file">
																			<input type="file" class="custom-file-input2" id="message_img2" accept=".mp4,.mpeg4,.ogg,.wmv">
																			<!--<img id="temp_image_src" hidden/>-->
																			<label class="custom-file-label" id="labelimg2" for="message_img2"><p>Choose file</p></label>
																		</div>
																		<div class="input-group-append">
																			<button type="button" class="input-group-text btn-up-file" id="uploadimg2" <?php /*?>onClick="$(this).parents('.group-upload').find('.show-thumb').removeClass('hid');"<?php */?>>Upload</button>
																		</div>
																</div>
															</div>
															<!--<div class="input-group">
																	<div class="custom-file">
																		<input type="file" class="custom-file-input2" id="message_img2" accept=".mp4,.ogg">
																		<img id="temp_image_src" hidden/>
																		<label class="custom-file-label" id="labelimg2" for="message_img2"><p>Choose file</p></label>
																	</div>
																	<div class="input-group-append">
																		<button class="input-group-text" id="uploadimg2">Upload</button>
																	</div>

															</div>-->
														 </div>
													</div>
													
													<div class="tab-custom-content wrap-upload pt-2">														 
														 <div class="main-imagemap">
															<div class="form-group p-0">
																<label>Select Type Preview Image URL</label>
																<select class="custom-select" id="select-previewimage-type">
																  <option value="0">URL</option>
																  <option value="1">Local file</option>
																</select>
															 </div>

															<div id="f-url" class="f-url form-group">  
																<label for="exampleInputFile">From image URL</label>
																<div class="input-group mb-3">
																  <div class="input-group-prepend">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																  <input type="text" class="form-control" placeholder="URL" id="message_urlpreviewimg">
																</div>
															</div>

															<div id="f-upload" class="f-upload form-group" style="display: none">
																<label for="exampleInputFile">Upload image</label>
																<div class="group-upload d-flex middle-xs">
																	<div class="show-thumb _self-cl-pr10 hid"><img id="temp_previewimage_src" /><span class="delete"></span></div>
																	<div class="input-group">
																			<div class="custom-file">
																				<input type="file" class="custom-file-input" id="message_previewimg" accept=".jpg,.jpeg,.png">
																				<!--<img id="temp_image_src" hidden/>-->
																				<label class="custom-file-label" id="labelpreviewimg" for="message_previewimg"><p>Choose file</p></label>
																			</div>
																			<div class="input-group-append">
																				<button type="button" class="input-group-text btn-up-file" id="uploadpreviewimg" <?php /*?>onClick="$(this).parents('.group-upload').find('.show-thumb').removeClass('hid');"<?php */?>>Upload</button>
																			</div>
																	</div>
																</div>
															 </div>
															</div>
													</div>

												</div>
												

											 </div>

											 <div class="tab-footer clearfix">
												<button type="button" id="displayimg" class="ui-btn  btn-info btn-lg float-right btn-add-image">Display</button>
											</div>

										  </div>

										  <div class="tab-pane fade" id="vert-tabs-map">
											<p class="lead mb-0">Properties</p>
											<div class="tab-custom-content">
												<?php /*?><div><label for="option_imgmap">Select Type</label></div>
												<!--<div class="btn-group btn-group-toggle mb-3" data-toggle="buttons">-->
												<div class="chk-type-imagemap">
												  <div class="ui-btn btn-xs bg-teal active icheck-white _self-mr10">
													  <input type="radio" name="option_imgmap" id="option_image" class="mr-1" value="0" autocomplete="off" checked="" onClick="$('.chk-type-imagemap .btn').removeClass('active'); $(this).parent().addClass('active')">
													  <label for="option_image" class="t-white">Image</label>
												  </div>
												  <div class="ui-btn btn-xs bg-teal icheck-white">
													<input type="radio" name="option_imgmap" id="option_vdo" class="mr-1" value="1" autocomplete="off" onClick="$('.chk-type-imagemap .btn').removeClass('active'); $(this).parent().addClass('active')"> 
													<label for="option_vdo" class="t-white">Video</label>
												</div>
												</div>
												<!--</div>--><?php */?>

												<div id="main-image" class="main-imagemap wrap-upload">
													<div class="form-group p-0">
														<label>Select Type Image</label>
														<select class="custom-select" id="select-mainimage-type">
														  <option value="0">URL</option>
														  <option value="1">Local file</option>
														</select>
													 </div>

													<div id="f-url_map" class="f-url form-group">  
														<label for="map_urlimg2">From image URL</label>
														<div class="input-group mb-3">
														  <div class="input-group-prepend">
															<span class="input-group-text"><i class="fas fa-link"></i></span>
														  </div>
														  <input type="text" class="form-control" placeholder="URL Image" id="map_urlimg2">
														</div>
													</div>

													<div id="f-upload_map" class="f-upload form-group" style="display: none">
														<label for="map_img2">Upload image</label>
														<div class="group-upload d-flex middle-xs">
														<div class="show-thumb _self-cl-pr10 hid"><img src="di/bg-2019.jpg"><span class="delete"></span></div>
														<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input2" id="map_img2" accept=".jpg,.jpeg,.png">
																	<img id="temp_mapimage_src" hidden/>
																	<label class="custom-file-label" id="map_labelimg2" for="map_img2"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button type="button" class="input-group-text btn-up-file" id="map_uploadimg2">Upload</button>
																</div>
														</div>
													</div>
														<!--<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input2" id="map_img2" accept=".jpg,.jpeg,.png">
																	<img id="temp_mapimage_src" hidden/>
																	<label class="custom-file-label" id="map_labelimg2" for="map_img2"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button class="input-group-text" id="map_uploadimg2">Upload</button>
																</div>

														</div>-->
													 </div>
												</div>

												<div class="f-imagemap _self-pa10 bg-light rounded">
												<p class="head">Image Properties</p>

												<div class="tab-custom-content wrap-upload pt-3">
													<div class="form-group p-0">
														<div><label>Select Layout</label></div>

														<div class="tab-layout btn-group btn-group-toggle mb-3">
														  <label class="btn">
															<input type="radio" name="option_layoutmap" id="option_layout1"  value="0" autocomplete="off" checked=""> <i class="ic-frame d-block mt-1 mb-1"><img src="di/icon-fullframe.png" height="60"></i> <div>Full width</div>
														  </label>
														  <label class="btn">
															<input type="radio" name="option_layoutmap" id="option_layout2h"  value="1" autocomplete="off"> <i class="ic-frame d-block mt-1 mb-1"><img src="di/icon-2h.png" height="60"></i> <div>2 Horizontal</div>
														  </label>

														  <label class="btn">
															<input type="radio" name="option_layoutmap" id="option_layout2v"  value="2" autocomplete="off"> <i class="ic-frame d-block mt-1 mb-1"><img src="di/icon-2v.png" height="60"></i> <div>2 Vertical</div>
														  </label>

														  <label class="btn">
															<input type="radio" name="option_layoutmap" id="option_layout4p"  value="3" autocomplete="off"> <i class="ic-frame d-block mt-1 mb-1"><img src="di/icon-4part.png" height="60"></i> <div>Divide 4 parts</div>
														  </label>
														</div>
													</div>

														<div class="imt-1 lay-sw">
															<label class="t-green">Full width</label>
															<div class="tab-custom-content">
															<div class="form-group">
																<label>Link URL</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>
															  </div>
														</div>

														<div class="imt-2 lay-sw">
															<label class="t-green">2 Horizontal</label>
															<div class="tab-custom-content">
															<div class="row">
															  <div class="form-group col-lg-6">
																<label>Link URL Left</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 1 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>

															  <div class="form-group col-lg-6">
																<label>Link URL Right</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 2 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>
															  </div>

															  </div>
														</div>

														<div class="imt-3 lay-sw">
															<label class="t-green">2 Vertical</label>
															<div class="tab-custom-content">
															<div class="form-group">
																<label>Link URL Top</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 1 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>

															  <div class="form-group">
																<label>Link URL Bottom</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 2 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>
															  </div>
														</div>

														<div class="imt-4 lay-sw">
															<label class="t-green">Divide 4 parts</label>
															<div class="tab-custom-content">
															<div class="row">
															  <div class="form-group col-lg-6">
																<label>Link URL Top Left</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 1 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>

															  <div class="form-group col-lg-6">
																<label>Link URL Top Right</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 2 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>
															  </div>

															  <div class="row">
															  <div class="form-group col-lg-6">
																<label>Link URL Bottom Left</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 3 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>

															  <div class="form-group col-lg-6">
																<label>Link URL Bottom Right</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action 4 URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															  </div>
															  </div>

															  </div>
														</div>

													</div>

												</div>
												
<?php /*?>
												<div id="for-vdo" class="f-imagevdo _self-pa10 bg-light rounded">

												<p class="head">Video Properties</p>

												<div class="tab-custom-content wrap-upload pt-3">
													<div class="form-group p-0">
														<label>Select Type Video</label>
														<select class="custom-select" id="select-vdo-type">
														  <option value="0">URL</option>
														  <option value="1">Local file</option>
														</select>
													 </div>


													<div id="f-url2" class="f-url form-group">  
														<label for="message_urlimg2">From Video URL</label>
														<div class="input-group mb-3">
														  <div class="input-group-prepend">
															<span class="input-group-text"><i class="fas fa-link"></i></span>
														  </div>
														  <input type="text" class="form-control" placeholder="URL Video" id="message_urlimg2">
														</div>
													</div>

													<div id="f-upload2" class="f-upload form-group" style="display: none">
														<label for="message_img2">Upload Video</label>
														<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input2" id="message_img2" accept=".mp4,.ogg">
																	<img id="temp_image_src" hidden/>
																	<label class="custom-file-label" id="labelimg2" for="message_img2"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button class="input-group-text" id="uploadimg2">Upload</button>
																</div>

														</div>
													 </div>
												</div>

												</div><?php */?>

											 </div>

											 <div class="tab-footer clearfix_self-mt20">
												<button type="button" id="displayimg" class="ui-btn btn-info btn-lg float-right btn-add-imagemap">Display</button>
											</div>

										  </div>
										  <!-- carousel -->
										  <div class="tab-pane fade" id="vert-tabs-flex">
											<p class="lead mb-0">Properties</p>
											<div class="tab-custom-content">
												 <div class="form-group">
													<label>Carousel Title</label>
													<input type="text" class="form-control" id="carouseltitle" placeholder="Title">
												</div>
												<div class="row">
												  <div class="max-w-tab col-5 col-xl-3 col-xxl-2 col-lg-4">

													<!--<div class="nav nav-tabs cs-flex-tab flex-column h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">-->
													<ul class="idTabs cs-flex-tab flex-column h-100" id="custom-carousel-tab" role="tablist" aria-orientation="vertical">


													  <li class="nav-item"><a class="nav-link selected" id="call-tab1" href="#crs-tab1">Card 1</a> <span> x </span></li>
													  <li class="nav-item"><a class="nav-link" id="call-tab2" href="#crs-tab2">Card 2</a> <span> x </span></li>
													  <li class="_self-mt10 txt-c"><a href="javascript:;" class="add-tab ui-btn-mini btn-sm bg-green btn-block"><i class="qic"><img src="di/ic-new-tpl.png" height="12"></i> Add Card</a></li>

												  </div>
												  <div class="col-7  col-xl col-lg-8">
													<div class="tab-content" id="carousel-tab">
													  <div class="tab-pane text-left fade " id="crs-tab1">
														<!-- card carousel -->
															<div id="main-card1" class="main-card wrap-upload">
																<div class="form-group p-0">
																	<p class="t-green">Card 1</p>
																	<label>Select Type Image 1</label>
																	<select class="custom-select" id="select-card1-type">
																	  <option value="0">URL</option>
																	  <option value="1">Local file</option>
																	</select>
																 </div>

																<div id="f-url_card1" class="f-url form-group">  
																	<label for="card_urlimg1">From image URL</label>
																	<div class="input-group mb-3">
																	  <div class="input-group-prepend">
																		<span class="input-group-text"><i class="fas fa-link"></i></span>
																	  </div>
																	  <input type="text" class="form-control" placeholder="URL Image" id="card_urlimg1">
																	</div>
																</div>

																<div id="f-upload_card1" class="f-upload form-group" style="display: none">
																	<label for="card_img1-1">Upload image</label>
																	<div class="group-upload d-flex middle-xs">
																		<div class="show-thumb _self-cl-pr10 hid"><img src="di/bg-2019.jpg"><span class="delete"></span></div>
																		<div class="input-group">
																				<div class="custom-file">
																					<input type="file" class="custom-file-input1" id="card_img1" accept=".jpg,.jpeg,.png">
																					<img id="temp_card1_src" hidden/>
																					<label class="custom-file-label" id="card_labelimg1" for="card_img1"><p>Choose file</p></label>
																				</div>
																				<div class="input-group-append">
																					<button type="button" class="input-group-text btn-up-file"  id="card_uploadimg1">Upload</button>
																				</div>
																		</div>
																	</div>
																	<!--<div class="input-group">
																			<div class="custom-file">
																				<input type="file" class="card-file-input1" id="card_img1" accept=".jpg,.jpeg,.png">
																				<img id="temp_card1_src" hidden/>
																				<label class="custom-file-label" id="card_labelimg1" for="card_img1"><p>Choose file</p></label>
																			</div>
																			<div class="input-group-append">
																				<button class="input-group-text" id="card_uploadimg1">Upload</button>
																			</div>

																	</div>-->
																 </div>
															</div>
															<div class="form-group pa10-xs bg-light rounded">
																<label>Link URL</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															</div>

															<!-- detail card -->
															<div class="form-group">
																<label>Title Card</label>
																<input type="text" class="form-control" id="titleCard1" placeholder="Title">
															</div>

															<div class="form-group">
																<textarea id="textCardD1" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>
																<textarea id="textCardD2" class="form-control" rows="2" placeholder="Description 2"></textarea>
															</div>

															<div class="form-group">
																<label>Number of Actions</label>
																<select class="form-control select-action-chd" id="select-card1Num-action">
																  <option value="0">1</option>
																  <option value="1">2</option>
																  <option value="2">3</option>
																</select>
															</div>

															<div class="main-apm-action bg-light pa10-xs">
																<div id="card1-a1" class="g-action1">
																	<div class="form-group">
																		<label>Action1</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card1Num-action1">
																				  <option value="0">Message Action</option>
																				  <option value="1">URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard1" placeholder="Action 1">
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard1" placeholder="Action 1">
																			</div>
																		</div>
																	</div>
																</div>

																<div id="card1-a2" class="g-action2">
																	<div class="form-group">
																		<label>Action2</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card2Num-action2">
																				  <option value="0">Message Action</option>
																				  <option value="1" selected>URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard2" placeholder="Action 2">
																			</div>
																		</div>


																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard2" placeholder="URI">
																			</div>
																		</div>
																	</div>
																</div>

																<div id="card1-a3" class="g-action3">
																	<div class="form-group">
																		<label>Action3</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card3Num-action3">
																				  <option value="0">Message Action</option>
																				  <option value="1" selected>URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard3" placeholder="Action 3">
																			</div>
																		</div>


																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard3" placeholder="URI">
																			</div>
																		</div>
																	</div>
																</div>

															</div>
															<!-- /detail card -->

															<!-- /card carousel -->
													  </div>
													  
													  <div class="tab-pane fade" id="crs-tab2">
														 <!-- card carousel -->
															<div id="main-card2" class="main-card wrap-upload">
																<div class="form-group p-0">
																	<p class="t-green">Card 2</p>
																	<label>Select Type Image 2</label>
																	<select class="custom-select" id="select-card2-type">
																	  <option value="0">URL</option>
																	  <option value="1">Local file</option>
																	</select>
																 </div>

																<div id="f-url_card2" class="f-url form-group">  
																	<label for="card_urlimg2">From image URL</label>
																	<div class="input-group mb-3">
																	  <div class="input-group-prepend">
																		<span class="input-group-text"><i class="fas fa-link"></i></span>
																	  </div>
																	  <input type="text" class="form-control" placeholder="URL Image" id="card_urlimg2">
																	</div>
																</div>

																<div id="f-upload_card2" class="f-upload form-group" style="display: none">
																	<label for="card_img2-1">Upload image</label>
																	<div class="group-upload d-flex middle-xs">
																		<div class="show-thumb _self-cl-pr10 hid"><img src="di/bg-2019.jpg"><span class="delete"></span></div>
																		<div class="input-group">
																				<div class="custom-file">
																					<input type="file" class="custom-file-input2" id="card_img2" accept=".jpg,.jpeg,.png">
																					<img id="temp_card2_src" hidden/>
																					<label class="custom-file-label" id="card_labelimg2" for="card_img2"><p>Choose file</p></label>
																				</div>
																				<div class="input-group-append">
																					<button type="button" class="input-group-text btn-up-file" id="card_uploadimg2">Upload</button>
																				</div>
																		</div>
																	</div>
																	<!--<div class="input-group">
																			<div class="custom-file">
																				<input type="file" class="card-file-input2" id="card_img2" accept=".jpg,.jpeg,.png">
																				<img id="temp_card2_src" hidden/>
																				<label class="custom-file-label" id="card_labelimg2" for="card_img2"><p>Choose file</p></label>
																			</div>
																			<div class="input-group-append">
																				<button class="input-group-text" id="card_uploadimg2">Upload</button>
																			</div>

																	</div>-->
																 </div>
															</div>
															<div class="form-group pa10-xs bg-light rounded">
																<label>Link URL</label>
																<div class="input-group">
																  <input type="text" class="form-control" placeholder="Action URL">
																  <div class="input-group-append">
																	<span class="input-group-text"><i class="fas fa-link"></i></span>
																  </div>
																</div>
															</div>
															<!-- detail card -->
															<div class="form-group">
																<label>Title Card</label>
																<input type="text" class="form-control" id="titleCard2" placeholder="Title">
															</div>

															<div class="form-group">
																<textarea id="textCard2D1" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>
																<textarea id="textCard2D2" class="form-control" rows="2" placeholder="Description 2"></textarea>
															</div>

															<div class="form-group">
																<label>Number of Actions</label>
																<select class="form-control select-action-chd" id="select-card2Num-action">
																  <option value="0">1</option>
																  <option value="1">2</option>
																  <option value="2">3</option>
																</select>
															</div>

															<div class="main-apm-action bg-light pa10-xs">
																<div id="card2-a1" class="g-action1">
																	<div class="form-group">
																		<label>Action1</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card2Num-action1">
																				  <option value="0">Message Action</option>
																				  <option value="1">URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard2" placeholder="Action 1">
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard2" placeholder="Action 1">
																			</div>
																		</div>
																	</div>
																</div>

																<div id="card2-a2" class="g-action2">
																	<div class="form-group">
																		<label>Action2</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card2Num2-action2">
																				  <option value="0">Message Action</option>
																				  <option value="1" selected>URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard2-2" placeholder="Action 2">
																			</div>
																		</div>


																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard2-2" placeholder="URI">
																			</div>
																		</div>
																	</div>
																</div>

																<div id="card3-a3" class="g-action3">
																	<div class="form-group">
																		<label>Action3</label>
																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																			<div class="col">
																				<select class="form-control" id="select-card3Num3-action3">
																				  <option value="0">Message Action</option>
																				  <option value="1" selected>URI Action</option>
																				  <option value="2">Postback Action</option>
																				</select>
																			</div>
																		</div>

																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionCard3-3" placeholder="Action 3">
																			</div>
																		</div>


																		<div class="row align-items-center mb10-xs">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																			<div class="col">
																				<input type="text" class="form-control" id="actionLinkCard3-3" placeholder="URI">
																			</div>
																		</div>
																	</div>
																</div>

															</div>
															<!-- /detail card -->
															<!-- /card carousel -->
													  </div>
													  
													

													</div>
												  </div>


												</div>



											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displayimg" class="ui-btn  btn-info btn-lg float-right btn-add-carousel">Display</button>
											</div>

										  </div>
										  <!-- /carousel -->

										  <div class="tab-pane fade" id="vert-tabs-button">

											<p class="lead mb-0">Properties</p>
											 <div class="tab-custom-content">

												<div id="main-apm" class="main-imagemap wrap-upload mb-4">
													<div class="form-group p-0 mb-1">
														<label>Select Type Image</label>
														<select class="custom-select" id="select-apmimage-type">
														  <option value="0">URL</option>
														  <option value="1">Local file</option>
														</select>
													 </div>

													<div id="f-url_apm" class="f-url form-group">  
														<label for="apm_urlimg2">From image URL</label>
														<div class="input-group mb-3">
														  <div class="input-group-prepend">
															<span class="input-group-text"><i class="fas fa-link"></i></span>
														  </div>
														  <input type="text" class="form-control" placeholder="URL Image" id="apm_urlimg2">
														</div>
													</div>

													<div id="f-upload_apm" class="f-upload form-group" style="display: none">
														<label for="apm_img2">Upload image</label>
														<div class="group-upload d-flex middle-xs">
															<div class="show-thumb _self-cl-pr10 hid"><img src="di/bg-2019.jpg"><span class="delete"></span></div>
															<div class="input-group">
																	<div class="custom-file">
																		<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
																		<img id="temp_apmimage_src" hidden/>
																		<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
																	</div>
																	<div class="input-group-append">
																		<button type="button" class="input-group-text btn-up-file" id="apm_uploadimg2">Upload</button>
																	</div>
															</div>
														</div>
														<!--<div class="input-group">
																<div class="custom-file">
																	<input type="file" class="custom-file-input2" id="apm_img2" accept=".jpg,.jpeg,.png">
																	<img id="temp_apmimage_src" hidden="">
																	<label class="custom-file-label" id="apm_labelimg2" for="apm_img2"><p>Choose file</p></label>
																</div>
																<div class="input-group-append">
																	<button class="input-group-text" id="apm_uploadimg2">Upload</button>
																</div>

														</div>-->
													 </div>
												</div>

												 <div class="form-group">
													<input type="text" class="form-control" id="titleApm" placeholder="Title">
												</div>

												<div class="form-group">
													<textarea id="textApm" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>
													<textarea id="textApm2" class="form-control" rows="2" placeholder="Description 2"></textarea>
													<!--<input type="text" class="form-control" id="textApm" placeholder="Text">-->
												</div>

												<div class="form-group">
													<label>Number of Actions</label>
													<select class="form-control select-action-chd" id="select-apmNum-action">
													  <option value="0">1</option>
													  <option value="1">2</option>
													  <option value="2">3</option>
													</select>
												</div>

												<div class="main-apm-action bg-light p_self-pa10">
													<div id="group-a1" class="g-action1">
														<div class="form-group">
															<label>Action1</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action1">
																	  <option value="0">Message Action</option>
																	  <option value="1">URI Action</option>
																	  <option value="2">Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm1" placeholder="Action 1">
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm1" placeholder="Action 1">
																</div>
															</div>
														</div>
													</div>

													<div id="group-a2" class="g-action2">
														<div class="form-group">
															<label>Action2</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action1">
																	  <option value="0">Message Action</option>
																	  <option value="1" selected>URI Action</option>
																	  <option value="2">Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm2" placeholder="Action 2">
																</div>
															</div>


															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm2" placeholder="URI">
																</div>
															</div>
														</div>
													</div>

													<div id="group-a3" class="g-action3">
														<div class="form-group">
															<label>Action3</label>
															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span>
																<div class="col">
																	<select class="form-control" id="select-apmNum-action1">
																	  <option value="0">Message Action</option>
																	  <option value="1" selected>URI Action</option>
																	  <option value="2">Postback Action</option>
																	  <option value="2">Web App Action</option>
																	  <option value="2">Datetime Picker Action</option>
																	</select>
																</div>
															</div>

															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionApm3" placeholder="Action 3">
																</div>
															</div>


															<div class="_flex middle-xs _self-cl-xs-12-mb10">
																<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span>
																<div class="col">
																	<input type="text" class="form-control" id="actionLinkApm3" placeholder="URI">
																</div>
															</div>
														</div>
													</div>

												</div>

											 </div>

											 <div class="tab-footer clearfix _self-mt20">
												<button type="button" id="displayCard" class="ui-btn  btn-info btn-lg float-right btn-add-appointment">Display</button>
											</div>

										  </div>

										  <div class="tab-pane fade" id="vert-tabs-payload">

											<p class="lead mb-0">Custom text</p>
											 <div class="tab-custom-content">
												 <div class="form-group">
													<label>Title</label>
													<input type="text" class="form-control" id="payloadtitle" placeholder="Title">
												</div>
												  <!-- textarea -->
												  <div class="form-group">
													<label>JSON Code</label>
													<textarea class="form-control" id="myCustomText" rows="10" placeholder="Paste code here ..."></textarea>
												  </div>
												  <!-- Link -->

											</div>

											<div class="tab-footer clearfix">
												<button type="button" id="displaypayload" class="ui-btn  btn-info btn-lg float-right btn-add-payload">Display</button>
											</div>
										  </div>
										</div>
										
										<div class="list-bubble">
											<p class="lead mb-0 pt-3">Sortable <small class="text-muted">(Maximun 3 messages)</small></p>
											<div class="tab-custom-content">
											<ul class="todo-list" data-widget="todo-list">


											</ul>
											
											<div class="save-template">
												<div class="icheck-primary chk-tpl text-right mb-0"><input type="checkbox" id="save-tp" class="save-as"> <label for="save-tp">save as template</label></div>
												<div class="set-name-tp" style="display: none">
														<p class="lead mb-0">Properties</p>
														<div class="tab-custom-content">
															 <div class="form-group">
																<label>Template Name</label>
																<div class="input-group mb-3">
																  <input type="text" class="form-control" id="tpl-name" placeholder="กรุณาตั้งชื่อ Template ค่ะ" onKeyPress="$('#btn-save-tpl').prop('disabled',false);">
																  <div class="input-group-append">
																	<button class="ui-btn-sq btn-sm btn bg-info t-white" type="button" id="btn-save-tpl" disabled onClick="$(this).children('i').removeClass('hid'); $('#btn-save-tpl').prop('disabled',true); $('#tpl-name').prop('readonly',true);"><i class="hid fas fa-check-circle"></i> Save</button>
																  </div>
																</div>
															</div>
														</div>
												</div>
											</div>
										</div>
									  </div>
					
									</div>
									</div>
								</div>
								<!-- /card -->

								<!-- card -->
								<div class="card _self-mt30">
									<div class="card-header">
										<h3 class="card-title"><span class="step ui-btn-green _self-ph10-mr10">ขั้นตอนที่ 2</span> Add Receiver</h3>
									</div>
									<div class="card-body">
										<ul class="idTabs tab-receiver">
											<li><a href="#linegroup"><i class="qic"><img src="di/ic-groups.png"></i>  Line Groups</a></li>
											<li><a href="#users"><i class="qic"><img src="di/ic-users.png"></i>  Users</a></li>
										</ul>
										<div class="contentTabs">
											<div class="remain">
												Remaining Messages <input class="showRmUID" type="text" id="countRmainUID" value="34920" readonly>/month
											</div>
											<div id="linegroup" class="form-group _self-pa30 bg-white">

												<label for="datepicker">เลือกกลุ่ม LINE</label>

												<div class="icheck-primary _self_ps-rlt"><input type="checkbox" id="checkAll"> <label for="checkAll">Select All</label></div>
												<div class="js-select wr">
													<select id="e1" class="keep-select-group" name="states[]" multiple="multiple">
													  <!--<option value="0">Select All Group</option>-->
													  <option value="A">Group name A</option>
													  <option value="B">Group name B</option>
													  <option value="C">Group name C</option>
													  <option value="D">Group name D</option>
													  <option value="E">Group name E</option>
													  <option value="F">Group name F</option>
													</select>
													
													<div class="info-container"><small class="info text-muted">*รายการกลุ่ม line ที่มี bot Keepaline <span class="t-red">"Activated"</span> แล้วเท่านั้น</small> </div>
												</div>
											</div>
											
											<div id="users" class="form-group _self-pa30 bg-white">
												<div class="top-bar _flex between-xs middle-xs">
													<label for="byUser">เลือก User ในระบบ</label>
													<div class="sort-bar d-flex flex-nowrap">
														<a id="sw-adv-srh" href="javascript:;" onClick="$(this).toggleClass('active'); $('#advance-srh').slideToggle();"><i class="fas fa-sliders-h"></i></a>
														<!--<select class="form-control select-box" data-placeholder="Type" style="width:auto">
														  <option></option>
														  <option>Name</option>
														  <option>Employee ID</option>
														  <option>Telephone</option>
														  <option>Department</option>
														  <option>Gender</option>
														</select>-->
														<div class="search-sm">
															<input class="txt-box" placeholder="ค้นหา..." style="width: 140px">
															<button type="submit" class="fas fa-search"></button>
														</div>
													</div>
												</div>
												
												<div id="advance-srh" style="display: none">
													<ul>
														<li>
															<label>Department</label>
															<select class="form-control js-adv-select">
															  <option>IT</option>
															  <option>Accounting</option>
															  <option>Finance</option>
															  <option>Sale</option>
															  <option>Maketing</option>
															  <option>Call Center</option>
															  <option>HR</option>
															  <option>Reseption</option>
															</select>
															
														</li>
														
														<li>
															<label>Position</label>
															<select class="form-control js-select-multi" multiple="multiple">
															  <option>Straff</option>
															  <option>Senior</option>
															  <option>Assistant Manager</option>
															  <option>Manager</option>
															  <option>Senior Manager</option>
															  <option>AVP</option>
															  <option>VP</option>
															  <option>MD</option>
															  <option>CEO</option>
															</select>
														</li>
														
														<li>
															<label>Gender</label>
															<select class="form-control js-select-multi" multiple="multiple">
															  <option>Male</option>
															  <option>Female</option>
															  <option>LGBT</option>
															</select>
														</li>
														
														<li>
															<label>Line Group name</label>
															<select class="form-control js-select-multi" multiple="multiple">
															  <option value="A">SWD Project</option>
															  <option value="B">SWD Team</option>
															  <option value="C">SWD Manager</option>
															  <option value="F">SWD ML</option>
															  <option value="D">HR</option>
															  <option value="E">OTO Family</option>
															  <option value="F">OTO benefic &amp; welfare</option>
															</select>
														</li>
														
														<li>
															<label>Age range</label>
															<input id="age_range" type="text" name="" value="" class="form-control bg-white irs-hidden-input hid" tabindex="-1" readonly="">
														</li>
														
														<li class="ctrl-btn txt-c">
															<button type="reset" class="ui-btn-gray2 btn-sm">Reset</button>
															<button type="submit" class="ui-btn-green btn-sm">Submit</button>
														</li>
													</ul>
												</div>
												
												<div class="icheck-primary _self_ps-rlt"><input type="checkbox" id="checkUserAll" class="master-chk"> <label for="checkUserAll">Select All</label></div>
												<div class="list-users">
													<?php for($i=1;$i<=10;$i++){ ?>
													<!-- User -->
													<div class="card _flex top-xs">
														<div class="custom-control icheck-primary _self-pl0">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck1-<?php echo $i; ?>">
															  <label for="userCheck1-<?php echo $i; ?>"></label>
														</div>
														
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a class="_flex mr10-xs" href="javascript:;">
																	<img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40">
																</a>
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading">Fatory Rascal</p>
																</a>
																<p class="col-s">Manager</p>
																<p class="col-s txt-c">IT</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Application <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<span class="badge badge-primary rounded2">Male</span>
																</div>
																<div class="col-auto txt-c">Age 42</div>
															</div>
															
															
														</div>
													</div>
													<!-- /User -->
													<!-- User -->
													<div class="card _flex top-xs">
														<div class="custom-control icheck-primary _self-pl0">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck2-<?php echo $i; ?>">
															  <label for="userCheck2-<?php echo $i; ?>"></label>
														</div>
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a class="_flex mr10-xs" href="javascript:;">
																	<img class="rounded2" src="https://www.w3schools.com/w3images/avatar2.png" alt="Fat Rascal" width="40">
																</a>
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading">Roony Maroon</p>
																</a>
																<p class="col-s">Senior</p>
																<p class="col-s txt-c">Sale</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Supervisor <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<?php  if($i%3==0) {?><span class="badge bg-purple rounded2">LGBT</span><? } else {?><span class="badge badge-primary rounded2">Male</span><? } ?>
																</div>
																<div class="col-auto txt-c">Age 28</div>
															</div>
	
														</div>
													</div>
													<!-- /User -->
													<!-- User -->
													<div class="card _flex top-xs">
														<div class="custom-control icheck-primary _self-pl0">
															  <input class="chd-chk bubble-chk" type="checkbox" value="" name="sentMSG" id="userCheck3-<?php echo $i; ?>">
															  <label for="userCheck3-<?php echo $i; ?>"></label>
														</div>
														<div class="pl-2 _flex between-xs _self-cl-xs">
															<div class="card-body _flex middle-xs">
																<a class="_flex mr10-xs" href="javascript:;">
																	<img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Tims Antony" width="40">
																</a>
																<a href="javascript:;" class="col-l">
																	<p class="list-item-heading mb-0 truncate">Tims Antony</p>
																</a>
																<p class="col-s">Staff</p>
																<p class="col-s txt-c">Accounting</p>
																<p class="col-s txt-c">
																<?php if($i%3==0) {?>R&amp;D
																<?php } elseif($i%5==0) {?>Network
																<?php } else { ?>Admin <?php } ?>
																</p>
																<div class="col-s txt-c">
																	<span class="badge bg-maroon rounded2">Female</span>
																</div>
																<div class="col-auto txt-c">Age 24</div>
															</div>
															
															
														</div>
													</div>
													<!-- /User -->
													<?php } ?>
												</div>
												
												
												<div id="showReceiver" class="mt20-xs status-added justify-content-between" style="">
													<div class="added col">
														<h3 class="bg-green pa10-xs rounded"><i class="fas fa-check-circle mr-2"></i> Selected <small class="t-white">(50 Messages)</small></h3>
														<ul>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
															<li><i class="fas fa-user-plus text-success"></i> Fatory Rascal</li>
															<li><i class="fas fa-user-plus text-success"></i> Roony Maroon</li>
															<li><i class="fas fa-user-plus text-success"></i> Tims Antony</li>
																							
																						</ul>

													</div>
													<?php /*?><div class="unfriend col-sm-5 col-md-3">
														<h3 class="bg-danger pa10-xs rounded"><i class="fas fa-times-circle mr-2"></i> Unable to send message <small class="t-white">(40%)</small></h3>
														<ul>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																							<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
															<li><i class="fas fa-user-times text-muted"></i> 0821837172</li>
																						</ul>
													</div><?php */?>
												  </div>
												
											</div>
										
										</div>
										
									</div>
								</div>
								<!-- /card -->
								<!-- card -->
								<div class="card _self-mt30">
									<div class="card-header">
										<h3 class="card-title"><span class="step ui-btn-green _self-ph10-mr10">ขั้นตอนที่ 3</span> Schedule</h3>
									</div>
									<div class="card-body">
									
									<div class="form-group">
								<div class="bg-white pa20-xs">
								  <div class="form-group clearfix">
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary1" name="chk-send" value="now" checked="">
										<label for="radioPrimary1">
											ส่งตอนนี้
										</label>
									  </div>
								  </div>
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary2" name="chk-send" value="set">
										<label for="radioPrimary2">
											ตั้งค่าวันที่/เวลาส่ง
										</label>
									  </div>
								  </div>
								  

								</div>

								<div id="show-schedule" class="row" style="display: none">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="datepicker">ตั้งค่าวันที่</label>
										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
											<input class="txt-box" type="text" id="datepicker">
											<i class="_self-ml10 far fa-calendar" onClick="$('#datepicker').focus();"></i>
										</div>
									 </div>
									 <!--<div class="txt-c" style="margin-top: -15px">
										<div id="calendarshow" class="w-100"></div>
									 </div>-->
								</div>

								<div class="col-sm-6 d-flex flex-column justify-content-between">

									<!-- time Picker -->
									<div class="bootstrap-timepicker">
									  <div class="form-group">
										<label>ตั้งค่าเวลา</label>

										<div class="_flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
										  <!--<input class="txt-box" type="time" id="appt" name="appt">-->
										  <input class="txt-box" type="text" id="time" placeholder="Time">
										  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
										</div>
										<!-- /.input group -->
									  </div>
									  <!-- /.form group -->
									</div>





								</div>
								</div>

								<div class="mt20 _chd-ph10 center-xs">
									<button type="button" class="ui-btn-gray btn-lg" data-toggle="modal" data-target="#modal-close">Cancel</button>
									<button type="submit" class="ui-btn-green btn-lg swalDefaultSuccess" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Send Message</button>
									<!-- <button type="submit" class="btn btn-danger btn-lg toastrDefaultError mr-2">Send Unsuccessful</button> -->
								</div>

								</div>
									
									</div>
								</div>
								<!-- /card -->
								
							</div>
						</div>
						
						<div class="sidebar-right _self-cl-sm-12-md-03 _flex center-xs">
							<!-- live Mobile -->
							<div id="main-log" class="bx-log active">
<div id="live-screen">
<ul class="list-sr-chat show">
<li class="done messageScreen1">
<input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
<div class="direct-chat-msg">
<div class="direct-chat-img"></div>
<div class="direct-chat-text">text</div>
</div>
</li>
<li class="done messageScreen2">
<input type="hidden" class="done messageScreen2" id="valScreen2" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting">
<div class="direct-chat-msg">
<div class="direct-chat-img"></div>
<div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting"></div>
</div>
</li>
<li class="result-imgmap messageScreen3">
<input type="hidden" class="done messageScreen3" id="valScreen3" value="img|https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting">
<div class="frame-image"><img class="img-fluid pad" src="https://dummyimage.com/500x500/17a3b8/ffffff.png&amp;text=Meeting" alt="Photo">
<div class="mark-link v2"><a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a><a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a></div>
</div>
</li>
<li class="result-carousel messageScreen4">
	<div class="direct-chat-msg"><div class="direct-chat-img"></div></div>
	<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x300/17a3b8/ffffff.png&amp;text=Survey%20demo"></a></figure>
		<div class="detail"> <h2>แบบสำรวจความพึงพอใจในการปฏิบัติงานของพนักงาน ครึ่งปีแรก 2563</h2> 
			<p>ขอความกรุณาพนักงานทุกท่านร่วมประเมิณความพอใจในการปฏิบัติงานของพนักงานค่ะ</p>
		</div>
		<ul>
		<li><a href="#Action1">เริ่มทำแบบสอบถาม</a></li>
		<li><a href="#Action2">ยังก่อน</a></li></ul>
		</div>
		</li>
<li class="result-carousel messageScreen5">
<div class="direct-chat-msg">
<div class="direct-chat-img"></div>
</div>
<div class="flexslider carousel">
<ul class="slides">
<li><a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_cheesecake_brownie.jpg" draggable="false"></a></li>
<li> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_lemon.jpg" draggable="false"></a></li>
<li> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_donut.jpg" draggable="false"></a></li>
<li> <a href="#listaction"><img src="http://flexslider.woothemes.com/images/kitchen_adventurer_caramel.jpg" draggable="false"></a></li>
</ul>
</div>
</li>
<li class="done messageScreen6">
<input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|test">
<div class="direct-chat-msg">
<div class="direct-chat-img"></div>
<div class="direct-chat-text">Custom Message!</div>
</div>
</li>
</ul>
</div>
</div>
							<?php /*?><div id="main-log" class="bx-log active">
							<div id="live-screen">
								<ul class="list-sr-chat show">
									<li id="result-msg1">
										<div class="direct-chat-msg">
											<!--<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-name float-left">Alexander Pierce</span>
											  <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
											</div>-->
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text">
											  Is this template really for free? That's unbelievable!
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									<li id="result-msg2">
										<div class="direct-chat-msg">
											<!-- /.direct-chat-infos -->
											<img class="direct-chat-img" src="https://ui.beurguide.net/LNS/dist/img/avatar-admin.png" alt="Message User Image">
											<!-- /.direct-chat-img -->
											<div class="direct-chat-text direct-chat-image">
											  <img class="img-fluid pad" src="https://ui.beurguide.net/LNS/dist/img/photo3.jpg" alt="Photo">
											</div>
											<!-- /.direct-chat-text -->
										  </div>
									</li>

									
								</ul>
							</div>
						</div><?php */?>
							<!-- /live Mobile -->
						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
  $( function() {
     $( ".todo-list" ).sortable({
      placeholder: "ui-state-highlight",
	  items: "li:not(.ui-state-disabled)",
	  //cancel: ".ui-state-disabled"
    });
    $( ".todo-list li" ).disableSelection();
  } );
</script>

<script>
/*upload*/
$( document ).ready( function () {
	var count_message = 0;
	
	$('.custom-select').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$(this).parents('.wrap-upload').find('.f-url').show();
			$(this).parents('.wrap-upload').find('.f-upload').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$(this).parents('.wrap-upload').find('.f-upload').show();
			$(this).parents('.wrap-upload').find('.f-url').hide();
		}

	});
	
	$('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	
	
	// jQuery UI sortable for the todo list
	  /*$('.todo-list').sortable({
		placeholder         : 'sort-highlight',
		handle              : '.handle',
		forcePlaceholderSize: true,
		zIndex              : 999999
	  })*/
	  
	  // Add Text
	$('.btn-add-msg').click(function() {
		var message_text = $("#message_text").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_text+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_text+'</div>'+
				'</div>'+
			'</li>');
			$("#message_text").val('');
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//addd image
	$('.btn-add-image').click(function() {
		var src_img = '';
		if ($("#select-image-type")[0].selectedIndex == 0)  {
			src_img = $('#message_urlimg').val();
		} else {
			src_img = $('#temp_image_src')[0].src; 
		}
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && src_img != '' ) {
			count_message++;
			if ($('#newpush .todo-list').hasClass('show')) {

			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+src_img+'</span>'+
				'<small class="badge bg-teal"><i class="far fa-image"></i> Image</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text direct-chat-image"><img class="img-fluid pad" src="'+src_img+'"></div>'+
				'</div>'+
			'</li>');
			$('#message_urlimg').val('');
			$('#message_img').val('');
			$('#temp_image_src')[0].src = '';
			$('#displayimg').prop('disabled', true);
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input image');
			}
		}
	});
	//addd imagemap
	/*file Upload*/
	document.getElementById("map_img2").onchange = function () {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    document.getElementById("temp_mapimage_src").src = e.target.result;
                                };
                                reader.readAsDataURL(this.files[0]);
   };  
   /*imagemap*/
$("[name=option_imgmap]").on("click", function() {
		var isCase = this.value;
		if(isCase == 1) {
			//alert("Video");
			$('#for-vdo').show();
			$('#for-image').hide();
		} else {
			//alert("Image");
			$('#for-vdo').hide();
			$('#for-image').show();
		}

	})
	$("[name=option_layoutmap]").on("click", function() {
		var isCase = this.value;
		if(isCase == 0) {
			$('.lay-sw').hide();
			$('.imt-1').show();	
		} else if(isCase == 1) {
			$('.lay-sw').hide();
			$('.imt-2').show();	
		} else if(isCase == 2) {
			$('.lay-sw').hide();
			$('.imt-3').show();	
		} else {
			$('.lay-sw').hide();
			$('.imt-4').show();	
		}
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
		$(".tab-layout .btn").removeClass('active');
		$(this).parents('.btn').addClass('active');
		}
		

	})
	$("[name=option_imgmap]:checked").click();
   
	$('.btn-add-imagemap').click(function() {
		var src_img = '';
		if ($("#select-mainimage-type")[0].selectedIndex == 0)  {
			src_img = $('#map_urlimg2').val();
		} else {
			src_img = $('#map_mapimage_src')[0].src; 
		}
		
		var radios = document.getElementsByName('option_layoutmap');
		var isCase = '';
		
		for (var i = 0, length = radios.length; i < length; i++) {
		  if (radios[i].checked) {
			// do whatever you want with the checked radio
			//alert(radios[i].value);
			isCase = radios[i].value;

			// only one radio can be logically checked, don't check the rest
			break;
		  }
		}

		//alert(isCase);

		
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && src_img != '' ) {
			count_message++;
			if ($('#newpush .todo-list').hasClass('show')) {

			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+src_img+'</span>'+
				'<small class="badge bg-purple"><i class="fas fa-images"></i> Imagemap</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			
			/*$('#message_urlimg').val('');
			$('#message_img').val('');
			$('#temp_image_src')[0].src = '';
			$('#displayimg').prop('disabled', true);*/
			if(isCase == 0) {
				//$('.mark-link').addClass('full-w');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
					'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link"><!--<div class="mark-link v2"><div class="mark-link p4">-->'+
					// 'if(isCase == 1) {'h2'} else if(isCase == 2) {'v2'} else if(isCase == 3) {'p4 '}'
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else if(isCase == 1) {
				//$('.mark-link').addClass('h2');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link h2">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else if(isCase == 2) {
				//$('.mark-link').addClass('v2');	
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link v2">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			} else {
				//$('.mark-link').addClass('p4');
				$('.list-sr-chat').append('<li class="result-imgmap messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="img|'+src_img+'"/>'+
					'<div class="frame-image">'+
					'<img class="img-fluid pad"src="'+src_img+'" alt="Photo">'+
					'<div class="mark-link p4">'+
					'<a href="#test1" class="link-p-1" title="Action1" style="background: #ccc; opacity:.5"></a>'+
					'<a href="#test2" class="link-p-2" title="Action2" style="background: #aaa; opacity:.5"></a>'+
					'<a href="#test3" class="link-p-3" title="Action3" style="background: #888; opacity:.5"></a>'+
					'<a href="#test4" class="link-p-4" title="Action4" style="background: #666; opacity:.5"></a>'+
					'</div>'+
					'</div>'+
				'</li>');
			}
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input image');
			}
		}
	});
	//add carousel
	$('.btn-add-carousel').click(function() {
		function addSlider() {
			  $('.flexslider').flexslider({
				animation: "slide",
				animationLoop: false,
				itemWidth: 210,
				itemMargin: 5,
				minItems: 1,
				maxItems: 3,
				move: 1,
				slideshow: false, 
				start: function(slider){
				  $('body').removeClass('loading');
				}
			  });

			  }
		var myCustomSlide = $("#carouseltitle").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myCustomSlide != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomSlide+'</span>'+
				'<small class="badge bg-maroon"><i class="fas fa-columns"></i> Carousel</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
										'<div class="direct-chat-msg">'+
											'<div class="direct-chat-img"></div>'+
										 '</div>'+
										'<div class="flexslider carousel">'+
										  '<ul class="slides">'+
											'<li class="card">'+
											  '<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=demo"></a></figure><div class="detail"> <h2>header</h2> <p>เวลา : 9.00 น.</p> <p>สถานที่ : อาคาร 3 ชั้น 2 ห้อง 206</p></div><ul><li><a href="#Action1">ยืนยัน</a></li><li><a href="#Action2">ยกเลิก</a></li><li><a href="#Action3">เลื่อนนัด</a></li></ul></div>'+
											'</li>'+
											'<li class="card">'+
											 '<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/00a3b8/ffffff.png&amp;text=demo"></a></figure><div class="detail"> <h2>header</h2> <p>เวลา : 9.00 น.</p> <p>สถานที่ : อาคาร 3 ชั้น 2 ห้อง 206</p></div><ul><li><a href="#Action1">ยืนยัน</a></li><li><a href="#Action2">ยกเลิก</a></li><li><a href="#Action3">เลื่อนนัด</a></li></ul></div>'+
											'</li>'+
											'<li class="card">'+
											'<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fe00b8/ffffff.png&amp;text=demo"></a></figure><div class="detail"> <h2>header</h2> <p>เวลา : 9.00 น.</p> <p>สถานที่ : อาคาร 3 ชั้น 2 ห้อง 206</p></div><ul><li><a href="#Action1">ยืนยัน</a></li><li><a href="#Action2">ยกเลิก</a></li><li><a href="#Action3">เลื่อนนัด</a></li></ul></div>'+
											'</li>'+
											'<li class="card">'+
											'<div class="card-button"><figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea300/ffffff.png&amp;text=demo"></a></figure><div class="detail"> <h2>header</h2> <p>เวลา : 9.00 น.</p> <p>สถานที่ : อาคาร 3 ชั้น 2 ห้อง 206</p></div><ul><li><a href="#Action1">ยืนยัน</a></li><li><a href="#Action2">ยกเลิก</a></li><li><a href="#Action3">เลื่อนนัด</a></li></ul></div>'+
											'</li>'+
										  '</ul>'+
										'</div>'+

			'</li>');
			addSlider();
			$("#carouseltitle").val('');
			
			
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//toggle action number
	
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
	//add appointment
	$('.btn-add-appointment').click(function() {
		var isAction = $('#select-apmNum-action')[0].selectedIndex;
		var myApmTitle = $("#titleApm").val();
		var myApmText = $("#textApm").val();
		var myApmText2 = $("#textApm2").val();
		var myApmAction1 = $("#actionApm1").val();
		var myApmAction2 = $("#actionApm2").val();
		var myApmAction3 = $("#actionApm3").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myApmTitle != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myApmTitle+'</span>'+
				'<small class="badge bg-primary"><i class="fas fa-user-clock"></i> Appointment</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="#Action1">'+myApmAction1+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
					'<li>'+
					'<a href="#Action3">'+myApmAction3+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			}

			$("#titleApm").val('');
			$("#textApm").val('');
			
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add payload
	$('.btn-add-payload').click(function() {
		
		var myCustomText = $("#myCustomText").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 3 && myCustomText != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomText+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-comment-dots"></i> Payload</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+myCustomText+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">Custom Message!</div>'+
				'</div>'+
			'</li>');
			$("#myCustomText").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	  
	  
	$('#message_urlimg').change(function () {
		$('#displayimg').prop('disabled', false);
	});
	// Upload image
	$("#message_img").change(function () {
		var file = $('#message_img')[0].files[0];
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>'+file.name+'</p>');
	});
	$('#displayimg').click(function () {
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>Choose file</p>');
	});
	$('#uploadimg').click(function () {
		var error_upload = [];
		error_upload[1] = 'Only file type JPG or PNG';
		error_upload[2] = 'File size Maximum 3 MB';
		var url = "https://api.innohub.me/pnp/upload";
		var chk_error = 0;
		var file = $('#message_img')[0].files[0];
		if (file.type != 'image/jpeg' && file.type != 'image/png') {
			chk_error = 1;
		} else if (file.size > 5*1024000) {
			chk_error = 2;
		}
		if (chk_error != 0) {
			alert(error_upload[chk_error]);
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#temp_image_src').attr('src', e.target.result);
		}
		reader.readAsDataURL(file);
		var formdata = new FormData();
		formdata.append("upload", file);
		$('#displayimg').append(' <i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			type: 'POST',
			url: url,
			data: formdata,
			success: function(data){
				if ( data.statusdesc == 'SUCCESS' ) {
					$('#displayimg').prop('disabled', false);
					$('#displayimg').find('i').remove();
					$('#temp_image_src').prop('src', data.url);
				}
			},
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
	});
	
	
	
	//live 
	/*$('.call-log').click(function() {
		if ($('#dxy').hasClass( 'open' )) {
			$('#main-log').removeClass('active');
			$('#dxy').removeClass('open');
			$('#dxy').addClass('close');
			$('.call-log').removeClass('active');
		} else {
		   $('#main-log').addClass('active');
		   $('#dxy').toggleClass('open');
		   $('#dxy').removeClass('close');
		   $(this).toggleClass('active');
		}
    });*/
	

		/*toggle save template*/
		$('.save-as').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$(".set-name-tp").fadeIn(100);
				$("#tpl-name").prop("disabled",false); 
				$("#tpl-name").prop("required",true); 
			} else {
				$(".set-name-tp").fadeOut(100);
				$("#tpl-name").prop("disabled",true);
				$("#tpl-name").prop("required",false);
			}
		});

	  
});

</script>


<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  /*send check type*/
	  $('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	//select2
	$(".select2").select2();
	$(".select-box").select2({dropdownAutoWidth : '70',minimumResultsForSearch: -1});
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	/*Groups all*/
	$("#checkAll").click(function(){
		if($("#checkAll").is(':checked') ){
			$("#e1 > option").prop("selected","selected");
			$("#e1").trigger("change");
		}else{
			$("#e1 > option").removeAttr("selected");
			$("#e1").trigger("change");
		 }
	});
	/*users all*/
	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});
	/*count remain*/
	//var credit = 3;
	//var count = 1;
	var checkboxes = $('.bubble-chk');
	var remain = $('#countRmainUID').val();
	checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
		//alert(current);
		$('#countRmainUID').val(remain - current);
		if(current >= remain) {
		   this.checked = false;
		   checkboxes.filter(':not(:checked)').prop("disabled", true);
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
        //checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
	
	/*$('.bubble-chk').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= remain) {
		   this.checked = false;
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
	   count = $(this).siblings(':checked').length + 1;
	   alert(count);
	   $('#countRmainUID').val(remain - count);
	});*/

});
  </script>
  
  <script>
$( document ).ready( function () {

/*tab carousel*/
$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-tab')) {
            $(this).tab('show');
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".cs-flex-tab li").children('a').first().click();
    });


$('.add-tab').click(function (e) {
    e.preventDefault();
    var id = $(".cs-flex-tab").children().length; //think about it ;)
    var tabId = 'card_' + id;
	/*var startId = 'start: ' + id;
	var settings = { startId , change:false }; 
	alert ( startId );
	alert ( settings );*/
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Card ' + id + ' </a> <span> x </span></li>');
    //$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
	var clonecard= '<div id="main-card3" class="main-card wrap-upload">';
	clonecard+='<div class="form-group p-0">';
	clonecard+='<p class="t-green">Card ' + id + '</p>';
	clonecard+='<label>Select Type Image ' + id + '</label>';
	clonecard+='<select class="custom-select" id="select-card' + id + '-type">';
	clonecard+='<option value="0">URL</option>';
	clonecard+='<option value="1">Local file</option>';
	clonecard+='</select>';
	clonecard+='</div>';

	clonecard+='<div id="f-url_card' + id + '" class="f-url form-group">  ';
	clonecard+='<label for="card_urlimg' + id + '">From image URL</label>';
	clonecard+='<div class="input-group mb-3">';
	clonecard+='<div class="input-group-prepend">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='<input type="text" class="form-control" placeholder="URL Image" id="card_urlimg' + id + '">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="f-upload_card' + id + '" class="f-upload form-group" style="display: none">';
	clonecard+='<label for="card_img' + id + '-1">Upload image</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<div class="custom-file">';
	clonecard+='<input type="file" class="card-file-input' + id + '" id="card_img' + id + '" accept=".jpg,.jpeg,.png">';
	clonecard+='<img id="temp_card' + id + '_src" hidden/>';
	clonecard+='<label class="custom-file-label" id="card_labelimg' + id + '" for="card_img' + id + '"><p>Choose file</p></label>';
	clonecard+='</div>';
	clonecard+='<div class="input-group-append">';
	clonecard+='<button class="input-group-text" id="card_uploadimg' + id + '">Upload</button>';
	clonecard+='</div>';

	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='<div class="form-group pa10-xs bg-light rounded">';
	clonecard+='<label>Link URL</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<input type="text" class="form-control" placeholder="Action URL">';
	clonecard+='<div class="input-group-append">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	

	clonecard+='<div class="form-group"><label>Title Card</label>';
	clonecard+='<input type="text" class="form-control" id="titleCard' + id + '" placeholder="Title">';
	clonecard+='</div>';

	clonecard+='<div class="form-group">';
	clonecard+='<textarea id="textCard' + id + 'D1" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>';
	clonecard+='<textarea id="textCard' + id + 'D2" class="form-control" rows="2" placeholder="Description 2"></textarea>';
	clonecard+='</div>';

	clonecard+='<div class="form-group"><label>Number of Actions</label>';
	clonecard+='<select class="form-control select-action-chd" id="select-card' + id + 'Num-action"><option value="0">1</option> <option value="1">2</option><option value="2">3</option></select>';
	clonecard+='</div>';

	clonecard+='<div class="main-apm-action bg-light pa10-xs">';
	clonecard+='<div id="card' + id + '-a1" class="g-action1"><div class="form-group"><label>Action1</label><div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num-action1"><option value="0">Message Action</option><option value="1">URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a2" class="g-action2"><div class="form-group"><label>Action2</label><div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num2-action2"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-2" placeholder="Action 2">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-2" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a3" class="g-action3"><div class="form-group"><label>Action3</label><div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num3-action3"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-3" placeholder="Action 3">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row align-items-center mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-3" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='</div>';

	//write
	//$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
    //$('.cs-flex-tab li:nth-child(' + id + ') a').click();
	//write
	$('#vert-tabs-flex .tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
	$('#custom-carousel-tab').idTabs(); 
    $('#custom-carousel-tab li:nth-child(' + id + ') a').click();
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
});

});
</script>
  

<script>
$( function() {
	
    //$( "#calendarshow" ).datepicker();

		$( "#datepicker" ).datepicker({
		  showOtherMonths: true,
		  selectOtherMonths: true
		});
	//set time	
	var timepicker = new TimePicker('time', {
  lang: 'en',
  theme: 'dark'
});
timepicker.on('change', function(evt) {
  
  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
  evt.element.value = value;

});

  } );
</script>



<!--Plugin CSS file with desired skin-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>   
<!--Plugin JavaScript file-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>

<script>



    $("#age_range").ionRangeSlider({
        type: "double",
        min: 18,
        max: 60,
        from: 25,
        to: 35,
        drag_interval: true,
        min_interval: null,
        max_interval: null
    });
    


</script>
<!-- /js -->

</body>
</html>
