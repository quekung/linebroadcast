<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">		

			<div class="contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending'">
					<div class="head-title txt-l m-0">
						<h2>View Schedule</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
								<!-- card -->
								<div class="card bg-white">
									<div class="card-body d-flex _self-pa20 middle-xs">
										<div id='calendar' style="max-width: 100%"></div>
									</div>
									
									<div class="card-footer">
									<div class="__chd-ph10 center-xs">
											<a href="broadcasts-setting.php" class="ui-btn-gray btn-md"><i class="fas fa-anggle-left"></i> Back</a>
											<a href="broadcasts.php" class="ui-btn-green btn-md"><i class="fas fa-bullhorn"></i> Sent New Message</a>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>


<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">-->
<!-- fullCalendar -->
<link href='js/packages/core/main.css' rel='stylesheet' />
<link href='js/packages/daygrid/main.css' rel='stylesheet' />


<!-- fullCalendar 2.2.5 -->

<script src='js/packages/core/main.js'></script>
<script src='js/packages/interaction/main.js'></script>
<script src='js/packages/daygrid/main.js'></script>

<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      //defaultDate: '2021-02-19',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'Day Event',
          start: '2021-02-01T09:00:00',
		  color: '#257e4a',
		  url: 'broadcasts-report-detail.php',
        },
        {
          title: 'Long Event',
          start: '2021-02-07T08:30:00',
          end: '2021-02-10T16:30:00',
		  url: 'broadcasts-report-detail.php',
        },
        {
          groupId: 999,
          title: 'Repeating Event',
          start: '2021-02-09T16:00:00',
		  color: '#257e4a',
		  url: 'broadcasts-report-detail.php',
        },
        {
          groupId: 999,
          title: 'Repeating Event',
          start: '2021-02-26T16:00:00',
		  url: 'broadcasts-edit.php',
        },
        {
          title: 'Meeting',
          start: '2021-02-19T10:30:00',
          end: '2021-02-19T12:30:00',
		  color: '#257e4a',
		  url: 'broadcasts-report-detail.php',
        },
        {
          title: 'Lunch',
          start: '2021-02-19T12:00:00',
		  color: '#257e4a',
		  url: 'broadcasts-report-detail.php',
        },
        {
          title: 'Meeting',
          start: '2021-02-19T14:30:00',
		  url: 'broadcasts-edit.php',
        },
        {
          title: 'Happy Hour',
          start: '2021-02-19T17:30:00',
		  url: 'broadcasts-edit.php',
        },
        {
          title: 'Dinner',
          start: '2021-02-19T20:00:00',
		  url: 'broadcasts-edit.php',
        },
        {
          title: 'Birthday Straff',
          start: '2021-02-13T07:00:00',
		  color: '#f3c100',
		  url: 'broadcasts-report-detail.php'
        },
        {
          title: 'Birthday Straff',
          start: '2021-02-28T07:00:00',
		  color: '#f3c100',
		  url: 'broadcasts-edit.php',
        },
		{
          title: 'Conference',
          start: '2021-02-23T08:00:00',
		  url: 'broadcasts-edit.php',
        },
      ]
    });

    calendar.render();
  });

</script>


<!-- /js -->

</body>
</html>
