<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User Detail"><i class="fas fa-users-cog"></i> <span>User Detail</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report" class="selected"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					<div class="head-title m-0 txt-c">
						<h2>Report by Transections</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="wrap-full">
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header _flex center-xs between-xsh">
										<h3 class="card-title center-xs pl20-xs"><b>Transections report </b></h3>
										<div id="reportrange" class="date-range pr20-xs">
											<i class="fa fa-calendar"></i>&nbsp;
											<span></span> <i class="fa fa-caret-down"></i>
										</div>
										
									</div>
									<div class="card-body _self-pa30 middle-xs">
										
										<div class="table-responsive table-responsive-sm">
											<table class="table table-report">
											  <thead>
											  <tr>
											  <th class="txt-l">TEMPLATE<!--MESSAGE--></th>
							<th align="center" class="txt-c">DELIVERED</th>
							<th align="center" class="txt-c">SUCCESS</th>
												<th align="center" class="txt-c">READ</th>
												<th align="center" class="txt-c">FAILED</th>
							<th style="min-width: 235px;">DELIVERY STATISTICS</th>
							</tr>
											  </thead>
											  <tbody>
											  <tr>
											  <td> Template ลูกค้าใหม่ต้อนรับ <a class="sw-tb-call2line btn bg-green btn-sm rounded-circle ml-2" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next().find('.sub-tmp').slideToggle();"><i class="fas fa-angle-down fa-2x text-white"></i></a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">33.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">33.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 14K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.5K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-4">
								<div class="g-bar mt-2">
									<div class="c1" style="width: 85%"></div>
									<div class="c2" style="width: 15%"></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(70%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-olive" style="width: 70%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>14000 Read
							  <span class="float-right">(29%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-orange" style="width: 29%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>500 Failed (Unsubscribed)
							  <span class="float-right">(70%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-danger" style="width: 3%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>
							<tr class="sub-table">
											  <td class="pa0-xs" colspan="6">
												<div id="sub-template1"  class="sub-tmp pa30-sm pa10-xs bg-gray3"  style="display: none">
													<table class="table table-bordered table-report bg-white">
													  <thead>
													  <tr>
													  <th width="70">&nbsp;</th>
													  <th>MESSAGE</th>
									<th class="txt-c" align="center">DELIVERED</th>
									<th class="txt-c" align="center">SUCCESS</th>
														<th class="txt-c" align="center">READ</th>
														<th class="txt-c" align="center">FAILED</th>
									<th  class="txt-c" align="center" style="min-width: 235px;">DELIVERY STATISTICS</th>
									</tr>
													  </thead>
													<tbody>
													<tr>
											  <td class="txt-c"> <i class="fas fa-receipt fa-2x mr-2"></i></td>
											  <td> ConSend Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">10.2k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">10K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 10K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
								<!--<div class="col-xs-8">
									<div class="g-bar g-apm">
										<div class="c1" style="width: 80%">Accept <em>(80%)</em></div>
													<div class="c3" style="width: 20%">Cancel <em>(20%)</em></div>
									</div>
								</div>-->
								<div class="col-xs-8">
								<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>8000 Accept 
								  <span class="float-right">(80%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 80%"></div>
								  </div>
								</div>
								</a>
									<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>          
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-map-marked-alt fa-2x mr-2"></i></td>
											  <td> Request Location</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">33.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">33.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 14K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.5K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>30000 Read
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>500 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 10%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"> <i class="fas fa-file-image fa-2x mr-2"></i></td>
											  <td> Request Accident Photo</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">21.4k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">20.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													1.3K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>21441 Delivered 
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>20124 Read
							  <span class="float-right">(95%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 95%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>1310 Failed (Unsubscribed)
							  <span class="float-right">(15%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 15%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"><i class="fas fa-calendar-check fa-2x mr-2"></i> </td>
											  <td>Appointment</td>
							<td align="center"><a href="broadcasts-broadcasts-report-detail2.php?fitler=all" class="text-dark text-sm" title="view detail">25.6k</a></td>
							<td align="center"><a href="broadcasts-broadcasts-report-detail2.php?fitler=success" title="view detail"><small class="text-success">25.4K</small></a></td>
												<td align="center"> <a href="broadcasts-broadcasts-report-detail2.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-broadcasts-report-detail2.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K 	
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Confirm 
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Postpone
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-secondary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>
							</td>
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-envelope-open-text fa-2x mr-2"></i></td>
											  <td> Text Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">33.5k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">33.5K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 33.5K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-gray mr-1">
													<!--<i class="fas fa-arrow-down"></i>-->
													0
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(100%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 100%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33500 Read
							  <span class="float-right">(98%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 98%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>0 Failed (Unsubscribed)
							  <span class="float-right">(0%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 0%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>    
							</tr>
																			</tbody>
													</table>
												</div>
											  </td>
							</tr>
							
											  <tr>
											  <td>Template โปรโมทการตลาด 2020 <a class="sw-tb-call2line btn bg-green btn-sm rounded-circle ml-2" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next().find('.sub-tmp').slideToggle();"><i class="fas fa-angle-down fa-2x text-white"></i></a></td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">25.6k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">25.4K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-4">
								<div class="g-bar mt-2">
									<div class="c1" style="width: 70%"></div>
									<div class="c2" style="width: 30%"></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="javascript:;" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>25682 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>20143 Read
							  <span class="float-right">(79%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-orange" style="width: 79%"></div>
							  </div>
							</div>
							</a>
							<a href="javascript:;" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>210 Failed (Unsubscribed)
							  <span class="float-right">(2%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-danger" style="width: 2%"></div>
							  </div>
							</div>
							</a>
							</div>
							</div>   
							</td>
							</tr>
							<tr class="sub-table">
											  <td class="pa0-xs" colspan="6">
												<div id="sub-template2"  class="sub-tmp pa30-sm pa10-xs bg-gray3"  style="display: none">
													<table class="table table-bordered table-report bg-white">
													  <thead>
													  <tr>
													  <th width="70">&nbsp;</th>
													  <th>MESSAGE</th>
									<th class="txt-c" align="center">DELIVERED</th>
									<th class="txt-c" align="center">SUCCESS</th>
														<th class="txt-c" align="center">READ</th>
														<th class="txt-c" align="center">FAILED</th>
									<th  class="txt-c" align="center" style="min-width: 235px;">DELIVERY STATISTICS</th>
									</tr>
													  </thead>
													<tbody>
													<tr>
											  <td class="txt-c"> <i class="fas fa-receipt fa-2x mr-2"></i></td>
											  <td> ConSend Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">10.2k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">10K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 10K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
								<!--<div class="col-xs-8">
									<div class="g-bar g-apm">
										<div class="c1" style="width: 80%">Accept <em>(80%)</em></div>
													<div class="c3" style="width: 20%">Cancel <em>(20%)</em></div>
									</div>
								</div>-->
								<div class="col-xs-8">
								<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>8000 Accept 
								  <span class="float-right">(80%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 80%"></div>
								  </div>
								</div>
								</a>
									<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>          
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-map-marked-alt fa-2x mr-2"></i></td>
											  <td> Request Location</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">33.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">33.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 14K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.5K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>30000 Read
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>500 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 10%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"> <i class="fas fa-file-image fa-2x mr-2"></i></td>
											  <td> Request Accident Photo</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">21.4k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">20.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													1.3K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>21441 Delivered 
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>20124 Read
							  <span class="float-right">(95%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 95%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>1310 Failed (Unsubscribed)
							  <span class="float-right">(15%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 15%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"><i class="fas fa-calendar-check fa-2x mr-2"></i> </td>
											  <td>Appointment</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">25.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">25.4K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K 	
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Confirm 
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Postpone
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-secondary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>
							</td>
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-envelope-open-text fa-2x mr-2"></i></td>
											  <td> Text Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">33.5k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">33.5K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 33.5K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-gray mr-1">
													<!--<i class="fas fa-arrow-down"></i>-->
													0
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(100%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 100%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33500 Read
							  <span class="float-right">(98%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 98%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>0 Failed (Unsubscribed)
							  <span class="float-right">(0%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 0%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>    
							</tr>
																			</tbody>
													</table>
												</div>
											  </td>
							</tr>
							
											  <tr>
											  <td>Template วิธีการเพิ่มเพื่อนทาง LINE <a class="sw-tb-call2line btn bg-green btn-sm rounded-circle ml-2" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next().find('.sub-tmp').slideToggle();"><i class="fas fa-angle-down fa-2x text-white"></i></a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">450</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-success">400</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 200</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=all" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													50
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-4">
								<div class="g-bar mt-2">
									<div class="c1" style="width: 20%"></div>
									<div class="c2" style="width: 80%"></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(75%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-olive" style="width: 75%"></div>
							  </div>
							</div>

							<div class="progress-group mb-0">
							  <small>14000 Read
							  <span class="float-right">(45%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-orange" style="width: 45%"></div>
							  </div>
							</div>

							<div class="progress-group mb-0">
							  <small>50 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-danger" style="width: 10%"></div>
							  </div>
							</div>

							</div>
							</div>  
							</td>
							</tr>
							<tr class="sub-table">
											  <td class="pa0-xs" colspan="6">
												<div id="sub-template4"  class="sub-tmp pa30-sm pa10-xs bg-gray3"  style="display: none">
													<table class="table table-bordered table-report bg-white">
													  <thead>
													  <tr>
													  <th width="70">&nbsp;</th>
													  <th>MESSAGE</th>
									<th class="txt-c" align="center">DELIVERED</th>
									<th class="txt-c" align="center">SUCCESS</th>
														<th class="txt-c" align="center">READ</th>
														<th class="txt-c" align="center">FAILED</th>
									<th  class="txt-c" align="center" style="min-width: 235px;">DELIVERY STATISTICS</th>
									</tr>
													  </thead>
													<tbody>
													<tr>
											  <td class="txt-c"> <i class="fas fa-receipt fa-2x mr-2"></i></td>
											  <td> ConSend Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">10.2k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">10K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 10K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
								<!--<div class="col-xs-8">
									<div class="g-bar g-apm">
										<div class="c1" style="width: 80%">Accept <em>(80%)</em></div>
													<div class="c3" style="width: 20%">Cancel <em>(20%)</em></div>
									</div>
								</div>-->
								<div class="col-xs-8">
								<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>8000 Accept 
								  <span class="float-right">(80%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 80%"></div>
								  </div>
								</div>
								</a>
									<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>          
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-map-marked-alt fa-2x mr-2"></i></td>
											  <td> Request Location</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">33.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">33.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 14K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.5K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>30000 Read
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>500 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 10%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"> <i class="fas fa-file-image fa-2x mr-2"></i></td>
											  <td> Request Accident Photo</td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=all" class="text-dark text-sm" title="view detail">21.4k</a></td>
							<td align="center"><a href="broadcasts-report-detail.php?fitler=success" title="view detail"><small class="text-success">20.1K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													1.3K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="broadcasts-report-detail.php?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>21441 Delivered 
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="broadcasts-report-detail.php?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>20124 Read
							  <span class="float-right">(95%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 95%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>1310 Failed (Unsubscribed)
							  <span class="float-right">(15%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 15%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"><i class="fas fa-calendar-check fa-2x mr-2"></i> </td>
											  <td>Appointment</td>
							<td align="center"><a href="report-detail?fitler=all" class="text-dark text-sm" title="view detail">25.6k</a></td>
							<td align="center"><a href="report-detail?fitler=success" title="view detail"><small class="text-success">25.4K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="report-detail?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K 	
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Confirm 
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Postpone
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-secondary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>
							</td>
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-envelope-open-text fa-2x mr-2"></i></td>
											  <td> Text Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">33.5k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">33.5K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 33.5K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-gray mr-1">
													<!--<i class="fas fa-arrow-down"></i>-->
													0
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="report-detail?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(100%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 100%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33500 Read
							  <span class="float-right">(98%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 98%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>0 Failed (Unsubscribed)
							  <span class="float-right">(0%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 0%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>    
							</tr>
																			</tbody>
													</table>
												</div>
											  </td>
							</tr>
											  <tr>
											  <td>Template Quick menu <a class="sw-tb-call2line btn bg-green btn-sm rounded-circle ml-2" href="javascript:;" title="view more detail" onClick="$(this).toggleClass('active').parents('tr').next().find('.sub-tmp').slideToggle();"><i class="fas fa-angle-down fa-2x text-white"></i></a></td>
							<td align="center"><a href="report-detail?fitler=all" class="text-dark text-sm" title="view detail">87.6k</a></td>
							<td align="center"><a href="report-detail?fitler=all" title="view detail"><small class="text-success">71.4K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 67.6K</small></a></td>
												<td align="center">
												<a href="report-detail?fitler=all" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													2.2K
												  </small>
												</a>
												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-4">
								<div class="g-bar mt-2">
									<div class="c1" style="width: 90%"></div>
									<div class="c2" style="width: 10%"></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<div class="progress-group mb-0">
							  <small>7694 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>

							<div class="progress-group mb-0">
							  <small>6642 Read
							  <span class="float-right">(89%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-orange" style="width: 89%"></div>
							  </div>
							</div>

							<div class="progress-group mb-0">
							  <small>219 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm">
								<div class="progress-bar bg-danger" style="width: 1%"></div>
							  </div>
							</div>

							</div>
							</div>           
							</td>
							</tr>
							<tr class="sub-table">
											  <td class="pa0-xs" colspan="6">
												<div id="sub-template4"  class="sub-tmp pa30-sm pa10-xs bg-gray3"  style="display: none">
													<table class="table table-bordered table-report bg-white">
													  <thead>
													  <tr>
													  <th width="70">&nbsp;</th>
													  <th>MESSAGE</th>
									<th class="txt-c" align="center">DELIVERED</th>
									<th class="txt-c" align="center">SUCCESS</th>
														<th class="txt-c" align="center">READ</th>
														<th class="txt-c" align="center">FAILED</th>
									<th  class="txt-c" align="center" style="min-width: 235px;">DELIVERY STATISTICS</th>
									</tr>
													  </thead>
													<tbody>
													<tr>
											  <td class="txt-c"> <i class="fas fa-receipt fa-2x mr-2"></i></td>
											  <td> ConSend Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">10.2k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">10K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 10K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
								<!--<div class="col-xs-8">
									<div class="g-bar g-apm">
										<div class="c1" style="width: 80%">Accept <em>(80%)</em></div>
													<div class="c3" style="width: 20%">Cancel <em>(20%)</em></div>
									</div>
								</div>-->
								<div class="col-xs-8">
								<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>8000 Accept 
								  <span class="float-right">(80%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 80%"></div>
								  </div>
								</div>
								</a>
									<a href="javascript:;" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>          
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-map-marked-alt fa-2x mr-2"></i></td>
											  <td> Request Location</td>
							<td align="center"><a href="report-detail?fitler=all" class="text-dark text-sm" title="view detail">33.6k</a></td>
							<td align="center"><a href="report-detail?fitler=success" title="view detail"><small class="text-success">33.1K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 14K</small></a></td>
												<td align="center">
												  <a href="report-detail?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.5K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="report-detail?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(90%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 90%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>30000 Read
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>500 Failed (Unsubscribed)
							  <span class="float-right">(10%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 10%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"> <i class="fas fa-file-image fa-2x mr-2"></i></td>
											  <td> Request Accident Photo</td>
							<td align="center"><a href="report-detail?fitler=all" class="text-dark text-sm" title="view detail">21.4k</a></td>
							<td align="center"><a href="report-detail?fitler=success" title="view detail"><small class="text-success">20.1K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="report-detail?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													1.3K
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="report-detail?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>21441 Delivered 
							  <span class="float-right">(85%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 85%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>20124 Read
							  <span class="float-right">(95%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 95%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>1310 Failed (Unsubscribed)
							  <span class="float-right">(15%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 15%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>
							</td>
							</tr>

													<tr>
											  <td class="txt-c"><i class="fas fa-calendar-check fa-2x mr-2"></i> </td>
											  <td>Appointment</td>
							<td align="center"><a href="broadcasts-report-detail2.php?fitler=all" class="text-dark text-sm" title="view detail">25.6k</a></td>
							<td align="center"><a href="broadcasts-report-detail2.php?fitler=success" title="view detail"><small class="text-success">25.4K</small></a></td>
												<td align="center"> <a href="broadcasts-report-detail2.php?fitler=read" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 20K</small></a></td>
												<td align="center">
												  <a href="broadcasts-report-detail2.php?fitler=failed" title="view detail">
												  <small class="text-danger mr-1">
													<i class="fas fa-arrow-down"></i>
													0.2K 	
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Confirm 
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-primary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>4000 Postpone
								  <span class="float-right">(40%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-secondary" style="width: 40%"></div>
								  </div>
								</div>
								</a>
								<a href="broadcasts-report-detail2.php?fitler=appointment" class="text-dark" title="view detail">
								<div class="progress-group mb-0">
								  <small>2000 Cancel
								  <span class="float-right">(20%)</span>
								  </small>
								  <div class="progress progress-sm rounded-2">
									<div class="progress-bar bg-maroon" style="width: 20%"></div>
								  </div>
								</div>
								</a>

								</div>
							</div>
							</td>
							</tr>
													<tr>
											  <td class="txt-c"> <i class="fas fa-envelope-open-text fa-2x mr-2"></i></td>
											  <td> Text Message</td>
							<td align="center"><a href="javascript:;" class="text-dark text-sm" title="view detail">33.5k</a></td>
							<td align="center"><a href="javascript:;" title="view detail"><small class="text-success">33.5K</small></a></td>
												<td align="center"> <a href="report-detail?fitler=all" title="view detail"><small class="text-orange"><i class="fas fa-eye"></i> 33.5K</small></a></td>
												<td align="center">
												  <a href="javascript:;" title="view detail">
												  <small class="text-gray mr-1">
													<!--<i class="fas fa-arrow-down"></i>-->
													0
												  </small>
												  </a>

												</td>
							<td>
							<div class="row center-xs middle-xs">
							<!--<div class="col-xs-8">
								<div class="g-bar g-apm mt-2">	
									<div class="c1" style="width: 50%">Delivered <em>(50%)</em></div>
									<div class="c2" style="width: 40%">Read <em>(40%)</em></div>
									<div class="c3" style="width: 10%">Failed <em>(10%)</em></div>
								</div>
							</div>-->

							<div class="col-xs-8">
							<a href="report-detail?fitler=all" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33582 Delivered 
							  <span class="float-right">(100%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-olive" style="width: 100%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=success" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>33500 Read
							  <span class="float-right">(98%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-orange" style="width: 98%"></div>
							  </div>
							</div>
							</a>
							<a href="report-detail?fitler=failed" class="text-dark" title="view detail">
							<div class="progress-group mb-0">
							  <small>0 Failed (Unsubscribed)
							  <span class="float-right">(0%)</span>
							  </small>
							  <div class="progress progress-sm rounded-2">
								<div class="progress-bar bg-danger" style="width: 0%"></div>
							  </div>
							</div>
							</a>

							</div>
							</div>    
							</tr>
																			</tbody>
													</table>
												</div>
											  </td>
							</tr>


											  </tbody>
											</table>
										  </div>
										
									</div>
									
									<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});

$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
