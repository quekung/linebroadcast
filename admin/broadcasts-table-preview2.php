<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout bg-gray2">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			
					

			
			<div class="contentTabs">
				<div id="tbc-1" class="msg">
					<div class="form-checkout form-sending">
					<div class="head-title m-0 txt-l">
						<h2>Select Category</h2>	
						<p>เลือกหมวดหมู่การสร้างตาราง</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card">
									<div class="card-header _flex center-xs txt-c">
										<h2 class="text-md">ตัวอย่างตารางข้อมูลของคุณ</h2>
									</div>
									
									<div class="contentTabs">
										<!-- All User -->
										<div id="company" class="card-body _self-pt10 middle-xs">
											<h3 class="head t-black"><small class="f-lite">หมวดหมู่ธุรกิจ :</small> Education</h3>
											<div class="bg-white _self-pa20 wrap-add-tb">
												<div class="table-resp off">
													<div class="mb10-xs d-flex middle-xs end-xs">
														<!--<span class="text-sm mr10-xs">
															<div class="icheck-primary ma0">
															<input type="checkbox" name="auto_id" id="auto_id"> 
															<label for="auto_id">สร้างรหัสประจำตัวอัตโนมัติ</label>
														  </div>
														 </span>-->

														<a href="broadcasts-setup.php" class="ui-btn-border-green btn-sm" title="Edit Table"><i class="fas fa-edit"></i> Edit Table</a>
													</div>
													<?php function generateRandomString($length = 10) {
														$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
														$charactersLength = strlen($characters);
														$randomString = '';
														for ($i = 0; $i < $length; $i++) {
															$randomString .= $characters[rand(0, $charactersLength - 1)];
														}
														return $randomString;
													} ?>
													<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
													<thead>
													<tr>
													<th scope="col" align="center">NO.</th>
													<th scope="col" align="center">Line</th>
													<th scope="col" align="center">รหัสในสถานศึกษา</th>
													<th scope="col" align="center">Email</th>
													<th scope="col" align="center">ชื่อ - นามสกุล</th>	
													<th scope="col" align="center">สถานะ</th>
													<th scope="col" align="center">University/School</th>
													<th scope="col" align="center">คณะ/แผนการเรียน</th>
													<th scope="col" align="center">ชั้นปี/ระดับชั้น</th>
													<th scope="col" align="center">สถานภาพ</th>
													<th scope="col" align="center">วันที่เข้าศึกษา</th>
													<th scope="col" align="center">วันสิ้นสุด</th>
													<th scope="col" align="center">Tag</th>
													</tr>
													</thead>
													<tbody>
													
													<?php for($i=1;$i<=10;$i++){ ?>
													<tr>
													<td valign="middle" align="center"><?php echo $i; ?></td>
													<td valign="middle">
														
														<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> <span class="name"><?php echo generateRandomString(); ?></span>
													</td>
													<td valign="middle">591055415<?php echo $i+1; ?></td>
													<td valign="middle">Patchim.n@ku.com</td>
													<td valign="middle">ปัจฉิม นิเทศ</td>
													<td valign="middle"><?php if($i<=8) {?>Student<?php } else {?>Teacher<?php } ?></td>
													<td valign="middle">Kasetsart University</td>
													<td valign="middle">Faculty of Engineering</td>
													<td valign="middle">ปี1</td>
													<td valign="middle">ปกติ</td>
													<td valign="middle">17/04/19</td>
													<td valign="middle">4/17/2023</td>
													<td valign="middle">คณะบริหาร,กรรมการ,สมาชิก</td>
																					
													</tr>
													<?php } ?>
													</tbody>
													</table>
												</div>

											</div>
										</div>

										<div class="sticky-bottom card-footer mt30-md bg-gray2 mf-bottom">
										<div class="__chd-ph10 center-xs">
												<a href="javascript:parent.jQuery.fancybox.close(); " data-fancybox-close="" class="ui-btn-green btn-md" onclick="parent.jQuery.fancybox.close();">Back</a>
										</div>
										</div>
								  
								  </div>
								  
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</div>

			</div>
			
			

		</section>
    </div>
</div>



<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<!-- /js -->
<script type="text/javascript">
var isIframe= (window.location != window.parent.location)? true : false;
if (isIframe)
{
	$('body').addClass('lb');
}
</script>
</body>
</html>
