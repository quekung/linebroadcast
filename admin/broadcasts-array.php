<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row _chd-cl-xs-12 _chd-cl-md-09">

							
							<div id="UserUpload" class="pt30-sm pb30-sm">
								<div class="addCall addFiles bg-white pa20-xs rounded">
										<label>Please Upload file:</label>


											<div class="js-select">
												<div class="input-main d-flex flex-nowarp middle-xs">
													<div class="ic-upload mr20-xs">
													  <i class="qic-big"><img src="di/ic-upload-excell.png" height="80"></i>
													</div>
													<div class="wrap col">
														<div class="custom-file">
															<input type="file" class="custom-file-input" id="upload-message" accept=".XLSX; .CSV; .TSV; .ODS; .XLS">
															<img id="temp_mapimage_src" hidden="">
															<label class="custom-file-label" id="label_file_message" for="upload-message"><p>Choose file</p></label>
														</div>
														<div class="info-container mt10-xs">     <small class="info text-muted">Example Message Array.xls <a class="t-blue" href="#">"download"</a></small> </div>
													</div>
													<!--<div class="input-group-append">
														<button type="button" class="input-group-text" id="btn_message" onclick="$('.main>.col-l').removeClass('hid'); $('.main>.col-c').removeClass('hid'); $('#UserUpload').addClass('hid');">Upload</button>
													</div>-->

												</div>


												
												
												<div class="ctrl-btn txt-c _self-mt30-pt30">
													<!--<input type="submit" id="Cancel" class="ui-btn-green btn-sm" value="เลือกแพ็คเกจอื่น"> -->
														<a href="javascript:window.history.back();" title="เลือกแพ็คเกจอื่น" class="ui-btn-gray"><i class="fas fa-angle-left"></i> Cancel</a> 
														<button type="button" id="Submit" class="ui-btn-green" onclick="$('.main>.col-l').removeClass('hid'); $('.main>.col-c').removeClass('hid'); $('#UserUpload').addClass('hid');">Upload</button>
												</div>
											</div>
								</div>
							</div>

							<div class="col-l hid _self-cl-xs-12-md-09">
								<div class="head-title">
									<h2>Send Message</h2>	
									<p>ระบบส่งข้อความผ่านทาง line</p>
								</div>
								<!-- card -->
								<div class="card">
									<div class="card-header d-flex between-xs">
										<h3 class="card-title"><i class="nav-icon fas fa-user-check"></i> Added <span class="t-red f-normal _self-mh10">30</span> Receiver</h3>
									</div>
									<div class="card-body contentTabs">
									
											<div class="remain">
												Remaining Messages <input class="showRmUID" type="text" id="countRmainUID" value="34920" readonly="">/month
											</div>
	
										<div id="template" class="accordion select-tpl select-array-box" id="accordionArray">
													  <?php for($i=1;$i<=30;$i++) {?>
													  <div class="card">
														<div class="hd-ac <?php if($i==1){ ?>active<?php } ?>" id="heading<?php echo $i; ?>">
														  <h2>
														  	<div class="input-group">
																<div class="input-group-prepend">
																  <i class="fas fa-mobile-alt"></i>
																</div>
															  	<div class="pl10-xs">08<?php if($i>9){ echo(rand(1,9)); } else { echo $i;} ?>23<?php if($i>9){ echo(rand(1,9)); } else { echo $i;} ?>454<?php if($i>9){ echo(rand(1,9)); } else { echo $i;} ?></div>
															</div>

														  </h2>
														  <div class="tools">
															  <a href="javascript:;"><i class="fas fa-edit text-white"></i></a>
															  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
															</div>
														</div>

														<div id="collapse<?php echo $i; ?>" class="pane" <?php if($i>1){ ?>style="display: none"<?php } ?>>

														  <div class="card-body ps-rlt p-0">
														  		<div id="msg-1" class="ary-1 bg-light p_self-pa10">
																	<div class="form-group">
																		<div class="hd d-flex between-xs middle-xs">
																			<label>Bubble Message</label>
																			<small class="icheck-primary chk-tpl text-right mb-0"><input type="radio" id="ms-tpl<?php echo $i; ?>" name="ms-tpl"> <label class="text-sm" for="ms-tpl<?php echo $i; ?>">USED MASTER TEMPLATE</label></small>
																		</div>
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 1</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ประกาศผลสอบ วิชาภาษาไทย">
																			</div>
																			<button class="rounded2 ui-btn-green-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br1').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 2</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="คุณ สมชาย มาติน">
																			</div>
																			<button class="rounded2 ui-btn-green-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br2').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 3</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="รหัสนักเรียน 11250">
																			</div>
																			<button class="rounded2 ui-btn-green-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br3').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>
																		
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 4</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ได้คะแนน">
																			</div>
																			<button class="rounded2 ui-btn-gray-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br4').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>
																		
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 5</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="<?php echo(rand(50,99)); ?>">
																			</div>
																			<button class="rounded2 ui-btn-gray-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br5').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>
																		
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 6</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="คะแนน">
																			</div>
																			<button class="rounded2 ui-btn-gray-mid btn-xs ml10-xs" onClick="$(this).toggleClass('ui-btn-green-mid').toggleClass('ui-btn-gray-mid'); $('.br6').toggleClass('hid');" type="button">&lang;br/&rang;</button>
																		</div>
																	</div>
																</div>

														  </div>
														</div>
													  </div>
													  <?php } ?>

													  <?php /*?><div class="card">
														<div class="hd-ac" id="headingTwo">
														  <h2>

															  <div class="input-group">
																	<div class="input-group-prepend">
																	  <i class="fas fa-mobile-alt"></i>
																	</div>
																	<div class="pl10-xs">0812345678</div>
																</div>

														  </h2>
														  <div class="tools">
															  <a href="javascript:;"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
															  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
															</div>
														</div>
														<div id="collapseTwo" class="pane" style="display: none">
														  <div class="card-body p-0">
														  <div id="msg-2" class="ary-2 bg-light p_self-pa10">
																	<div class="form-group">
																		<label>Bubble Message</label>
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 1</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ประกาศผลสอบ วิชาภาษาไทย">
																			</div>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 2</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="คุณ ชิลลิ ซาร์">
																			</div>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 3</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="รหัสนักเรียน 11251">
																			</div>
																		</div>
																		
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 4</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ได้คะแนน 85 คะแนน">
																			</div>
																		</div>
																	</div>
																</div>
													  	  </div>
														</div>
													  </div>

													  <div class="card">
														<div class="hd-ac" id="headingThree">
														  <h2>

															  <div class="input-group">
																<div class="input-group-prepend">
																  <i class="fas fa-mobile-alt"></i>
																</div>
															  	<div class="pl10-xs">0811414170</div>
															</div>

														  </h2>
														  <div class="tools">
															  <a href="javascript:;"><i class="fas fa-edit text-white" onclick="$('#edit-template').show();"></i></a>
															  <a href="javascript:;" onclick="$(this).parents('.hd-ac').next().remove(); $(this).parents('.hd-ac').remove(); "><i class="fas fa-trash text-white"></i></a>
															</div>
														</div>
														<div id="collapseThree" class="pane" style="display: none">
														   <div class="card-body p-0">
														   <div id="msg-3" class="ary-3 bg-light p_self-pa10">
																	<div class="form-group">
																		<label>Bubble Message</label>
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 1</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ประกาศผลสอบ วิชาภาษาไทย">
																			</div>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 2</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="คุณ ปังปอน นอนหลับ">
																			</div>
																		</div>

																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 3</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="รหัสนักเรียน 11252">
																			</div>
																		</div>
																		
																		<div class="d-flex middle-xs _self-cl-xs-12-mb10">
																			<span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">message 4</span>
																			<div class="col">
																				<input type="text" class="form-control" placeholder="Message" value="ได้คะแนน 54 คะแนน">
																			</div>
																		</div>
																	</div>
																</div>
														  </div>
														</div>
													  </div><?php */?>
													  
													</div>
													
													<div class="mt15-xs d-flex center-xs middle-xs bg-white pa10-xs">
														<a href="javascript:;" title="Upload Again" class="ui-btn-green btn-md" onclick="$('.main>.col-l').addClass('hid'); $('.main>.col-c').addClass('hid'); $('#UserUpload').removeClass('hid');"><i class="fas fa-file-upload"></i> Upload again</a>
													</div>
										
									</div>
								</div>
								<!-- /card -->
								
								<!-- card -->
								<div class="card _self-mt0">
									<div class="card-header">
										<h3 class="card-title"><!--<span class="step ui-btn-green _self-ph10-mr10">ขั้นตอนที่ 3</span>--> <i class="nav-icon fas fa-calendar-alt"></i> Schedule</h3>
									</div>
									<div class="card-body bg-white  pa10-xs">
									
									<div class="form-group">
								<div class="bg-light pa10-xs rounded">
								  <div class="form-group clearfix">
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary1" name="chk-send" value="now" checked="">
										<label for="radioPrimary1">
											ส่งตอนนี้
										</label>
									  </div>
								  </div>
								  <div class="_self-mb10">
									  <div class="icheck-primary">
										<input type="radio" id="radioPrimary2" name="chk-send" value="set">
										<label for="radioPrimary2">
											ตั้งค่าวันที่/เวลาส่ง
										</label>
									  </div>
								  </div>
								  

								</div>

								<div id="show-schedule" class="row" style="display: none">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="datepicker">ตั้งค่าวันที่</label>
										<div class="d-flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
											<input class="txt-box" type="text" id="datepicker">
											<i class="_self-ml10 far fa-calendar" onClick="$('#datepicker').focus();"></i>
										</div>
									 </div>
									 <!--<div class="txt-c" style="margin-top: -15px">
										<div id="calendarshow" class="w-100"></div>
									 </div>-->
								</div>

								<div class="col-sm-6 d-flex flex-column between-xs">

									<!-- time Picker -->
									<div class="bootstrap-timepicker">
									  <div class="form-group">
										<label>ตั้งค่าเวลา</label>

										<div class="d-flex nowrap middle-xs date" id="timepicker" data-target-input="nearest">
										  <!--<input class="txt-box" type="time" id="appt" name="appt">-->
										  <input class="txt-box" type="text" id="time" placeholder="Time">
										  <i class="_self-ml10 far fa-clock" onClick="$('#time').focus();"></i>
										</div>
										<!-- /.input group -->
									  </div>
									  <!-- /.form group -->
									</div>





								</div>
								</div>

								<div class="mt20 _chd-ph10 center-xs">
									<button type="button" class="ui-btn-gray btn-lg" data-toggle="modal" data-target="#modal-close">Cancel</button>
									<button type="button" id="sendMSG" class="ui-btn-green btn-lg swalDefaultSuccess"><i class="hid fas fa-circle-notch fa-spin"></i> Send Message</button>
									<!-- <button type="submit" class="btn btn-danger btn-lg toastrDefaultError mr-2">Send Unsuccessful</button> -->
								</div>

								</div>
									
									</div>
								</div>
								<!-- /card -->
								
							</div>
								<!-- /card -->
								
								
								<div id="showPhone" class="hid status-freinds _self-mt30" >
													<div class="added col">
													
														 <div class="up-progress hid">
															<span class="progress-val">100%</span>
															<span class="up-progress-bar"><span class="progress-in"></span></span>
														  </div>
														
														<h3 class="bg-green pa10-xs rounded"><i class="fas fa-check-circle"></i> Message recipient list <small class="t-white">(<span class="count-num">30</span> Number)</small></h3>
														<ul>
															<li><i class="fas fa-user-check text-success"></i> 0812344545</li>
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															<li><i class="fas fa-user-check text-success"></i> 0879599955</li>
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															<li><i class="fas fa-user-check text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															<li><i class="fas fa-user-check text-success"></i> 0879599955</li>
															
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															<li><i class="fas fa-user-check text-success"></i> 0859599955</li>
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															
															<li><i class="fas fa-user-check text-success"></i> 0849599955</li>
															<li><i class="fas fa-user-check text-success"></i> 0812345678</li>
															<li><i class="fas fa-user-check text-success"></i> 0811414170</li>
															<li><i class="fas fa-user-check text-success"></i> 0899599955</li>
															<li><i class="fas fa-user-plus "></i> 0812345678</li>
															
															<li><i class="fas fa-user-plus "></i> 0811414170</li>
															<li><i class="fas fa-user-plus "></i> 0839599955</li>
															<li><i class="fas fa-user-plus "></i> 0812345678</li>
															<li><i class="fas fa-user-plus "></i> 0811414170</li>
															<li><i class="fas fa-user-plus "></i> 0879599955</li>
															
															<li><i class="fas fa-user-plus "></i> 0812345678</li>
															<li><i class="fas fa-user-plus "></i> 0811414170</li>
															<li><i class="fas fa-user-plus "></i> 0899599955</li>
															<li><i class="fas fa-user-plus "></i> 0812345678</li>
															<li><i class="fas fa-user-plus "></i> 0811414170</li>
															
																						</ul>

													</div>

												  </div>
								
							</div>
						
							<!--<div class="col-c hid _self-cl-sm-07-md-07-lg-08">
								

								
								
								
							</div>-->
						
						<div class="sidebar-right _self-cl-sm-12-md-03 d-flex center-xs">
							<!-- live Mobile -->
							<div id="main-log" class="bx-log active">
							<div id="live-screen">
								<ul class="list-sr-chat show">
									<li class="messageScreen1"><input type="hidden" class="messageScreen1" id="valScreen1" value="txt|ประกาศผลสอบ วิชาภาษาไทย 
									คุณ สมชาย มาติน 
									รหัสนักเรียน 11250 
									ได้คะแนน 62 คะแนน">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">ประกาศผลสอบ วิชาภาษาไทย<br class="br1">
											คุณ สมชาย มาติน<br class="br2">
											รหัสนักเรียน 11250<br class="br3">
											ได้คะแนน<br class="br4 hid"> <?php echo(rand(50,99)); ?><br class="br5 hid"> คะแนน<br class="br6 hid">
											</div>
										</div>
									</li>
								</ul>

							</div>
							</div>
							<!-- /live Mobile -->
						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->

<div class="popup thm-lite" id="modal-consent">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h2 class="t-black">Consent Form</h2>
					</div>
					<div class="modal-body">
					  
					  <div class="text-body txt-l">
					  	

							<div class="detail">
									<h3 class="text-md t-black">การขอความยินยอม (Consent Form)</h3>
									<div class="max">
									<p>วัตถุประสงค์</p>
									<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>3. เพื่อให้บุคลากรที่ได้รับ</p>
									<p>4. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
									วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
									<p>5. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
									ของตนได้</p>
									<p>6. เพื่อให้บุคลากรที่ได้รับ</p>

									</div>
								</div>

					  </div>
	
					</div>
					<div class="modal-footer p-0 center-xs">
					  <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-edit-name">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Edit Name</h4>
					  
					</div>
					<div class="modal-body txt-l">
					  <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Name</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Nadia">
                      </div>
					  
					   <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Surname</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Carmichael">
                      </div>
	
					</div>
					<div class="modal-footer justify-content-between mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-gray btn-sm" title="Cancel" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</a>
					   <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Save" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-edit-user">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Edit Phone Number / UID</h4>
					  
					</div>
					<div class="modal-body txt-l">
					  <div class="form-group bg-light rounded p-2">
                        <label class="d-block">Phone Number</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="0899599955">
                      </div>
					  
					   <div class="form-group bg-light rounded p-2">
                        <label class="d-block">User ID</label>
                        <input type="text" class="form-control form-control-lg text-lg text-center bg-white _self-pa30" value="Ub8c6ef4a7880161aa490bd7bd11d0133">
                      </div>
	
					</div>
					<div class="modal-footer justify-content-between mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-gray btn-sm" title="Cancel" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Cancel</a>
					   <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Save" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Save</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-history">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Call History</h4>
					  
					</div>
					<div class="modal-body">
					  <ul class="products-list product-list-in-card pl-2 pr-2">
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	20 เมษายน 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								12.00 น. | <span class="text-muted">เวลาทำรายการ 12.38 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	15 มีนาคม 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								09.53 น. | <span class="text-muted">เวลาทำรายการ 5.14 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						  <li class="item">
							
							<div class="product-info ml-0">
							  <a href="javascript:;" data-fancybox="historyChat" data-modal="modal" data-src="#modal-view-chat" title="edit" class="product-title text-dark" data-dismiss="modal">
							  	7 มกราคม 2563
								<span class="badge badge-info float-right">view chat</span></a>
							  <span class="product-description">
								16.05 น. | <span class="text-muted">เวลาทำรายการ 2.46 นาที</span>
							  </span>
							</div>
						  </li>
						  <!-- /.item -->
						</ul>
	
					</div>
					<div class="modal-footer center-xs mt10-xs">
					   <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					  <!--<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>-->
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-view-chat">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Chat at  20 เม.ย. 2563</h4>
					  
					</div>
					<div class="modal-body d-flex center-xs">
					  						
						<div id="main-log" class="bx-log active">
							<div id="live-screen" class="live-chat">
								<ul class="list-sr-chat show">

									<li class="messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาแชร์โลเคชั่นคุณลูกค้ามาให้ทางเราด้วยค่ะ</div>
										</div>
									</li>
									<li class="messageScreen1-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน แจ้งวัฒนะ ตำบลคลองเกลือ อำเภอปากเกร็ด นนทบุรี 11120 ประ
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:24 pm</span>
											</div>
										</div>
										
									</li>
									<li class="messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาส่ง Location ที่เกิดเหตุด้วยค่ะ</div>
										</div>
										<div class="button-quick-reply d-flex start-xs mt-3">
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กด แจ้ง Location</a>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน รังสิต-นครนายก คลอง6 ตำบลรังสิต อำเภอธัญบุรี ปทุมธานี 12110 ประเทศไทย
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:27 pm</span>
											</div>
										</div>
										
									</li>
									
									<li class="result-carousel messageScreen3">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button">
											<figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=Consent form"></a>
											</figure>
											<div class="detail">
												<h2>ทางบ.xxx ขออนุญาตเรียนแจ้งให้ท่านทราบว่า</h2>
												<p class="max">สำหรับการใช้บริการ....คุณลูกค้ากรุณาแสดงความยินยอม หรือ consent สำหรับการขอใช้บริการ ดูรายละเอียด consent form ได้ที่นี่ค่ะ [เงื่อนไขการยินยอม]
												</p>
											</div>
											<ul>
												<li class="p-1"><a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">เงื่อนไขและรายละเอียด</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action1">ยินยอม</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action2">ไม่ยินยอม</a>
												</li>
											</ul>
										</div>
									</li>
									<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text">					
											ข้าพเจ้ายินยอมเงื่อนไขการให้บริการ
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:37 pm</span>
											</div>
										</div>
									</li>
									<li class="result-imgmap messageScreen4">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button w-100">
	
											<div class="detail">
												<h2>การขอความยินยอม (Consent Form)</h2>
												<div class="max">

<p>วัตถุประสงค์</p>
<p>1. เพื่อให้ประชาชน ผู้รับบริการ ทุกพื้นที่สามารถเข้าถึงบริการด้านการให้ค าแนะน าด้านสุขภาพ
วินิจฉัยโรคเบื้องต้น ดูแลในกรณีฉุกเฉิน และดูแลในกรณีพิเศษ (ส่งต่อ)</p>
<p>2. เพื่อให้ประชาชนผู้รับบริการสามารถเข้าถึงและบริหารจัดการข้อมูลสุขภาพทางอิเล็กทรอนิกส์
ของตนได้</p>
<p>3. เพื่อให้บุคลากรที่ได้รับ</p>


												</div>
											</div>
											
										</div>
									</li>
									<li class="messageScreen5"><input type="hidden" class="done messageScreen5" id="valScreen5" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">ขอบคุณค่ะ</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
					<div class="modal-footer center-xs mt10-xs">
					  <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-demo-chat">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">Chat at  20 เม.ย. 2563</h4>
					  
					</div>
					<div class="modal-body d-flex center-xs">
					  						
						<div id="main-log" class="bx-log active">
							<div id="live-screen" class="live-chat">
								<ul class="list-sr-chat show">

									<li class="messageScreen1"><input type="hidden" class="done messageScreen1" id="valScreen1" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาแชร์โลเคชั่นคุณลูกค้ามาให้ทางเราด้วยค่ะ</div>
										</div>
									</li>
									<!--<li class="messageScreen1-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน แจ้งวัฒนะ ตำบลคลองเกลือ อำเภอปากเกร็ด นนทบุรี 11120 ประ
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:24 pm</span>
											</div>
										</div>
										
									</li>-->
									<li class="messageScreen2"><input type="hidden" class="done messageScreen2" id="valScreen2" value="txt|text">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
											<div class="direct-chat-text">กรุณาส่ง Location ที่เกิดเหตุด้วยค่ะ</div>
										</div>
										<div class="button-quick-reply d-flex start-xs mt-3">
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Address"><i class="fas fa-address-book"></i> กด พิมพ์ที่อยู่</a>
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> กด แจ้ง Location</a>
											<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Camera"><i class="fas fa-camera"></i> กด ส่งรูป</a>
										</div>
									</li>
									<!--<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text display-map">
											<figure class="map">
											<img src="http://ui.beurguide.net/LNS/dist/img/googlemap-demo.png">
											</figure>
											<div class="address">
											ถนน รังสิต-นครนายก คลอง6 ตำบลรังสิต อำเภอธัญบุรี ปทุมธานี 12110 ประเทศไทย
											</div>
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:27 pm</span>
											</div>
										</div>
										
									</li>-->
									
									<li class="result-carousel messageScreen3">
										<div class="direct-chat-msg">
											<div class="direct-chat-img"></div>
										</div>
										<div class="card-button">
											<figure class="image"><a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&amp;text=Consent form"></a>
											</figure>
											<div class="detail">
												<h2>ทางบ.xxx ขออนุญาตเรียนแจ้งให้ท่านทราบว่า</h2>
												<p class="max">สำหรับการใช้บริการ....คุณลูกค้ากรุณาแสดงความยินยอม หรือ consent สำหรับการขอใช้บริการ ดูรายละเอียด consent form ได้ที่นี่ค่ะ [เงื่อนไขการยินยอม]
												</p>
											</div>
											<ul>
												<li class="p-1"><a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">เงื่อนไขและรายละเอียด</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action1">ยินยอม</a>
												</li>
												<li class="p-1"><a class="btn btn-block bg-gray2 p-1" href="#Action2">ไม่ยินยอม</a>
												</li>
											</ul>
										</div>
									</li>
									<!--<li class="messageScreen2-reply">
										<div class="direct-chat-msg right">
											<img class="direct-chat-img" alt="message user image" src="http://ui.beurguide.net/LNS/dist/img/user7-128x128.jpg">
											<div class="direct-chat-text">					
											ข้าพเจ้ายินยอมเงื่อนไขการให้บริการ
											</div>
											<div class="direct-chat-infos clearfix">
											  <span class="direct-chat-timestamp float-right">23 May 5:37 pm</span>
											</div>
										</div>
									</li>-->
									
								</ul>
							</div>
						</div>
						
					</div>
					<div class="modal-footer center-xs mt10-xs">
					  <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->
			  
			  <div class="popup thm-lite" id="modal-map">
				<div class="box-middle">
				  <div class="modal-content">
					<div class="modal-header">
					  <h4 class="modal-title w-100 text-center">พิกัด 13.904478, 100.530079</h4>
					  
					</div>
					<div class="modal-body d-flex center-xs">
					 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1219.889347675032!2d100.53007896713898!3d13.904477974903562!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1febf57be0e63088!2z4Lit4Liy4LiE4Liy4Lij4LiL4Lit4Lif4LiV4LmM4LmB4Lin4Lij4LmM4Lib4Liy4Lij4LmM4LiE!5e0!3m2!1sth!2sth!4v1588137777781!5m2!1sth!2sth" width="600" height="450" frameborder="0" style="border:0; max-width: 100%" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
					<div class="modal-footer center-xs mt10-xs">
					  <a data-fancybox-close="" class="ui-btn-green btn-sm" title="Close" href="javascript:;" onclick="parent.jQuery.fancybox.close();">Close</a>
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>
			  <!-- /.modal -->


<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<!--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
  $( function() {
     $( ".todo-list" ).sortable({
      placeholder: "ui-state-highlight",
	  items: "li:not(.ui-state-disabled)",
	  //cancel: ".ui-state-disabled"
    });
    $( ".todo-list li" ).disableSelection();
  } );
</script>

<script>
/*upload*/
$( document ).ready( function () {
	var count_message = 0;
	
	$('.custom-select').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$(this).parents('.wrap-upload').find('.f-url').show();
			$(this).parents('.wrap-upload').find('.f-upload').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$(this).parents('.wrap-upload').find('.f-upload').show();
			$(this).parents('.wrap-upload').find('.f-url').hide();
		}

	});
	
	$('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	
	//upload progress
	function calluploading() {
		$('.progress-in').stop().animate({ width: $('.progress-val').text()},
			{ duration: 5000, step: function (now) {
				 $('.progress-val').text(Math.ceil(now) + '%');
			}
		}); 
		var count = 0;
		var cnumber = $('.count-num').text() - 1;
		  var innterval = setInterval(function() {
		  
			if (count == cnumber)
			  clearInterval(innterval);
			count++;
			$(".count-num").html( count + "%");
		  }, 150);
		/*$('.t-white').stop().animate({ width: $('.count-num').text()},
			{ duration: 5000, step: function (now) {
				 $('.count-num').text(Math.ceil(now));
			}
		}); */
	}
	//calluploading();
	$('#sendMSG').click(function() {
		$('#showPhone').removeClass('hid'); 
		$(this).children('i').removeClass('hid'); 
		$('.up-progress').removeClass('hid'); 
		calluploading()
		//$('.form-sending')[0].reset();
		
		
	});
	
	// jQuery UI sortable for the todo list
	  /*$('.todo-list').sortable({
		placeholder         : 'sort-highlight',
		handle              : '.handle',
		forcePlaceholderSize: true,
		zIndex              : 999999
	  })*/
	  
	  // Add Text
	$('.btn-add-msg').click(function() {
		var message_text = $("#message_text").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_text+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-text-height"></i> Text</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_text+'</div>'+
				'</div>'+
			'</li>');
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	//add Quick reply 
	$('#select-replay-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#replay-action1').show();
			$('#replay-action2').hide();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').hide();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').hide();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 3)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').hide();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 4)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').hide();
		} else if ($(this)[0].selectedIndex == 5)  {
			$('#replay-action1').show();
			$('#replay-action2').show();
			$('#replay-action3').show();
			$('#replay-action4').show();
			$('#replay-action5').show();
			$('#replay-action6').show();
		}
	});
	$('.btn-add-qreply').click(function() {
		var message_replay_text = $("#message_replay_text").val();
		var isAction = $('#select-replay-action')[0].selectedIndex;
		var myLabelAction1 = $("#labelQreply1").val();
		var myLabelAction2 = $("#labelQreply2").val();
		var myLabelAction3 = $("#labelQreply3").val();
		var myLabelAction4 = $("#labelQreply4").val();
		var myLabelAction5 = $("#labelQreply5").val();
		var myLabelAction6 = $("#labelQreply6").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && message_text != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+message_replay_text+'</span>'+
				'<small class="badge bg-purple"><i class="fas fa-reply"></i> Reply</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 3) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 4) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
				'</div>'+
			'</li>');
			} else if(isAction == 5) {
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+message_replay_text+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">'+message_replay_text+'</div>'+
				'</div>'+
				'<div class="button-quick-reply d-flex start-xs mt-3">'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction1+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction2+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction3+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction4+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction5+'</a>'+
					'<a class="btn bg-gray btn-sm text-white rounded-2" href="javascript:;" title="quickReply-Location"><i class="fas fa-map-marker-alt"></i> '+
						 ''+myLabelAction6+'</a>'+
				'</div>'+
			'</li>');
			}
			
			$("#message_text").val('');
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	

	//toggle action number
	
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
	//add appointment
	$('#select-apmNum-action').on('change', function() {

		if ($(this)[0].selectedIndex == 0) { 
			$('#group-a1').show();
			$('#group-a2').hide();
			$('#group-a3').hide();
			
		} else if ($(this)[0].selectedIndex == 1)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').hide();
		} else if ($(this)[0].selectedIndex == 2)  {
			$('#group-a1').show();
			$('#group-a2').show();
			$('#group-a3').show();
		}
	});

	
	$('.btn-add-appointment').click(function() {
		var isAction = $('#select-apmNum-action')[0].selectedIndex;
		var myApmTitle = $("#titleApm").val();
		var myApmText = $("#textApm").val();
		var myApmText2 = $("#textApm2").val();
		var myApmAction1 = $("#actionApm1").val();
		var myApmAction2 = $("#actionApm2").val();
		var myApmAction3 = $("#actionApm3").val();
		
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myApmTitle != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myApmTitle+'</span>'+
				'<small class="badge bg-primary"><i class="fas fa-user-clock"></i> Appointment</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			if(isAction == 0) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 1) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li class="p-1">'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			} else if(isAction == 2) {
			$('.list-sr-chat').append('<li class="result-carousel messageScreen'+count_message+'">'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
				 '</div>'+
				'<div class="card-button">'+
				  '<figure class="image">'+
					  '<a href="#listaction"><img src="https://dummyimage.com/500x500/fea3b8/ffffff.png&text=demo" /></a>'+
				  '</figure>'+
					'<div class="detail">'+
					 ' <h2>'+myApmTitle+'</h2>'+
					 ' <p>'+myApmText+'</p>'+
					 ' <p>'+myApmText2+'</p>'+
					'</div>'+
					'<ul>'+
					'<li>'+
					'<a href="https://ui.beurguide.net/LNS/microsite/consent1.html" target="_blank">'+myApmAction1+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action2">'+myApmAction2+'</a>'+
					'</li>'+
					'<li class="p-1">'+
					'<a class="btn btn-block bg-gray2 p-1" href="#Action3">'+myApmAction3+'</a>'+
					'</li>'+
				  '</ul>'+
				'</div>'+
			'</li>');
			}

			$("#titleApm").val('');
			$("#textApm").val('');
			
		} else {
			if ( count_items >= 5 ) {
				alert('Maximum 5 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	//add payload
	$('.btn-add-payload').click(function() {
		
		var myCustomText = $("#myCustomText").val();
		var count_items = 0;
		$("ul.list-sr-chat>li").each(function () {
			count_items++;
		});
		if ( count_items < 5 && myCustomText != '' ) {
			count_message++;
			if ( $('#newpush .todo-list').hasClass('show') ) {
				
			} else {
				$('.todo-list,.list-sr-chat,.save-template').addClass('show');
			}
			$('.todo-list').append('<li class="done messageList'+count_message+'">'+
				'<span class="handle ui-sortable-handle">'+
					' <i class="fas fa-ellipsis-v"></i>'+
					' <i class="fas fa-ellipsis-v"></i>'+
				'</span>'+
				'<div class="icheck-primary d-inline ml-2">'+
					' <input type="checkbox" id="chklist'+count_message+'" onclick="checkboxShowHide('+count_message+')" checked>'+
					' <label for="chklist'+count_message+'"></label>'+
				'</div>'+
				'<span class="text">'+myCustomText+'</span>'+
				'<small class="badge badge-info"><i class="fas fa-comment-dots"></i> Payload</small>'+
				'<div class="tools">'+
					'<i class="fas fa-edit text-gray" onclick="editMessage('+count_message+')"></i>'+
					'<i class="fas fa-trash" onclick="removeMessage('+count_message+')"></i>'+
				'</div>'+
			'</li>');
			$('.list-sr-chat').append('<li class="messageScreen'+count_message+'">'+
				'<input type="hidden" class="messageScreen'+count_message+'" id="valScreen'+count_message+'" value="txt|'+myCustomText+'"/>'+
				'<div class="direct-chat-msg">'+
					'<div class="direct-chat-img"></div>'+
					'<div class="direct-chat-text">Custom Message!</div>'+
				'</div>'+
			'</li>');
			$("#myCustomText").val('');
			
		} else {
			if ( count_items >= 3 ) {
				alert('Maximum 3 Message');
			} else {
				alert('Please input text');
			}
		}
	});
	
	  
	  
	$('#message_urlimg').change(function () {
		$('#displayimg').prop('disabled', false);
	});
	// Upload image
	$("#message_img").change(function () {
		var file = $('#message_img')[0].files[0];
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>'+file.name+'</p>');
	});
	$('#displayimg').click(function () {
		$('#labelimg').find('p').remove();
		$('#labelimg').append('<p>Choose file</p>');
	});
	$('#uploadimg').click(function () {
		var error_upload = [];
		error_upload[1] = 'Only file type JPG or PNG';
		error_upload[2] = 'File size Maximum 3 MB';
		var url = "https://api.innohub.me/pnp/upload";
		var chk_error = 0;
		var file = $('#message_img')[0].files[0];
		if (file.type != 'image/jpeg' && file.type != 'image/png') {
			chk_error = 1;
		} else if (file.size > 5*1024000) {
			chk_error = 2;
		}
		if (chk_error != 0) {
			alert(error_upload[chk_error]);
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#temp_image_src').attr('src', e.target.result);
		}
		reader.readAsDataURL(file);
		var formdata = new FormData();
		formdata.append("upload", file);
		$('#displayimg').append(' <i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			type: 'POST',
			url: url,
			data: formdata,
			success: function(data){
				if ( data.statusdesc == 'SUCCESS' ) {
					$('#displayimg').prop('disabled', false);
					$('#displayimg').find('i').remove();
					$('#temp_image_src').prop('src', data.url);
				}
			},
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json'
		});
	});
	
	
	
	//live 
	/*$('.call-log').click(function() {
		if ($('#dxy').hasClass( 'open' )) {
			$('#main-log').removeClass('active');
			$('#dxy').removeClass('open');
			$('#dxy').addClass('close');
			$('.call-log').removeClass('active');
		} else {
		   $('#main-log').addClass('active');
		   $('#dxy').toggleClass('open');
		   $('#dxy').removeClass('close');
		   $(this).toggleClass('active');
		}
    });*/
	

		/*toggle save template*/
		$('.save-as').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$(".set-name-tp").fadeIn(100);
				$("#tpl-name").prop("disabled",false); 
				$("#tpl-name").prop("required",true); 
			} else {
				$(".set-name-tp").fadeOut(100);
				$("#tpl-name").prop("disabled",true);
				$("#tpl-name").prop("required",false);
			}
		});
		/*toggle Receiver*/
		$('.rcv-addfriend').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#phone-chk").addClass('bg-light');
				$("#number-check").removeClass('hid');
			} else {
				$("#phone-chk").removeClass('bg-light');
				$("#number-check").addClass('hid');
			}
		});
		function removeBtnCheck() {
		  $('#number-check').find('i').removeClass('fa-spinner').removeClass('fa-spin');
		  $('#showPhone').fadeIn(100);
		}
		$('#number-check').click( function() {
			$(this).find('i').addClass('fa-spinner').addClass('fa-spin');
			setTimeout(function () {
				removeBtnCheck();
			}, 1800);
			
		});

	  
});

</script>


<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });
	  /*send check type*/
	  $('input[type=radio][name=chk-send]').change(function() {
		if (this.value == 'now') {
			$("#show-schedule").hide();
		}
		else if (this.value == 'set') {
			$("#show-schedule").show();
		}
	});
	//select2
	$(".select2").select2();
	$(".select-box").select2({dropdownAutoWidth : '70',minimumResultsForSearch: -1});
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	/*Groups all*/
	$("#checkAll").click(function(){
		if($("#checkAll").is(':checked') ){
			$("#e1 > option").prop("selected","selected");
			$("#e1").trigger("change");
		}else{
			$("#e1 > option").removeAttr("selected");
			$("#e1").trigger("change");
		 }
	});
	/*users all*/
	$('.master-chk').click(function() {
		if($(this).is(':checked')) {
			$("input[class*='chd-chk']").prop("checked", true);
			$(this).prop("checked", true);
		} else {
			$("input[class*='chd-chk']").prop("checked", false);
			$(this).prop("checked", false);
		}
	});
	$("input[class*='chd-chk']").change( function() {
		$('.master-chk').prop("checked", false);
	});
	/*count remain*/
	//var credit = 3;
	//var count = 1;
	var checkboxes = $('.bubble-chk');
	var remain = $('#countRmainUID').val();
	checkboxes.change(function(){
        var current = checkboxes.filter(':checked').length;
		//alert(current);
		$('#countRmainUID').val(remain - current);
		if(current >= remain) {
		   this.checked = false;
		   checkboxes.filter(':not(:checked)').prop("disabled", true);
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
        //checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
    });
	
	/*$('.bubble-chk').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= remain) {
		   this.checked = false;
		   alert("ขออภัยค่ะเครดิตคุณเหลือ 0 ");
	   }
	   count = $(this).siblings(':checked').length + 1;
	   alert(count);
	   $('#countRmainUID').val(remain - count);
	});*/

});
  </script>
  
  <script>
$( document ).ready( function () {
	

/*tab carousel*/
$(".cs-flex-tab").on("click", "a", function (e) {
        e.preventDefault();
        if (!$(this).hasClass('add-tab')) {
            $(this).tab('show');
        }
    })
    .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".cs-flex-tab li").children('a').first().click();
    });


$('.add-tab').click(function (e) {
    e.preventDefault();
    var id = $(".cs-flex-tab").children().length; //think about it ;)
    var tabId = 'card_' + id;
	/*var startId = 'start: ' + id;
	var settings = { startId , change:false }; 
	alert ( startId );
	alert ( settings );*/
    $(this).closest('li').before('<li class="nav-item"><a class="nav-link" href="#card_' + id + '">Card ' + id + ' </a> <span> x </span></li>');
    //$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">Contact Form: New Contact ' + id + '</div>');
	var clonecard= '<div id="main-card3" class="main-card wrap-upload">';
	clonecard+='<div class="form-group p-0">';
	clonecard+='<label>Select Type Image ' + id + '</label>';
	clonecard+='<select class="custom-select" id="select-card' + id + '-type">';
	clonecard+='<option value="0">URL</option>';
	clonecard+='<option value="1">Local file</option>';
	clonecard+='</select>';
	clonecard+='</div>';

	clonecard+='<div id="f-url_card' + id + '" class="f-url form-group">  ';
	clonecard+='<label for="card_urlimg' + id + '">From image URL</label>';
	clonecard+='<div class="input-group mb-3">';
	clonecard+='<div class="input-group-prepend">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='<input type="text" class="form-control" placeholder="URL Image" id="card_urlimg' + id + '">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="f-upload_card' + id + '" class="f-upload form-group" style="display: none">';
	clonecard+='<label for="card_img' + id + '-1">Upload image</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<div class="custom-file">';
	clonecard+='<input type="file" class="card-file-input' + id + '" id="card_img' + id + '" accept=".jpg,.jpeg,.png">';
	clonecard+='<img id="temp_card' + id + '_src" hidden/>';
	clonecard+='<label class="custom-file-label" id="card_labelimg' + id + '" for="card_img' + id + '"><p>Choose file</p></label>';
	clonecard+='</div>';
	clonecard+='<div class="input-group-append">';
	clonecard+='<button class="input-group-text" id="card_uploadimg' + id + '">Upload</button>';
	clonecard+='</div>';

	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='<div class="form-group pa10-xs bg-light rounded">';
	clonecard+='<label>Link URL</label>';
	clonecard+='<div class="input-group">';
	clonecard+='<input type="text" class="form-control" placeholder="Action URL">';
	clonecard+='<div class="input-group-append">';
	clonecard+='<span class="input-group-text"><i class="fas fa-link"></i></span>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	

	clonecard+='<div class="form-group"><label>Title Card</label>';
	clonecard+='<input type="text" class="form-control" id="titleCard' + id + '" placeholder="Title">';
	clonecard+='</div>';

	clonecard+='<div class="form-group">';
	clonecard+='<textarea id="textCard' + id + 'D1" class="form-control mb10-xs" rows="2" placeholder="Description"></textarea>';
	clonecard+='<textarea id="textCard' + id + 'D2" class="form-control" rows="2" placeholder="Description 2"></textarea>';
	clonecard+='</div>';

	clonecard+='<div class="form-group"><label>Number of Actions</label>';
	clonecard+='<select class="form-control select-action-chd" id="select-card' + id + 'Num-action"><option value="0">1</option> <option value="1">2</option><option value="2">3</option></select>';
	clonecard+='</div>';

	clonecard+='<div class="main-apm-action bg-light pa10-xs">';
	clonecard+='<div id="card' + id + '-a1" class="g-action1"><div class="form-group"><label>Action1</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num-action1"><option value="0">Message Action</option><option value="1">URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Text</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '" placeholder="Action 1">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a2" class="g-action2"><div class="form-group"><label>Action2</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num2-action2"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-2" placeholder="Action 2">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-2" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div id="card' + id + '-a3" class="g-action3"><div class="form-group"><label>Action3</label><div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Type</span><div class="col">';
	clonecard+='<select class="form-control" id="select-card' + id + 'Num3-action3"><option value="0">Message Action</option><option value="1" selected>URI Action</option><option value="2">Postback Action</option></select>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">Label</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionCard' + id + '-3" placeholder="Action 3">';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='<div class="row middle-xs mb10-xs"><span class="col-sm-4 col-md-3 col-lg-2 text-right text-muted mb-0">URI</span><div class="col">';
	clonecard+='<input type="text" class="form-control" id="actionLinkCard' + id + '-3" placeholder="URI">';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';
	clonecard+='</div>';

	clonecard+='</div>';

	//write
	//$('.tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
    //$('.cs-flex-tab li:nth-child(' + id + ') a').click();
	//write
	$('#vert-tabs-flex .tab-content').append('<div class="tab-pane" id="' + tabId + '">' + clonecard + '</div>');
	$('#custom-carousel-tab').idTabs(); 
    $('#custom-carousel-tab li:nth-child(' + id + ') a').click();
	function callToggleNum() {
		$('.select-action-chd').on('change', function() {

			if ($(this)[0].selectedIndex == 0) { 
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').hide();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();

			} else if ($(this)[0].selectedIndex == 1)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').hide();
			} else if ($(this)[0].selectedIndex == 2)  {
				$(this).parent().next('.main-apm-action').children('.g-action1').show();
				$(this).parent().next('.main-apm-action').children('.g-action2').show();
				$(this).parent().next('.main-apm-action').children('.g-action3').show();
			}
		});
	}
	callToggleNum();
	
});

});
</script>
  

<script>
$( function() {
	
    //$( "#calendarshow" ).datepicker();

		$( "#datepicker" ).datepicker({
		  showOtherMonths: true,
		  selectOtherMonths: true
		});
	//set time	
	var timepicker = new TimePicker('time', {
  lang: 'en',
  theme: 'dark'
});
timepicker.on('change', function(evt) {
  
  var value = (evt.hour || '00') + ':' + (evt.minute || '00');
  evt.element.value = value;

});



  } );
</script>



<!--Plugin CSS file with desired skin-->
<link rel="stylesheet" href="js/tagator/fm.tagator.jquery.css">
<!--Plugin JavaScript file-->
<script src="js/tagator/fm.tagator.jquery.js"></script>
<!-- /js -->

</body>
</html>
