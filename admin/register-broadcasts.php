<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<!-- /Headbar -->
<div class="page-register">

    
    <div id="toc">
		<section class="z-broadcast mb0">
			<!--<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="broadcasts.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="broadcasts-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="broadcasts-acc.php" title="User management" class="selected"><i class="fas fa-users-cog"></i> <span>User Management</span></a></li>
						  <li><a href="broadcasts-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="broadcasts-report.php" title="Report"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
						  <li><a href="broadcasts-survey.php" title="Survey"><i class="fas fa-tasks"></i> <span>Survey</span></a></li>
				  </ul>
			</div>-->
					

			
			<div class="contentTabs main-sv">
				<div id="tbc-1" class="msg">

					<div class="head-title txt-c m-0">
						<h2>Register</h2>	
						<p>ลงทะเบียนเข้าใช้งาน</p>
					</div>
					
					<div class="container pt30-sm pt10-xs">
					 <div class="type-register">
					 	 <h3 class="txt-c t-gray mb15-xs text-sm">กรุณาเลือกประเภทการลงทะเบียน</h3>
						 <ul class="d-flex _chd-ph20 center-xs middle-xs inputGroup">
							<li><input type="radio" id="type-r1" name="type-user" value="0"> <label for="type-r1"><i class="fas fa-id-card-alt fa-2x"></i> <span> พนักงาน <small>Employee</small></span></label> </li>
							<li><input type="radio" id="type-r2" name="type-user" value="1"> <label for="type-r2"><i class="fas fa-user-graduate fa-2x"></i> <span>นักเรียน/นักศึกษา <small>Student</small></span></label> </li>
							<li><input type="radio" id="type-r3" name="type-user" value="2"> <label for="type-r3"><i class="fas fa-address-card fa-2x"></i> <span>บุคคลอื่นๆ <small>User</small></span></label> </li>
						 </ul>
					 </div>
					
					  <form id="save_guest" class="form-checkout pt25-sm" method="POST" action="javascript:;" enctype="multipart/form-data">
						<fieldset class="fix-label">
						<ul class="form-list">   

							<li class="group">
								<div class="d-flex _chd-ml15 middle-xs">
									<label>เพศ : </label>
									<div class="icheck-primary"><input type="radio" name="gender" value="0" id="male"><label for="male">ชาย</label></div>
									<div class="icheck-primary"><input type="radio" name="gender" value="1" id="female"><label for="female">หญิง</label></div>
									<div class="icheck-primary"><input type="radio" name="gender" value="2" id="LGBT"><label for="LGBT">อื่นๆ</label></div>
								</div>

								<div class="row">
									<div class="left col-xs-12 col-sm-6">
										<div class="wr">
											<input type="text" class="txt-box" id="first_name" name="first_name" required>
											<label for="first_name">ชื่อจริง</label>
											<span class="line"></span>
										</div>
									</div>
									<div class="right col-xs-12 col-sm-6">
										<div class="wr">
											<input type="text" class="txt-box" id="last_name" name="last_name" required>
											<label for="last_name">นามสกุล</label>
											<span class="line"></span>
										</div>
									</div>
								</div>
								<div class="row">
									 <div class="left col-xs-12 col-sm-6">
											<div class="wr">
												<input type="date" class="txt-box" id="birthdate" name="birthdate" required maxlength="10">
												<label for="birthdate">วันเดือนปีเกิด</label>
												<span class="line"></span>
											</div>
										</div>
										<div class="right col-xs-12 col-sm-6">
											<div id="employee" class="wr" style="display: none">
												<input type="text" class="txt-box" id="id_employee" name="id_student" autocomplete="off" required>
												<label for="id_student">รหัสพนักงาน</label>
												<span class="line"></span>
											</div>
											<div id="student" class="wr" style="display: none">
												<input type="text" class="txt-box" id="id_student" name="id_student" autocomplete="off" required>
												<label for="id_student">รหัสนักเรียน/นักศึกษา</label>
												<span class="line"></span>
											</div>
											<div id="user" class="wr" style="display: none">
												<input type="text" class="txt-box" id="id_card" name="id_student" autocomplete="off" required>
												<label for="id_student">เลขบัตรประชาชน</label>
												<span class="line"></span>
											</div>
										</div>
								</div>
								<div class="row">
									 <div class="left col-xs-12 col-sm-6">
											<div class="wr">
												<input type="tel" class="txt-box" id="staff_mobile" name="staff_mobile" required maxlength="10">
												<label for="staff_mobile">เบอร์โทรศัพท์มือถือ</label>
												<span class="line"></span>
											</div>
										</div>
										<div class="right col-xs-12 col-sm-6">
											<div class="wr">
												<input type="email" class="txt-box" id="email" name="email" autocomplete="off" required>
												<label for="email">e-mail</label>
												<span class="line"></span>
											</div>
										</div>
								</div>
								
								<?php /*?><div class="row">
									<div class="col-xs-12 col-sm-12">
										<div class="wr js-select">
											<!--<input type="text" class="txt-box" id="division" name="division" autocomplete="off" required>-->
											<select class="select-box" id="select-group" name="select-group" data-placeholder="Select Group">
												<option></option>
												<option value="1">พนักงานบริษัทเอกชน</option>
												<option value="2">ภาครัฐ / รัฐวิสาหกิจ</option>
												<option value="3">เจ้าของธุรกิจ SME</option>
												<option value="4">เจ้าของธุรกิจขนาดใหญ่</option>
												<option value="4">อาชีพอิสระ/Freelance</option>
											</select>
											<label for="division">อาชีพ</label>
											<span class="line"></span>
										</div>
									</div>   
								</div>                    

								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<div class="wr">
											<input type="email" class="txt-box" id="email" name="email" autocomplete="off" required>
											<label for="email">e-mail</label>
											<span class="line"></span>
										</div>
									</div>
								</div><?php */?>     


							</li>
							
							<li class="mt20-xs">
								<div id="popup-terms" class="thm-popup frame-terms">
									<div class="inner"><div class="container"><section id="terms" data-wow-delay="0.25s" class="main-terms wow fadeIn" style="visibility: visible; animation-delay: 0.25s; animation-name: fadeIn;"><div class="_self-pa20"><h1 class="h-text txt-c">ข้อกำหนดและเงื่อนไขในการใช้งาน broadcasts</h1><br><p>ข้อกำหนดและเงื่อนไขในการใช้งานที่แสดงไว้ ณ ที่นี้ ระบุถึงข้อกำหนดระหว่าง ไลน์ คอร์ปอเรชั่น (ต่อไปนี้เรียกว่า "บริษัทฯ") และผู้ใช้ (ต่อไปนี้เรียกว่า "ผู้ใช้") เกี่ยวกับบริการหรือคุณลักษณะใดๆ ของ broadcasts (ต่อไปนี้เรียกว่า "บริการฯ") ซึ่งบริษัทฯ เป็นผู้ให้บริการ</p><br><p></p><br><h2 class="h3">1. การตกลงยอมรับข้อกำหนดและเงื่อนไขฯ</h2><p>1.1 ผู้ใช้ทุกรายจะต้องใช้บริการฯ ตามข้อกำหนดที่ระบุไว้ในข้อกำหนดและเงื่อนไขฯ ผู้ใช้จะไม่สามารถใช้บริการฯ ได้เว้นเสียแต่ว่า ผู้ใช้ได้ตกลงยอมรับข้อกำหนดและเงื่อนไขฯ การตกลงยอมรับดังกล่าวมีผลสมบูรณ์และเพิกถอนมิได้</p><p>1.2 การที่ผู้ใช้ใช้บริการฯ ถือว่า ผู้ใช้ได้ตกลงยอมรับข้อกำหนดและเงื่อนไขฯ การตกลงยอมรับดังกล่าวมีผลสมบูรณ์และเพิกถอนมิได้</p><p>1.3 หากมีข้อกำหนดและเงื่อนไขฯ เพิ่มเติมสำหรับบริการฯ ผู้ใช้จะต้องปฏิบัติตามข้อกำหนดและเงื่อนไขฯ เพิ่มเติมนั้นด้วยเช่นเดียวกับข้อกำหนดและเงื่อนไขฯ นี้</p><p></p><p></p><br><h2 class="h3">2. การแก้ไขข้อกำหนดและเงื่อนไขฯ</h2><p>บริษัทฯ อาจแก้ไขข้อกำหนดและเงื่อนไขฯ หรือ ข้อกำหนดและเงื่อนไขฯ เพิ่มเติม เมื่อบริษัทฯ เห็นว่ามีความจำเป็น โดยไม่ต้องให้คำบอกกล่าวล่วงหน้าแก่ผู้ใช้ การแก้ไขจะมีผลบังคับเมื่อมีการประกาศข้อกำหนดและเงื่อนไขฯ ที่แก้ไข หรือข้อกำหนดและเงื่อนไขฯ เพิ่มเติมที่แก้ไขไว้ในสถานที่ที่เหมาะสมภายในเว็บไซต์ที่บริษัทฯ เป็นผู้ดำเนินงาน การที่ผู้ใช้ใช้บริการฯ ต่อไป ถือว่าผู้ใช้ได้ให้การยอมรับที่มีผลสมบูรณ์และเพิกถอนมิได้ต่อข้อกำหนดและเงื่อนไขฯ ที่แก้ไขแล้ว หรือข้อกำหนดและเงื่อนไขฯ เพิ่มเติมที่แก้ไขแล้ว ผู้ใช้ควรตรวจสอบข้อกำหนดและเงื่อนไขฯ เป็นประจำระหว่างการใช้บริการฯ เนื่องจากอาจไม่มีการแจ้งเตือนแยกต่างหากเกี่ยวกับการแก้ไขข้อกำหนดและเงื่อนไขฯ ดังกล่าว</p><p></p><p></p><br><h2 class="h3">3. บัญชี</h2><p>3.1 บัญชี LINE ผู้ใช้จะต้องใช้บัญชี LINE เพื่อเข้าใช้งาน broadcasts โดยที่ทางบริษัทฯ ไม่ได้มีการบันทึกรหัสผ่านของคุณหรือทำสำรองเก็บไว้ และถ้าผู้ใช้ต้องการดูรายการกิจกรรมที่ใช้บริการ สามารถติดต่อทางบริษัทฯ ได้โดยตรง</p><p>3.2 บัญชี Google บริษัทฯจะทำการขอสิทธิ์การเข้าถึงบัญชี Google ของผู้ใช้ เพื่อเก็บและค้นหาไฟล์ใน Google Drive เพื่อบันทึกการนัดหมายบน Google Calendar และเพื่อค้นหารายชื่อจาก Google Contacts โดยที่ทางบริษัทฯ ไม่ได้มีการบันทึกรหัสผ่านของคุณหรือทำสำรองเก็บไว้</p><p>3.3 ผู้ใช้งานที่จดทะเบียนใช้บริการฯ สามารถลบบัญชีของตนและเลิกใช้บริการฯ ได้ ไม่ว่าในเวลาใดๆ ก็ตาม</p><p>3.4 สิทธิ์ที่จะใช้บริการของผู้ใช้จะสิ้นสุดลงเมื่อมีการลบบัญชีของผู้ใช้ไม่ว่าด้วยเหตุผลใดๆ ก็ตาม บัญชีจะไม่สามารถกู้คืนมาได้แม้ว่าผู้ใช้จะลบบัญชีของตนโดยไม่ได้ตั้งใจก็ตาม และบริษัทฯ ขอให้ผู้ใช้ตระหนักถึงเรื่องนี้ด้วย</p><p></p><br><h2 class="h3">4. การคุ้มครองข้อมูลส่วนบุคคล</h2><p>4.1 บริษัทฯ ให้ความสำคัญแก่ความเป็นส่วนตัวของผู้ใช้ของบริษัทฯ เป็นลำดับแรก</p><p>4.2 บริษัทฯ สัญญาว่าจะคุ้มครองความเป็นส่วนตัวและข้อมูลส่วนบุคคลของผู้ใช้ของบริษัทฯ ตาม "นโยบายของ broadcasts ว่าด้วยการคุ้มครองข้อมูลส่วนบุคคล"</p><p>4.3 บริษัทฯ สัญญาที่จะใช้ความระมัดระวังและความใส่ใจอย่างสูงสุดเกี่ยวกับมาตรการรักษาความปลอดภัยของบริษัทฯ เพื่อให้มีการรักษาความปลอดภัยของข้อมูลใดๆ ของผู้ใช้ ทั้งหมด</p><br><h2 class="h3">5. การให้บริการ</h2><p>5.1 ผู้ใช้จะต้องเป็นผู้รับผิดชอบจัดหาเครื่องคอมพิวเตอร์ อุปกรณ์โทรศัพท์เคลื่อนที่ อุปกรณ์สื่อสาร ระบบปฏิบัติการและการเชื่อมต่อข้อมูลที่จำเป็นสำหรับการใช้บริการฯ โดยผู้ใช้เป็นผู้ออกค่าใช้จ่ายเอง</p><p>5.2 ผู้ใช้จำเป็นต้องกรอกข้อมูลการชำระเงินก่อนที่จะรับบริการทดลองใช้งานฟรี 30 วัน โดยบริษัทฯ จะอนุญาตให้ผู้ใช้รับบริการทดลองใช้งานฟรีนี้ในรูปแบบแพ็กเกจ Lite ที่สามารถนำ broadcasts ไปใช้ในบริการกลุ่มสนทนา LINE ได้เพียง 1 กลุ่มเท่านั้น</p><p>5.3 ในการใช้บริการ ผู้ใช้จะต้องเชิญ broadcasts เข้ากลุ่มสนทนาและเปิดใช้บริการก่อน เมื่อ broadcasts เข้ากลุ่มสนทนาแล้ว จะมีการขอยืนยันสิทธิ์การเก็บไฟล์จากสมาชิกในกลุ่มสนทนาตามนโยบายคุ้มครองข้อมูลส่วนบุคคล ถ้าสมาชิกคนใดไม่ยอมรับเงื่อนไขนี้ broadcasts จะไม่สามารถเก็บไฟล์ของสมาชิกคนนั้นได้</p><p>5.4 บริษัทฯ สงวนสิทธิ์ที่จะเปลี่ยนแปลงหรือยุติบริการฯ ทั้งหมดหรือเพียงบางส่วนตามดุลยพินิจของบริษัทฯ ไม่ว่าในเวลาใดๆ ก็ตาม โดยไม่ต้องให้คำบอกกล่าวล่วงหน้าใดๆ แก่ผู้ใช้</p><p>5.5 บริษัทฯ มีหน้าที่เป็นผู้ดูแลการส่งต่อข้อมูลจากการทำงานของผู้ใช้ทางไลน์ที่มี broadcasts บริการอยู่ไปยังบัญชี Google ของผู้ใช้ที่ได้รับอนุญาตจากผู้ใช้เรียบร้อยแล้ว โดยการบริการทั้งหมดนี้ทางบริษัทฯ ไม่ได้มีการทำสำรองข้อมูลหรือทำการกระทำใดๆ นอกเหนือจากข้อตกลงการใช้บริการที่ให้ไว้</p><p>5.6 บริษัทฯ จะไม่รับผิดชอบต่อค่าเสียหายใดๆ ที่เกิดขึ้นกับผู้ใช้อันเกี่ยวกับการใช้บริการฯ โดยที่ผู้ใช้นั้นนำข้อมูลไปผยแพร่ส่วนตัว เชิญบุคคลเข้ากลุ่มสนทนาไลน์ หรืออนุญาตให้บุคคลเข้า Google account ของผู้ใช้เอง แต่อย่างไรก็ตามหากผู้ใช้สงสัยว่าจะมีการใช้บัญชีของท่านโดยไม่ได้รับอนุญาตหรือมีการละเมิดการรักษาความปลอดภัย ผู้ใช้สามารถแจ้งทางบริษัทฯ ผ่านช่องทางการติดต่อของเรา</p><br><h2 class="h3">6.การชำระเงิน</h2><p>6.1 การซื้อสินค้าและบริการ ผู้ใช้สามารถซื้อการบริการจากหน้าเว็บไซต์หรือทางไลน์จากหัวข้อแพ็กเกจและราคา บริษัทฯ ใช้บริการบริษัท ทูซีทูพี (ประเทศไทย) จำกัด ที่ให้ผู้ใช้บริการสามารถใช้บริการชำระเงินได้ผ่านทางอินเตอร์เน็ตและบริการออกใบเสร็จให้แก่ผู้ใช้ทันทีที่มีการซื้อบริการ broadcasts โดยจะทำการหักผ่านบัตรเครดิตของผู้ใช้บริการทุกเดือนจนกว่าผู้ใช้จะทำการยกเลิกการชำระเงินนี้ ดังนั้นผู้ใช้จำเป็นต้องทำความเข้าใจและยอมรับเงือนไขการบริการของ 2C2P อย่างละเอียดก่อน</p><p>6.2 การยกเลิกบริการ ผู้ใช้สามารถยกเลิกบริการได้ทุกเมื่อ และจะมีผลในรอบบิลนั้นๆ ทันที นอกจากนั้นบริษัทฯ ไม่มีนโยบายคืนเงินให้กับผู้ใช้ในทุกกรณี</p><p>6.3 การลดแพ็กเกจ หากผู้ใช้มีความต้องการลดจำนวนกลุ่มสนทนาที่มี broadcasts ให้บริการอยู่ ผู้ใช้ควรตรวจสอบข้อจำกัดของแต่ละแพ็กเกจและจัดการเงื่อนไขตามที่บริษัทฯ กำหนดก่อนทำการเปลี่ยนแปลงแพ็กเกจ โดยการเปลี่ยนแปลงนี้จะมีผลทันทีหลังจากมีการชำระเงิน และทางบริษัทฯ ไม่มีนโยบายคืนเงินให้กับผู้ใช้ในทุกกรณี</p><p>6.4 การเพิ่มแพ็กเกจ ผู้ใช้สามารถเพิ่มการใช้งานของ broadcasts ให้อยู่ในกลุ่มสนทนาได้หลายกลุ่มมากขึ้น ค่าใช้จ่ายของการเปลี่ยนแปลงนี้จะคำนวนจากส่วนต่างระหว่างแพ็กเกจใหม่กับแพ็กเกจปัจจุบันที่ผู้ใช้ใช้งานอยู่ รวมถึงหักจากจำนวนวันที่มีการใช้งาน โดยการเปลี่ยนแปลงนี้จะมีผลทันทีหลังจากมีการชำระเงิน และทางบริษัทฯ ไม่มีนโยบายคืนเงินให้กับผู้ใช้ในทุกกรณี</p><p>6.5 การเปลี่ยนแปลงราคาและแพ็กเกจ บริษัทฯ อาจมีการเปลี่ยนแปลงราคาและแพ็กเกจ โดยจะมีการแจ้งผู้ใช้ก่อนผ่านทางอีเมล หรือข้อความทางไลน์ การเปลี่ยนแปลงนี้จะนำไปใช้ในรอบบิลถัดไปหลังจากแจ้งผู้ใช้หรือแพ็กเกจที่ผู้ใช้ใช้อยู่หมดสัญญา</p><p></p><p></p><br><h2 class="h3">7. ความรับผิดชอบของผู้ใช้</h2><p>7.1 ผู้ใช้จะต้องใช้บริการฯ นี้โดยเป็นความเสี่ยงของผู้ใช้เอง และจะต้องรับผิดชอบแต่เพียงผู้เดียวสำหรับการกระทำที่กระทำไปและผลของการกระทำที่มีต่อบริการนี้</p><p>7.2 บริษัทฯ อาจใช้มาตรการที่บริษัทฯ เห็นว่าจำเป็นและเหมาะสมได้ หากบริษัทฯ รับทราบว่าผู้ใช้รายหนึ่งรายใดกำลังใช้บริการฯ ในทางที่ฝ่าฝืนข้อกำหนดและเงื่อนไข อย่างไรก็ตาม บริษัทฯ ไม่ต้องรับผิดชอบในการแก้ไขหรือป้องกันการฝ่าฝืนดังกล่าวต่อผู้ใช้หรือบุคคลอื่นๆ</p><p>7.3 ในกรณีที่เกิดความสูญเสีย/ความเสียหายแก่บริษัทฯ หรือบริษัทฯ ถูกเรียกเก็บค่าใช้จ่ายใดๆ (ซึ่งรวมถึงโดยไม่จำกัดเพียงค่าทนายความ) ไม่ว่าโดยตรงหรือโดยอ้อม (ซึ่งรวมถึงโดยไม่จำกัดเพียงกรณีที่มีบุคคลภายนอกฟ้องร้องเรียกค่าเสียหายจากบริษัทฯ) อันเนื่องมาจากการที่ผู้ใช้ฝ่าฝืนกฎหมายที่เกี่ยวข้องหรือข้อกำหนดและเงื่อนไขฯ ในขณะที่ใช้บริการฯ ผู้ใช้จะต้องชดใช้ค่าเสียหายให้แก่บริษัทฯ ทันทีที่บริษัทฯ ร้องขอ</p><p></p><p></p><p></p><br><h2 class="h3">นโยบายอื่นๆ</h2><p>นโยบายคุ้มครองข้อมูลส่วนบุคคลของ broadcasts</p><p></p><p></p><br><h2 class="h3"></h2><p></p><p></p><p></p><br><h2 class="h3"></h2><p></p><br></div></section></div>
									</div>
								</div>
								<div class="agree">
									<div id="check-accept" class="col-xs-12 mz-chk"><input type="checkbox" id="iam-gree" name="iam-gree" required="required"><label for="iam-gree"> I agree to the broadcasts 
									<a class="t-blue" href="javascript:;" data-fancybox="login2" data-src="#popup-terms" style="color: blue;"> Terms and Conditions.</a></label></div>
									
								</div>
							</li>


							<li class="ctrl-btn txt-c">
								<button id="btnSignUp" class="ui-btn-green btn-lg" type="submit" value="submit" data-fancybox="success" data-src="#popupCredit" title="ส่งข้อมูล">ยืนยัน</button>
							</li>
						</ul>


						</fieldset>
					</form>
					
					</div>
					

				</div>
			</div>
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	


	//select2
	$(".select-box").select2({minimumResultsForSearch: -1});
	//check type
	$("[name=type-user]").on("click", function() {
		var isCase = this.value;
		if(isCase == 0) {
			//alert("Employee");
			$('#employee').show();
			$('#student').hide();
			$('#user').hide();
		} else if(isCase == 1) {
			//alert("Student");
			$('#employee').hide();
			$('#student').show();
			$('#user').hide();
		} else if(isCase == 2) {
			//alert("User");
			$('#employee').hide();
			$('#student').hide();
			$('#user').show();
		}

	})


});
  </script>
  


<!-- /js -->

</body>
</html>
