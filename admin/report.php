<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(3)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			<div class="bx-stepbar _self-pv20 cb-af container">
				<ul class="tabsbar">
						  <li><a href="lns.php" title="Send Message"><i class="fas fa-bullhorn"></i> <span>Send Message</span></a></li>
						  <li><a href="lns-create.php" title="Create Message"><i class="fas fa-layer-group"></i> <span>Create Template</span></a></li>
						  <li><a href="lns-setting.php" title="Message Setting"><i class="fas fa-sliders-h"></i> <span>Message Setting</span></a></li>
						  <li><a href="lns-report.php" title="Report" class="selected"><i class="fas fa-file-medical-alt"></i> <span>Report</span></a></li>
				  </ul>
			</div>
					

			
			<div class="bg-gray2 contentTabs">
				<div id="tbc-1" class="msg">
					<form method="post" class="form-checkout form-sending">
					
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main row center-xs">
							<div class="container">
								<div class="head-title m-0">
									<h2>Report</h2>	
									<p>ระบบส่งข้อความผ่านทาง line</p>
								</div>
								<!-- card -->
								<div class="card bg-white">
									<div class="card-header">
										<h3 class="card-title center-xs"><b>Monthly transection report </b></h3>
									</div>
									<div class="card-body _self-pa30 middle-xs">
										<div class="table-resp">
											<table class="table table-bordered">
											  <thead>
											  <tr class="bg-gray2">
												<th class="text-center" align="center">no</th>
												<th class="text-center" align="center">Month</th>
												<th class="text-center" align="center">Message Type</th>
												<th class="text-center" align="center">Message Delivered</th>
												<!--<th class="text-center" align="center">Remain <br>messages</th>
												<th class="text-center" align="center">Quota</th>
												<th class="text-center text-danger" align="center">Over Quota<br> Exceeded</th> -->

											  </tr>
											  </thead>
											  <tbody>
											  <tr>
												<td align="center">4</td>
												<td align="center">June 2020</td>
							
											  	<td align="center">UID</td>
												<td align="center"><a href="broadcasts-report-transections.php" class="t-blue">100,250</a></td>

												<?php /*?><td align="center">-</td>
																	<td align="center">
																	  35,000

																	</td>
												<td align="center"><span class="t-red">65,250</span></td><?php */?>
											  </tr>
											  
											  <tr>
							<td align="center">3</td>
							<td align="center">May 2020</td>
							<td align="center">UID</td>
												<td align="center"><a href="broadcasts-report-transections.php" class="t-blue">32,960</a></td>

												<?php /*?><td align="center"><small>Unlimited</small></td>
																	<td align="center">
																	  <small class="t-red">*Unlimited</small>

																	</td>
												<td align="center">-</td><?php */?>
											  </tr>
											  
											  <tr>
							<td align="center">2</td>
							<td align="center">April 2020</td>
							<td align="center">UID</td>
												<td align="center"><a href="broadcasts-report-transections.php" class="t-blue">7,250</a></td>

												<?php /*?><td align="center">27,750</td>
																	<td align="center">
																	  35,000

																	</td>
												<td align="center">-</td><?php */?>
											  </tr>
											  
											  <tr>
							<td align="center">1</td>
							<td align="center">March 2020</td>
							<td align="center">UID</td>
												<td align="center"><a href="broadcasts-report-transections.php" class="t-blue">35,000</a></td>

												<?php /*?><td align="center">0</td>
																	<td align="center">
																	  35,000

																	</td>
												<td align="center">-</td><?php */?>
											  </tr>

											  </tbody>
											</table>
										  </div>
									</div>
									
									<div class="sticky-bottom card-footer">
									<div class="__chd-ph10 center-xs">
											<button type="button" class="ui-btn-green btn-md" onclick="$('.form-sending')[0].reset();"><i class="fas fa-file-excel"></i> Export Excel</button>
									</div>
								  </div>
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</form>

			</div>
			
			
			
			
		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.keep-select-group').select2({
    	placeholder: "Please select",
    	//allowClear: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	
	$('#uid-unlimit').change( function() {
			var isChecked = this.checked;
			if(isChecked) {
				$("#uid-limit").removeClass("bg-white");
				$("#uid-limit").prop("disabled",true); 
				$("#uid-limit").prop("required",false); 
			} else {
				$("#uid-limit").addClass("bg-white");
				$("#uid-limit").prop("disabled",false);
				$("#uid-limit").prop("required",true);
			}
		});
	

});
  </script>
  


<!-- /js -->

</body>
</html>
