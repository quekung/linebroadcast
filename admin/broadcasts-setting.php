<!DOCTYPE HTML>
<html dir="ltr" lang="th">
<!-- Top Head -->
<?php include("incs/head-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-hidden">
<!-- Headbar -->
<?php include("incs/header.html") ?>
<script>
$(".main-menu .list-unstyled>li.active").removeClass('active');
$(".main-menu .list-unstyled>li:nth-child(1)").addClass('active');
</script>
<!-- /Headbar -->
<div class="page-checkout">

    
    <div id="toc">
		<section class="z-broadcast _self-pt0 mb0">
			
					

			
			<div class="bg-white contentTabs">
				<div id="tbc-1" class="msg">
					<div class="form-checkout form-sending">
					<div class="head-title m-0 txt-l">
						<h2>User Detail</h2>	
						<p>ระบบส่งข้อความผ่านทาง line</p>
					</div>
					<div class="wrap-full _chd-cl-xs-12 _chd-cl-sm">
						<div class="main">
							<div class="container">
								
								<!-- card -->
								<div class="card">
									<div class="card-header _flex center-xs between-xsh">
										<h2 class="text-md">ประเภทองค์กรของคุณ</h2>
									</div>
									
									
												
												
									<div class="group-tab-user bg-gray3 pa20-xs _flex center-xs between-xsh">
										<ul id="type-user" class="idTabs tab-btn _chd-mr10">
											<li><a href="#company" class="selected"><big>Company</big> <small class="d-block">บริษัท/องค์กร</small></a></li>
											
											<li><a href="#education"><big>Education</big> <small class="d-block">สถานบันการศึกษา</small></a></li>
											<li><a href="#community"><big>Community</big> <small class="d-block">กลุ่ม/ชมรม/ชุมชน</small> </a></li>

											<!--<li><a href="#import"><i class="qic-img"><img src="di/ic-import.png" height="16"></i> Import User (.csv)</a></li>-->
										</ul>
										
										<div class="user-sort-bar d-flex flex-nowrap middle-xs">
											<!--<a id="sw-adv-srh" href="edit-table-custom.php" ><i class="fas fa-sliders-h"></i></a>-->
					

												<button type="submit" class="ui-btn-green2 btn-md"><i class="fas fa-check-circle"></i> ยืนยัน</button>
	
				
										</div>
									</div>
									
									<div class="contentTabs">
										<!-- All User -->
										<div id="company" class="card-body _self-pt30 middle-xs">

											
											
										<div class="table-resp off">
											<div class="mb10-xs d-flex end-xs">
												<a href="broadcasts-drop-edit.php" class="ui-btn-border-green btn-sm" title="Edit Table"><i class="fas fa-edit"></i> Edit Table</a>
											</div>

											
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">รหัสพนักงาน</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">บริษัท</th>
											<th scope="col" align="center">ฝ่าย</th>
											<th scope="col" align="center">แผนก</th>
											<th scope="col" align="center">ตำแหน่ง</th>
											<th scope="col" align="center">ระดับงาน</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> <small>Add Friend</small></th>
											<th scope="col" align="center"><small>APPROVED</small></th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
<?php function generateRandomString($length = 10) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} ?>
											<?php for($i=1;$i<=20;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle"><?php if($i<=2) {?> <?php } else { ?>1100200<?php echo $i+1; ?><?php } ?></td>
											<td valign="middle"><?php if($i<=2) {?> <?php } else { ?>ทวีทรัพย์ พรสัจจะ<?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>INNOHUB<?php } else { ?> OTO <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Production<?php } elseif($i%5==0) {?> IT <?php } elseif($i%7==0) {?>HR<?php } else { ?> Admin <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Content<?php } elseif($i%5==0) {?> Platform <?php } elseif($i%7==0) {?>Interview<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Web Design<?php } elseif($i%5==0) {?> Developer <?php } elseif($i%7==0) {?>Supervisor<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%3==0) {?>Manager<?php } elseif($i%5==0) {?> Assitant <?php } else { ?> Straff <?php } ?></td>
											<td valign="middle">
												<?php if($i>17 && $i<=20){ ?>
												<?php } else { ?>
													<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40">
													<? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> 
													<span class="name"><?php echo generateRandomString(); ?></span>
												<?php } ?>
											</td>
											<td valign="middle" align="center"><?php if($i>=3 && $i<=17) {?> <?php if($i%2==0 || $i%3==0 ) {?><span class="text-success">Added</span><?php } else { ?><?php } ?> <?php } ?></td>	
											<td valign="middle" align="center">
												<div class="icheck-primary ma0">
													<input type="checkbox" name="approve-csv<? echo($i) ?>" id="approve-csv-chk<? echo($i) ?>" <?php if($i%3==0 && $i<15) {?>checked=""<?php } ?>> 
													<label for="approve-csv-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
												  </div>
											</td>
											<td valign="middle" align="center">
												<!-- <a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> --> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user"  title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											
			
										</div>
										</div>
										
										<!-- education -->
										<div id="education" class="card-body _self-pt30 middle-xs">
										<div class="table-resp off">
											<div class="mb10-xs d-flex end-xs">
												<a href="broadcasts-drop-edit.php" class="ui-btn-border-green btn-sm" title="Edit Table"><i class="fas fa-edit"></i> Edit Table</a>
											</div>
											
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">รหัสนักศึกษา</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">ห้อง</th>
											<th scope="col" align="center">ระดับชั้น</th>
											<th scope="col" align="center">ครูปที่ปรึกษา</th>
											<th scope="col" align="center">เบอร์โทรศัพท์</th>
											<th scope="col" align="center">ผู้ปกครอง</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> <small>Add Friend</small></th>
											<th scope="col" align="center"><small>APPROVED</small></th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=9;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle">1100200<?php echo $i+1; ?></td>
											<td valign="middle">ทวีทรัพย์ พรสัจจะ</td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>INNOHUB<?php } else { ?> OTO <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Production<?php } elseif($i%5==0) {?> IT <?php } elseif($i%7==0) {?>HR<?php } else { ?> Admin <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Content<?php } elseif($i%5==0) {?> Platform <?php } elseif($i%7==0) {?>Interview<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Web Design<?php } elseif($i%5==0) {?> Developer <?php } elseif($i%7==0) {?>Supervisor<?php } else { ?> Officer <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>Manager<?php } elseif($i%5==0) {?> Assitant <?php } else { ?> Straff <?php } ?></td>
											<td valign="middle">
												<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> <span class="name"><?php echo generateRandomString(); ?></span>
											</td>
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%2==0 || $i%3==0) {?><span class="text-success">Added</span><?php } else { ?><?php } ?></td>	
											<td valign="middle" align="center">
												<div class="icheck-primary ma0">
													<input type="checkbox" name="approve<? echo($i) ?>" id="approve-chk<? echo($i) ?>"> 
													<label for="approve-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
												  </div>
											</td>
											<td valign="middle" align="center">
												<!-- <a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> --> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user" title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											

										</div>
									</div>
										
										<!-- Community -->
										<div id="community" class="card-body _self-pt30 middle-xs">
										<div class="table-resp off">
											<div class="mb10-xs d-flex middle-xs end-xs">
												<!--<a href="javascrtip:;" data-fancybox="" data-src="#acc-add-user" class="ui-btn-border-green btn-sm" title="Add Menual User"><i class="fas fa-user-plus"></i> Edit User</a>-->
												<span class="text-sm mr10-xs">
													<div class="icheck-primary ma0">
													<input type="checkbox" name="auto_id" id="auto_id"> 
													<label for="auto_id">สร้างรหัสประจำตัวอัตโนมัติ</label>
												  </div>
												 </span>
												
												<a href="broadcasts-drop-edit.php" class="ui-btn-border-green btn-sm" title="Edit Table"><i class="fas fa-edit"></i> Edit Table</a>
											</div>
											
											<table class="table tb-bordered tb-skin" style="min-width: 900px" width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr>
											<th scope="col" align="center">NO.</th>
											<th scope="col" align="center">Line</th>
											<th scope="col" align="center">รหัสประจำตัว</th>
											<th scope="col" align="center">ชื่อ - นามสกุล</th>
											<th scope="col" align="center">เพศ</th>
											<th scope="col" align="center">อายุ</th>
											<th scope="col" align="center"><i class="fab fa-line text-success"></i> Add Friend</th>
											<th scope="col" align="center"><small>APPROVED</small></th>
											<th scope="col">&nbsp;</th>
											</tr>
											</thead>
											<tbody>
											<?php for($i=1;$i<=15;$i++){ ?>
											<tr>
											<td valign="middle" align="center"><?php echo $i; ?></td>
											<td valign="middle">
												<?php if($i%3==0) {?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar4.png" alt="Fat Rascal" width="40"><? } else { ?><img class="rounded2" src="https://www.w3schools.com/w3images/avatar3.png" alt="Fat Rascal" width="40"><? } ?> <span class="name"><?php echo generateRandomString(); ?></span>
											</td>
											<td valign="middle">1100200<?php echo $i+1; ?></td>
											<td valign="middle">ทวีทรัพย์ พรสัจจะ</td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>ชาย<?php } elseif($i%5==0) {?> หญิง <?php } else { ?> ไม่ระบุ <?php } ?></td>
											<td valign="middle" align="center"><?php if($i%3==0) {?>30<?php } elseif($i%5==0) {?> 35 <?php } elseif($i%7==0) {?>25<?php } else { ?> 40 <?php } ?></td>
											
											<td valign="middle" align="center"><?php if($i<=2) {?> <?php } elseif($i%2==0 || $i%3==0) {?><span class="text-success">Added</span><?php } else { ?><?php } ?></td>											
											<td valign="middle" align="center">
												<div class="icheck-primary ma0">
													<input type="checkbox" name="approve3<? echo($i) ?>" id="approve3-chk<? echo($i) ?>"> 
													<label for="approve3-chk<? echo($i) ?>"><span class="visible-xs"> Approve</span></label>
												  </div>
											</td>
											<td valign="middle" align="center">
												<!-- <a class="more ui-btn-blue-min btn-xs" href="javascrtip:;" data-fancybox="" data-src="#userdetail" title="ดูรายละเอียด"><i class="fas fa-eye"></i> view</a> --> 
												<!--<a href="#" title="View Leave" class="ui-btn-trans-mid btn-xs"><i class="fas fa-calendar-day"></i></a>
												<a href="#" title="View Time Attendance" class="ui-btn-trans-mid btn-xs"><i class="fas fa-user-clock"></i></a>-->
												<a href="javascrtip:;" data-fancybox="" data-src="#acc-edit-user" title="Edit" class="ui-btn-trans-mid btn-xs"><i class="fas fa-edit"></i></a>
											</td>
											</tr>
											<?php } ?>
											</tbody>
											</table>
											
											
											

										</div>
									</div>

									
									
									<?php /*?>
										<div class="sticky-bottom card-footer mf-bottom">
										<div class="__chd-ph10 center-xs">
												<button type="reset" class="ui-btn-gray btn-md" data-toggle="modal" data-target="#modal-close">Cancel</button>
												<button type="button" class="ui-btn-green btn-md" onclick="$(this).children('i').removeClass('hid'); $('.form-sending')[0].reset();"><i class="hid fas fa-circle-notch fa-spin"></i> Save Changes</button>
										</div>
										</div><?php */?>
								  
								  </div>
								  
								</div>
								<!-- /card -->
							</div>

						</div>
					</div>
				</div>
					</div>

			</div>
			
			

		</section>
    </div>
</div>

<!--<div id="skin-loading" class="bg-wh" onclick="$(this).fadeOut();">
	<div class="lds-hourglass"></div>
</div>-->
<script>
	window.setTimeout(function(){
		$('#skin-loading').fadeOut();
	}, 3000);
</script>

<!-- footer -->
<?php include("incs/footer.html") ?>
<!-- /footer -->
<!-- js -->
<?php include("incs/js.html") ?>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	 
      $('.carousel').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 1,
        maxItems: 3,
		move: 1,
        /*start: function(slider){
          $('body').removeClass('loading');
        }*/
      });

	//select2
	$('.js-select-multi').select2({
		//maximumSelectionLength: 3,
    	placeholder: "Select",
    	//allowClear: true,
		//tags: true,
		dropdownAutoWidth : true,
		width: '100%'
	});
	$(".js-adv-select").select2({
	   //dropdownAutoWidth : true,
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  });

	$(".js-example-tags").select2({
	   //dropdownAutoWidth : true,
	   placeholder: "Select",
	  width: '100%',
	  tags: true,
	  insertTag: function (data, tag) {
		// Insert the tag at the end of the results
		data.push(tag);
	  }
	  /*createTag: function (params) {
		var term = $.trim(params.term);

		if (term === '') {
		  return null;
		}

		return {
		  id: term,
		  text: term,
		  newTag: true // add additional parameters
		}
	  }*/
	});
	
	/*toggle radio*/
	$(".btn-group-toggle input[type=radio]").on("click", function() {
		
		if ($(this).parents('.btn').hasClass( 'active' )) {
		} else {
			$(this).parents(".btn-group-toggle").find('.btn').removeClass('active');
			$(this).parents('.btn').addClass('active');
		}
		

	})
	
	

});
  </script>

<link rel="stylesheet" href="js/plugin/dropzone.min.css" />
<script src="js/plugin/dropzone.min.js"></script>

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/basic.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.js"></script>-->
<script>
if ($().dropzone && !$(".dropzone").hasClass("disabled")) {
      $(".dropzone").dropzone({
        url: "https://httpbin.org/post",
        init: function () {
          this.on("success", function (file, responseText) {
            console.log(responseText);
          });
        },
        thumbnailWidth: 160,
        previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
      });
    }
	</script>
<!-- /js -->

</body>
</html>
